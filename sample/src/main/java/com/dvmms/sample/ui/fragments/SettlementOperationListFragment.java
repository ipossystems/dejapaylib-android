package com.dvmms.sample.ui.fragments;

import android.content.Context;

import com.dvmms.dejapay.DejavooTerminal;
import com.dvmms.dejapay.IRequestCallback;
import com.dvmms.dejapay.exception.DejavooThrowable;
import com.dvmms.dejapay.models.requests.DejavooRequestSettlement;
import com.dvmms.dejapay.models.requests.DejavooResponseSettlement;
import com.dvmms.sample.common.Logger;
import com.dvmms.sample.screens.navigators.SettleSampleNavigatorContract;
import com.dvmms.sample.ui.TerminalService;
import com.dvmms.sample.ui.fields.BaseFieldViewModel;
import com.dvmms.sample.ui.fields.NavigationWithHeaderFieldViewModel;

import java.util.ArrayList;
import java.util.List;

public class SettlementOperationListFragment extends AbstractFieldsFragment {
    public static final String TAG = "SettleOperationListFragment";
    private SettleSampleNavigatorContract mNavigator;

    @Override
    protected void setupUI() {
        List<BaseFieldViewModel> fields = new ArrayList<>();


        fields.add(new NavigationWithHeaderFieldViewModel.Builder()
                .title("Settle batch")
                .hasHeader(false)
                .clickListener((f, v) -> settleBatch(false))
                .build()
        );

        fields.add(new NavigationWithHeaderFieldViewModel.Builder()
                .title("Settle batch (force)")
                .hasHeader(false)
                .clickListener((f, v) -> settleBatch(true))
                .build()
        );

        setForm(fields);
    }

    private void settleBatch(boolean isForce) {
        DejavooTerminal terminal = TerminalService.getInstance().getTerminal();

        if (terminal == null) {
            showAlertMessage("DejaPay not configured");
            return;
        }

        DejavooRequestSettlement settleRequest = DejavooRequestSettlement.builder()
                .setForce(isForce)
                .build();

        onStartProgress("Processing Settle request");

        terminal.processSettlementRequest(
                settleRequest,
                new IRequestCallback<DejavooResponseSettlement>() {
                    @Override
                    public void onResponse(DejavooResponseSettlement response) {
                        onStopProgress();
                        Logger.d(TAG, "Settle request completed");
                        showAlertMessage("Settle request completed");
                        mNavigator.showResponse(response);
                    }

                    @Override
                    public void onError(DejavooThrowable t) {
                        onStopProgress();
                        Logger.e(TAG, t, "Settle request failed");
                        showAlertMessage("Transaction Failed: " + t.toString());
                    }
                }
        );
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SettleSampleNavigatorContract) {
            mNavigator = (SettleSampleNavigatorContract) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement SettleSampleNavigatorContract");
        }
    }


    public static SettlementOperationListFragment newInstance() {
        return new SettlementOperationListFragment();
    }

}
