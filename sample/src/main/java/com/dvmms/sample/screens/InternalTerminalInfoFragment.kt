package com.dvmms.sample.screens

import android.os.Build
import android.util.Log
import com.dvmms.dejapay.IRequestCallback
import com.dvmms.dejapay.exception.DejavooThrowable
import com.dvmms.dejapay.terminals.DvPayTerminalInfo
import com.dvmms.dejapay.terminals.InternalTerminal
import com.dvmms.sample.ui.fields.BaseFieldViewModel
import com.dvmms.sample.ui.fields.LabelWithHeaderFieldViewModel
import com.dvmms.sample.ui.fragments.AbstractFieldsFragment

class InternalTerminalInfoFragment : AbstractFieldsFragment() {
    private lateinit var navigation: InternalNavigation

    override fun setupUI() {
        navigation = navigator(InternalNavigation::class.java)

        context?.let {
            InternalTerminal(requireContext().packageName).getTerminalInfo(it,
                object : IRequestCallback<DvPayTerminalInfo> {
                    override fun onResponse(response: DvPayTerminalInfo) {
                        setFieldsWithData(response)
                    }

                    override fun onError(throwable: DejavooThrowable?) {
                        Log.e(TAG, "Callback error has happened")
                    }
                }
            )
        }
    }

    private fun setFieldsWithData(data: DvPayTerminalInfo) {
        val fields = mutableListOf<BaseFieldViewModel>()

        LabelWithHeaderFieldViewModel.Builder()
            .title("Serial Number")
            .data(data.deviceSerialNumber)
            .build()
            .let(fields::add)
        LabelWithHeaderFieldViewModel.Builder()
            .title("TPN")
            .data(data.tpn)
            .build()
            .let(fields::add)
        LabelWithHeaderFieldViewModel.Builder()
            .title("Terminal number")
            .data(data.terminalNumber)
            .build()
            .let(fields::add)
        LabelWithHeaderFieldViewModel.Builder()
            .title("Terminal name")
            .data(data.terminalName)
            .build()
            .let(fields::add)
        LabelWithHeaderFieldViewModel.Builder()
            .title("User name")
            .data(data.userName)
            .build()
            .let(fields::add)
        LabelWithHeaderFieldViewModel.Builder()
                .title("Auth key")
                .data(data.authKey)
                .build()
                .let(fields::add)

        setForm(fields)
    }

    companion object {
        @JvmStatic
        val TAG = InternalTerminalInfoFragment::class.java.simpleName

        @JvmStatic
        fun newInstance(): InternalTerminalInfoFragment {
            return InternalTerminalInfoFragment()
        }
    }
}