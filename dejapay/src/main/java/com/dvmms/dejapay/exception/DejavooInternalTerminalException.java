package com.dvmms.dejapay.exception;

/**
 * Internal Terminal exception
 */
public class DejavooInternalTerminalException extends DejavooThrowable {
    private final String error;
    private final String errorCode;

    public DejavooInternalTerminalException(String error, String errorCode) {
        this.error = error;
        this.errorCode = errorCode;
    }

    public DejavooInternalTerminalException(String error, String errorCode, Throwable throwable) {
        super(throwable);
        this.error = error;
        this.errorCode = errorCode;
    }

    /**
     * Get error message
     * @return error message
     */
    public String getError() {
        return error;
    }

    /**
     * Get error code
     * @return error code
     */
    public String getErrorCode() {
        return errorCode;
    }
}
