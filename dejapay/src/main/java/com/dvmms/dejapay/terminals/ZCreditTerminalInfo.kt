package com.dvmms.dejapay.terminals

class ZCreditTerminalInfo {
    var tpn: String = ""
    var deviceSerialNumber: String = ""
    var userName: String = ""
    var terminalNumber: String = ""
    var terminalName: String = ""
}