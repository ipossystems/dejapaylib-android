package com.dvmms.dejapay.terminals

import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import org.xmlpull.v1.XmlPullParserFactory
import java.io.ByteArrayInputStream
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.math.BigDecimal
import java.util.*

class XmlDocument(private val fields: List<XmlField>) {
    fun isContains(path: String): Boolean {
        return findFieldByPath(path) != null
    }

    private fun getField(path: String): XmlField? {
        return findFieldByPath(path)
    }

    fun getFields(path: String): List<XmlField> {
        return getField(path)?.fields?.toList() ?: listOf()
    }

    fun getFieldValueOrDefault(path: String, defaultValue: String): String {
        return findFieldByPath(path)?.run { data } ?: defaultValue
    }

    fun getFieldValueString(path: String): String? {
        return findFieldByPath(path)?.run { data }
    }

    fun getFieldValueBoolean(path: String): Boolean? {
        return findFieldByPath(path)?.run { data == "true" }
    }


    fun getFieldValueInt(path: String): Int? {
        return findFieldByPath(path)?.run {
            try {
                data.toInt()
            } catch(e: NumberFormatException) {
                null
            }
        }
    }

    fun getFieldValueLong(path: String): Long? {
        return findFieldByPath(path)?.run {
            try {
                data.toLong()
            } catch(e: NumberFormatException) {
                null
            }
        }
    }


    fun getFieldValueBigDecimal(path: String): BigDecimal? {
        return findFieldByPath(path)?.run {
            try {
                BigDecimal(data)
            } catch(e: NumberFormatException) {
                null
            }
        }
    }

    fun getFieldIntValue(path: String, defaultValue: Int): Int {
        val value = getFieldValueOrDefault(path, "") ?: return defaultValue
        return try {
            value.toInt()
        } catch (e: NumberFormatException) {
            defaultValue
        }
    }

    fun getFieldAmountValue(path: String, defaultValue: Int): Int {
        var value = getFieldValueOrDefault(path, "")
        return if (value.isEmpty()) {
            defaultValue
        } else try {
            // Clean non-digits char
            value = value.replace("[^0-9]".toRegex(), "")
            value.toInt()
        } catch (e: NumberFormatException) {
            defaultValue
        }
    }

    //
    fun getFieldAttributeValue(path: String, attr: String, defaultValue: String): String {
        return findFieldByPath(path)?.let {
            it.getAttr(attr, defaultValue)
        } ?: defaultValue
    }

    /**
     * Get field with format
     * @param path case-insensitive path to field, path should be splitted by /, e.i. fields/internal -
     * @return field or null
     */
    private fun findFieldByPath(path: String): XmlField? {
        val components = splitPath(path)

        var fields: List<XmlField> = fields

        while (components.isNotEmpty()) {
            val name = components.pop()
            for (field in fields) {
                if (name.equals(field.name, ignoreCase = true)) {
                    if (components.isEmpty()) {
                        return field
                    } else {
                        fields = field.fields
                    }
                    break
                }
            }
        }
        return null
    }

    private fun splitPath(path: String): Stack<String> {
        val stack = Stack<String>()

        path.split("/").reversed().forEach { stack.push(it) }

        return stack
    }

    companion object {
        fun fromFile(path: String): XmlDocument? {
            return try {
                return fromText(File(path).readText())
            } catch (e: IOException) {
                e.printStackTrace()
                null
            }

        }

        fun fromText(text: String): XmlDocument? {
            val xml = text.replace(">\\s*<".toRegex(), "><")

            return try {
                val fields: MutableList<XmlField> = ArrayList()

                val xmlFactoryObject = XmlPullParserFactory.newInstance()
                val parser = xmlFactoryObject.newPullParser()
                val inputStream: InputStream = ByteArrayInputStream(xml.toByteArray())

                parser.setInput(inputStream, null)
                val fieldStack = Stack<XmlField>()

                var event = parser.eventType
                while (event != XmlPullParser.END_DOCUMENT) {
                    val name = parser.name
                    when (event) {
                        XmlPullParser.START_DOCUMENT -> {
                        }
                        XmlPullParser.START_TAG -> {
                            val field = XmlField(
                                    name, "", HashMap(), ArrayList()
                            )
                            var i = 0
                            while (i < parser.attributeCount) {
                                val attrName = parser.getAttributeName(i)
                                val attrValue = parser.getAttributeValue(i)
                                field.attributes.put(attrName, attrValue)
                                i++
                            }
                            fieldStack.add(field)
                        }
                        XmlPullParser.TEXT -> {
                            val field = fieldStack.peek()
                            if (field != null) {
                                field.data = parser.text
                            }
                        }
                        XmlPullParser.END_TAG -> {
                            val field = fieldStack.pop()
                            if (field != null) {
                                if (!fieldStack.empty()) {
                                    val rootField = fieldStack.peek()
                                    rootField.fields.add(field)
                                } else {
                                    fields.add(field)
                                }
                            }
                        }
                    }
                    event = parser.next()
                }
                XmlDocument(fields)
            } catch (ex: XmlPullParserException) {
                ex.printStackTrace()
                null
            } catch (ex: IOException) {
                ex.printStackTrace()
                null
            }
        }
    }
}

class XmlField(
        val name: String,
        var data: String = "",
        val attributes: MutableMap<String, String> = mutableMapOf(),
        val fields: MutableList<XmlField> = mutableListOf()
) {
    fun getAttr(attrName: String, defaultValue: String): String {
        return attributes.getOrElse(attrName) { defaultValue }
    }

    fun getAttrInt(attrName: String, defaultValue: Int): Int {
        return getAttr(attrName, defaultValue.toString()).toIntOrNull() ?: defaultValue
    }

    fun asXmlString(): String {
        val builder = StringBuilder()
        builder.append("<$name")
        attributes.map { " ${it.key}=\"${it.value}\" "}
                .joinToString("")
                .let(builder::append)

        builder.append(">")

        if (data.isEmpty()) {
            fields.forEach {
                builder.append(it.asXmlString())
            }
        } else {
            builder.append(data)
        }

        builder.append("</$name>")

        return builder.toString()
    }
}