package com.dvmms.sample.screens

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import com.dvmms.dejapay.IRequestCallback
import com.dvmms.dejapay.exception.DejavooThrowable
import com.dvmms.dejapay.models.DejavooPrintoutImageItem
import com.dvmms.dejapay.models.DejavooPrintoutLineItem
import com.dvmms.dejapay.models.DejavooPrintoutRequest
import com.dvmms.dejapay.models.DejavooPrintoutResponse
import com.dvmms.dejapay.terminals.InternalTerminal
import com.dvmms.sample.R
import com.dvmms.sample.ui.fields.BaseFieldViewModel
import com.dvmms.sample.ui.fields.ButtonFieldViewModel
import com.dvmms.sample.ui.fields.TextFieldViewModel
import com.dvmms.sample.ui.formatter.TextFieldFormatter
import com.dvmms.sample.ui.fragments.AbstractFieldsFragment
import java.io.ByteArrayOutputStream


class InternalPrintoutFragment : AbstractFieldsFragment() {
    private lateinit var navigation: InternalNavigation
    private lateinit var request: DejavooPrintoutRequest

    override fun setupUI() {
        navigation = navigator(InternalNavigation::class.java)
        val fields = mutableListOf<BaseFieldViewModel>()

        TextFieldViewModel.Builder()
            .title("AuthKey")
            .type(TextFieldViewModel.Type.Text)
            .data("")
            .key("AuthKey")
            .formatter(TextFieldFormatter())
            .hidden(false)
            .build()
            .let(fields::add)

        ButtonFieldViewModel.Builder()
                .type(ButtonFieldViewModel.Type.Plain)
                .title("Print Text")
                .clickListener { _, _ ->
                    if (findFieldByKey<BaseFieldViewModel>("AuthKey").data<String>().isNotEmpty()) {
                        onStartProgress("Processing")
                        InternalTerminal(requireContext().packageName)
                            .print(requireContext(), setPrintTextRequest(), object :
                                IRequestCallback<DejavooPrintoutResponse> {
                                override fun onResponse(response: DejavooPrintoutResponse) {
                                    onStopProgress()
                                    Log.d("InternalPrintoutFr", "Receive response $response")

                                    showAlertMessage("Success")

                                }

                                override fun onError(t: DejavooThrowable) {
                                    onStopProgress()
                                    Log.e("InternalPrintoutFr", "Transaction failed", t)
                                    showAlertMessage("Transaction Failed ${t.localizedMessage}")
                                }
                            })
                    } else {
                        showAlertMessage("Empty authKey")
                    }

                }
                .build()
                .let(fields::add)

        ButtonFieldViewModel.Builder()
            .type(ButtonFieldViewModel.Type.Plain)
            .title("Print Picture")
            .clickListener { _, _ ->
                if (findFieldByKey<BaseFieldViewModel>("AuthKey").data<String>().isNotEmpty()) {
                    onStartProgress("Processing")
                    InternalTerminal(requireContext().packageName)
                        .print(requireContext(), setPrintImageRequest(), object :
                            IRequestCallback<DejavooPrintoutResponse> {
                            override fun onResponse(response: DejavooPrintoutResponse) {
                                onStopProgress()
                                Log.d("InternalPrintoutFr", "Receive response $response")

                                showAlertMessage("Success $response")

                            }

                            override fun onError(t: DejavooThrowable) {
                                onStopProgress()
                                Log.e("InternalPrintoutFr", "Transaction failed", t)
                                showAlertMessage("Transaction Failed ${t.localizedMessage}")
                            }
                        })
                } else {
                    showAlertMessage("Empty authKey")
                }

            }
            .build()
            .let(fields::add)

        setForm(fields)
    }

    private fun setPrintTextRequest(): DejavooPrintoutRequest {
        val requestText = DejavooPrintoutRequest(listOf(
            DejavooPrintoutLineItem.center("Center", false, false, false, false),
            DejavooPrintoutLineItem.left("Center", true, false, false, false),
            DejavooPrintoutLineItem.center("Center", false, false, false, false),
            DejavooPrintoutLineItem.center("Center", false, false, false, true),
            DejavooPrintoutLineItem.right("Center", false, false, false, false)
        ))
        request = requestText
        request.authenticationKey = findFieldByKey<BaseFieldViewModel>("AuthKey").data<String>()
        return request
    }

    private fun setPrintImageRequest(): DejavooPrintoutRequest {
        val image = BitmapFactory.decodeResource(context?.resources, R.drawable.aura_logo)
        val stream = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.JPEG, 80, stream)
        val byteArray = stream.toByteArray()
        val requestImage = DejavooPrintoutRequest(listOf(
            DejavooPrintoutImageItem.Builder().image(byteArray).build()
        ))
        request = requestImage
        request.authenticationKey = findFieldByKey<BaseFieldViewModel>("AuthKey").data<String>()
        return request
    }

    companion object {
        @JvmStatic
        val TAG = InternalPrintoutFragment::class.java.simpleName

        @JvmStatic
        fun newInstance(): InternalPrintoutFragment {
            return InternalPrintoutFragment()
        }
    }
}