package com.dvmms.sample.screens.navigators;

import com.dvmms.dejapay.models.DejavooTransactionResponse;
import com.dvmms.dejapay.models.requests.DejavooResponseSale;

public interface SaleSampleNavigatorContract {
    void showResponse(DejavooResponseSale saleResponse);
    void showReceipt(DejavooTransactionResponse response);

    void onSaleVoided();
}
