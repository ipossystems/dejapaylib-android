package com.dvmms.dejapay

data class LogPosition(
    val file: String,
    val method: String,
    val line: String
)