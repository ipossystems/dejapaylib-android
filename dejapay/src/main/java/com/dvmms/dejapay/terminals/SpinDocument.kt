package com.dvmms.dejapay.terminals

import java.math.BigDecimal


class RequestDocument(private val operation: String) : SpinDocument() {
    fun asRequest(): String {
        return StringBuilder().apply {
            append("<request>")

            append("<operation>$operation</operation>")

            fields.forEach {
                append(it.asXmlString())
            }

            append("</request>")
        }.toString()
    }
}

open class SpinDocument {
    protected val fields = mutableListOf<XmlField>()

    fun add(tag: String, value: String?) {
        value?.let {
            fields.add(XmlField(tag, it))
        }
    }

    fun add(tag: String, value: Int?) {
        value?.let {
            add(tag, it.toString())
        }
    }

    fun add(tag: String, value: BigDecimal?) {
        value?.let {
            add(tag, it.toString())
        }
    }

    fun add(tag: String, value: Boolean?) {
        value?.let {
            if (it) "true" else "false"
        }?.let {
            add(tag, it)
        }
    }

    fun addFields(f: List<XmlField>) {
        fields.addAll(f)
    }

}