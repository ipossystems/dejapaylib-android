package com.dvmms.dejapay.terminals

class DvPayTerminalInfo {
    var tpn: String? = ""
    var deviceSerialNumber: String? = ""
    var userName: String? = ""
    var terminalNumber: String? = ""
    var terminalName: String? = ""
    var authKey: String? = ""
}