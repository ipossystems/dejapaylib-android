package com.dvmms.dejapay;

import com.dvmms.dejapay.models.DejavooPaymentType;
import com.dvmms.dejapay.models.DejavooTransactionType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Platon on 30.06.2016.
 */
class TransactionTypeValidator
    extends DefaultValidator<DejavooTransactionType>
    implements IValidator<DejavooTransactionType>
{
    private final DejavooPaymentType paymentType;

    public TransactionTypeValidator(DejavooPaymentType paymentType) {
        this.paymentType = paymentType;
    }

    @Override
    public boolean validate(ValidatorContext context, DejavooTransactionType field) {
        return true;
    }

    public Map<DejavooPaymentType, List<DejavooTransactionType>> data() {
        HashMap<DejavooPaymentType, List<DejavooTransactionType>> data = new HashMap<>();

        return data;
    }
}
