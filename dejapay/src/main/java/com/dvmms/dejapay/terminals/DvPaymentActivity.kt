package com.dvmms.dejapay.terminals

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.dvmms.dejapay.R
import com.dvmms.dejapay.SimpleLogger
import com.dvmms.dejapay.terminals.PaymentApp.Companion.TAG

interface TransactionCallback {
    fun onSuccess(response: String)
    fun onFailed(t: Throwable)
}

private const val ARG_APP_ID = "ARG_APP_ID"
private const val ARG_PACKET = "ARG_PACKET"
private const val ARG_PAYMENT_APP = "ARG_PAYMENT_APP"


class DvPaymentActivity() : AppCompatActivity() {
    private lateinit var packet: String
    private lateinit var paymentApp: PaymentApp
    private lateinit var appId: String
    private lateinit var logger: SimpleLogger

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_payment)


        packet = savedInstanceState?.getString(ARG_PACKET)
                ?: intent?.getStringExtra(ARG_PACKET)
                        ?: ""
        paymentApp = PaymentApp.fromCode(
            savedInstanceState?.getInt(ARG_PAYMENT_APP)
                ?: intent?.getIntExtra(ARG_PAYMENT_APP, PaymentApp.DvAndroid.code)
                ?: PaymentApp.DvAndroid.code
        )
        appId = savedInstanceState?.getString(ARG_APP_ID)
                ?: intent?.getStringExtra(ARG_APP_ID)
                        ?: ""
        logger = SimpleLogger(appId)
        findViewById<View>(R.id.btnTryAgain).setOnClickListener {
            logger.verbose(applicationContext, TAG, "Try again button pushed")
            sendPacket(packet) }
        findViewById<View>(R.id.btnCancel).setOnClickListener {
            logger.verbose(applicationContext, TAG, "Cancel button pushed")
            sCallback?.onFailed(Exception("Canceled"))
            finish()
        }

        if (packet.isEmpty()) {
            logger.error(applicationContext, TAG, "Response message is empty")
            sCallback?.onFailed(Exception("Response message is empty"))
            finish()
        }

        if (savedInstanceState == null) {
            sendPacket(packet)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(ARG_PACKET, packet)
        outState.putString(ARG_APP_ID, appId)
        outState.putInt(ARG_PAYMENT_APP, paymentApp.code)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        val response: String = intent?.getStringExtra("TerminalTransactionRsp") ?: ""

        if (response.isNotEmpty()) {
            // send to receiver
            logger.verbose(applicationContext, TAG, "Response received: $response")
            logger.verbose(applicationContext, TAG, "callback is $sCallback")
            sCallback?.onSuccess(response)
        } else {
            logger.error(applicationContext, TAG, "Response message is empty")
            logger.verbose(applicationContext, TAG, "callback is $sCallback")
            sCallback?.onFailed(Exception("Response message is empty"))
        }

        finish()
    }

    private fun sendPacket(packet: String) {
        logger.debug(applicationContext, TAG, "Send packet: $packet")
        try {
            val intent = Intent()
            intent.putExtra("TerminalTransaction", packet)
            intent.putExtra("TerminalAppId", appId)
            intent.action = paymentApp.action
            startActivity(intent)
        } catch (e: Exception) {
           sCallback?.onFailed(e)
        }
    }

    override fun finish() {
        setCallback(null)
        super.finish()
    }

    companion object {
        private var sCallback: TransactionCallback? = null

        @JvmStatic
        fun getCallingIntent(
            context: Context,
            app: PaymentApp,
            packet: String
        ): Intent {
            val appId = context.packageName ?: ""
            val intent = Intent(context, DvPaymentActivity::class.java)
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            intent.addFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK)
//            }
            intent.putExtra(ARG_PAYMENT_APP, app.code)
            intent.putExtra(ARG_APP_ID, appId)
            intent.putExtra(ARG_PACKET, packet)

            return intent
        }

        fun isAvailable(context: Context, app: PaymentApp): Boolean {
            val packageManager = context.packageManager
            val intent = Intent(app.action)
            val resolveInfo: List<*> = packageManager.queryIntentActivities(
                    intent,
                    PackageManager.MATCH_DEFAULT_ONLY
            )
            return resolveInfo.isNotEmpty()
        }

        @JvmStatic
        internal fun setCallback(callback: TransactionCallback?) {
            sCallback = callback
        }
    }
}

enum class PaymentApp(val code: Int, val action: String) {
    Aura(1, "android.intent.action.dejavoo.dvandroid.CGI_LOCAL"),
    DvAndroid(2, "android.intent.action.dejavoo.dvandroid.CGI_LOCAL");

    companion object {
        fun fromCode(code: Int): PaymentApp {
            for (app in values()) {
                if (app.code == code) {
                    return app
                }
            }
            return DvAndroid
        }

        val TAG: String = DvPaymentActivity::class.java.canonicalName ?: DvPaymentActivity::class.java.simpleName
    }
}