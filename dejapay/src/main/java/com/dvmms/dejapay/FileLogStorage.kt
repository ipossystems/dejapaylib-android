package com.dvmms.dejapay

import android.content.Context
import android.util.Log
import java.io.*
import java.util.*

class FileLogStorage(
    private val context: Context,
    private val filename: String
) : LogStorage {
    private var logFile: File? = null

    companion object {
        private val tag: String = FileLogStorage::class.java.canonicalName
            ?: FileLogStorage::class.java.simpleName
        private const val MAX_QUEUE_FILE_SIZE = (10 * 1024 * 1024).toLong()
    }

    init {
        logFile = create()
    }

    @Throws(IOException::class, java.lang.RuntimeException::class)
    override fun put(message: String?) {
        var msg = message ?: return
        if (!msg.endsWith("\n")) {
            msg += "\n"
        }

        var writer: FileOutputStream? = null
        try {
            val rawMessage = msg.toByteArray()
            val currSize = getCurrentStorageFileSize() + rawMessage.size
            if (currSize >= MAX_QUEUE_FILE_SIZE) {
                Log.v(tag, "File size larger than maximum size")
                recreateLogFile()
            }
            writer = context.openFileOutput(filename, Context.MODE_APPEND)
            writer.write(rawMessage)
        } finally {
            writer?.close()
        }
    }

    override fun getAllLogs(removeFile: Boolean): Queue<String> {
        val logs: Queue<String> = ArrayDeque()
        var input: FileInputStream? = null
        try {
            input = context.openFileInput(filename)
            val inputStream = DataInputStream(input)
            val bufReader = BufferedReader(InputStreamReader(inputStream))
            var logLine = bufReader.readLine()
            while (logLine != null) {
                logs.offer(logLine)
                logLine = bufReader.readLine()
            }

            if (removeFile) {
                removeLogs()
            }
        } catch (ex: IOException) {
            Log.e(tag, "Cannot load logs from the local storage: ${ex.message}")
        } finally {
            try {
                input?.close()
            } catch (ex2: IOException) {
                Log.e(tag, "Cannot close the local storage file: ${ex2.message}")
            }
        }
        return logs
    }

    @Throws(IOException::class)
    override fun removeLogs() {
        val isDeleted = logFile?.delete() ?: false
        if (!isDeleted) {
            throw IOException("Cannot delete $filename")
        }
    }

    @Throws(IOException::class)
    override fun recreateLogFile() {
        if (logFile == null) {
            logFile = create()
        } else {
            removeLogs()
        }
        logFile = create()
    }

    private fun create(): File {
        return File(context.filesDir, filename)
    }

    private fun getCurrentStorageFileSize(): Long {
        if (logFile == null) {
            logFile = create()
        }
        return logFile?.length() ?: 0
    }
}