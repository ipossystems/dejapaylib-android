package com.dvmms.sample.ui.fields;


import androidx.fragment.app.FragmentManager;

/**
 * Created by Platon on 15.01.2016.
 */
public interface IUseFragmentManager {
    void setSupportFragmentManager(FragmentManager fragmentManager);
}
