package com.dvmms.sample.screens

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dvmms.dejapay.IRequestCallback
import com.dvmms.dejapay.exception.DejavooThrowable
import com.dvmms.dejapay.models.*
import com.dvmms.dejapay.models.requests.DejavooResponseTransactionRecords
import com.dvmms.dejapay.terminals.InternalTerminal
import com.dvmms.sample.ui.fields.BaseFieldViewModel
import com.dvmms.sample.ui.fields.ButtonFieldViewModel
import com.dvmms.sample.ui.fields.TextFieldViewModel
import com.dvmms.sample.ui.formatter.PriceFieldFormatter
import com.dvmms.sample.ui.fragments.AbstractFieldsFragment

class AdjustTipForRecordFragment: AbstractFieldsFragment() {
    private lateinit var responseTransactionRecords: DejavooResponseTransactionRecords
    private lateinit var record: DejavooTransactionRecord


    override fun setupUI() {
        val fields = mutableListOf<BaseFieldViewModel>()
        TextFieldViewModel.Builder()
            .title("Tip")
            .key("Tip")
            .type(TextFieldViewModel.Type.Number)
            .formatter(PriceFieldFormatter())
            .hidden(false)
            .build()
            .let(fields::add)

        ButtonFieldViewModel.Builder()
            .title("Adjust")
            .clickListener { _, _ -> tipAdjust()}
            .build()
            .let(fields::add)
        setForm(fields)
    }

    private fun tipAdjust() {
        val request = DejavooTransactionRequest()

        val paymentType: String = arguments?.getString(PAYMENT_TYPE, "DvCredit").toString()
        request.paymentType = DejavooPaymentType.Credit
        request.transactionType = DejavooTransactionType.TipAdjust

        request.setAmount(arguments?.getDouble(AMOUNT))
        request.tip = getValueByKey("Tip", 0F)
        request.referenceId = arguments?.getString(REF_ID)
        request.acntLast4 = "9570"
        request.tickerNumber = arguments?.getString(INVOICE_ID)

        onStartProgress("Start tip adjust")
        context?.let {
            InternalTerminal(requireContext().packageName).commitTransaction(it, request,
                object : IRequestCallback<DejavooTransactionResponse> {
                    override fun onResponse(response: DejavooTransactionResponse?) {
                        onStopProgress()
                        response?.let { it1 ->
                            navigator(InternalNavigation::class.java).openDppResponse(
                                it1
                            )
                        }
                    }

                    override fun onError(throwable: DejavooThrowable?) {
                        onStopProgress()
                        showAlertMessage(throwable?.message)
                    }
                })
        }
    }

    companion object {

        @JvmStatic
        val TAG = AdjustTipForRecordFragment::class.java.simpleName
        private const val PAYMENT_TYPE = "PAYMENT_TYPE"
        private const val AMOUNT = "AMOUNT"
        private const val REF_ID = "REF_ID"
        private const val INVOICE_ID = "INVOICE_ID"

        @JvmStatic
        fun newInstance(paymentType: String, amount: Double, refId: String, invoiceId: String): AdjustTipForRecordFragment {
            val fragment = AdjustTipForRecordFragment()
            val args = Bundle().apply {
                putString(PAYMENT_TYPE, paymentType)
                putDouble(AMOUNT, amount)
                putString(REF_ID, refId)
                putString(INVOICE_ID, invoiceId)
            }

            fragment.arguments = args
            return fragment
        }
    }

    private fun <T> getValueByKey(key: String?, defaultValue: T): T {
        val field = findFieldByKey<BaseFieldViewModel>(key)
        return if (field != null) {
            field.data()
        } else defaultValue
    }
}