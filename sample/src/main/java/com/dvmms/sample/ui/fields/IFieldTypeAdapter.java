package com.dvmms.sample.ui.fields;

/**
 * Created by Platon on 15.01.2016.
 */
public interface IFieldTypeAdapter {
    int resourceLayoutId();
}
