package com.dvmms.dejapay;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Platon on 30.06.2016.
 */
class DejavooValidator {
    private final ValidatorContext context;
    private List<ValidatorItem> items = new ArrayList<>();

    protected DejavooValidator(ValidatorContext context) {
        this.context = context;
    }

    public static DejavooValidator validator() {
        return new DejavooValidator(new ValidatorContext());
    }

    public static DejavooValidator validator(ValidatorContext context) {
        return new DejavooValidator(context);
    }

    public <T> DejavooValidator on(T model, IValidator<T> validator) {
        items.add(new ValidatorItem(model, validator));
        return this;
    }


    public <T> DejavooValidator onEach(Collection<T> models, IValidator<T> validator) {
        for (T model : models) {
            on(model, validator);
        }
        return this;
    }


    public DejavooValidatorResult doValidate() {
        for (ValidatorItem item : items) {
            if (item.getValidator().accept(context, item.getTarget())) {
                item.getValidator().validate(context, item.getTarget());
            }
        }

        return new DejavooValidatorResult(context.getErrors());
    }
}
