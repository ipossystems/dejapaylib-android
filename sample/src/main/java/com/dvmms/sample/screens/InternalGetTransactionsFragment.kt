package com.dvmms.sample.screens

import android.os.Build
import android.util.Log
import com.dvmms.dejapay.IRequestCallback
import com.dvmms.dejapay.exception.DejavooThrowable
import com.dvmms.dejapay.models.DejavooPaymentType
import com.dvmms.dejapay.models.requests.DejavooResponseTransactionRecords
import com.dvmms.dejapay.terminals.DvPayTerminalInfo
import com.dvmms.dejapay.terminals.InternalTerminal
import com.dvmms.sample.ui.fields.BaseFieldViewModel
import com.dvmms.sample.ui.fields.ButtonFieldViewModel
import com.dvmms.sample.ui.fragments.AbstractFieldsFragment

class InternalGetTransactionsFragment : AbstractFieldsFragment() {
    private lateinit var navigation: InternalNavigation

    override fun setupUI() {
        navigation = navigator(InternalNavigation::class.java)

        val fields = mutableListOf<BaseFieldViewModel>()

        ButtonFieldViewModel.Builder()
                .type(ButtonFieldViewModel.Type.Plain)
                .title("Test")
                .clickListener { _, _ ->
                    onStartProgress("Processing")
                    InternalTerminal(requireContext().packageName).getTerminalInfo(requireContext(),
                        object : IRequestCallback<DvPayTerminalInfo> {
                            override fun onResponse(response: DvPayTerminalInfo) {
                                response.tpn?.let {
                                    response.authKey?.let { it1 ->
                                        InternalTerminal(requireContext().packageName)
                                            .getTransactions(requireContext(), DejavooPaymentType.Credit, 1, 10,
                                                it,
                                                it1,
                                                object :
                                                    IRequestCallback<DejavooResponseTransactionRecords> {
                                                    override fun onResponse(response: DejavooResponseTransactionRecords) {
                                                        onStopProgress()
                                                        Log.d("GetTransactionsFr", "Receive response $response")

                                                        showAlertMessage("Terminal has ${response.numRecords} transactions")

                                                    }

                                                    override fun onError(t: DejavooThrowable) {
                                                        onStopProgress()
                                                        Log.e("GetTransactionsFr", "Transaction failed", t)
                                                        showAlertMessage("Transaction Failed ${t.localizedMessage}")
                                                    }
                                                })
                                    }
                                }
                            }

                            override fun onError(throwable: DejavooThrowable?) {
                                Log.e(InternalTerminalInfoFragment.TAG, "Callback error has happened")
                            }
                        }
                    )


                }
                .build()
                .let(fields::add)

        setForm(fields)
    }

    companion object {
        @JvmStatic
        val TAG = InternalGetTransactionsFragment::class.java.simpleName

        @JvmStatic
        fun newInstance(): InternalGetTransactionsFragment {
            return InternalGetTransactionsFragment()
        }
    }
}