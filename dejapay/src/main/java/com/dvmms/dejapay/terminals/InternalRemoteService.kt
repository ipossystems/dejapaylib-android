package com.dvmms.dejapay.terminals

import android.content.ComponentName
import android.content.Context
import android.content.ServiceConnection
import android.os.IBinder
import android.os.RemoteException
import com.dejavoo.aura.app.IInternalTerminal
import com.dvmms.dejapay.IRequestCallback
import com.dvmms.dejapay.SimpleLogger
import java.util.concurrent.LinkedBlockingQueue
import kotlin.RuntimeException

const val KEY_TPN = "TPN"
const val KEY_USER_NAME = "USER_NAME"
const val KEY_TERMINAL_NUMBER = "TERMINAL_NUMBER"
const val KEY_TERMINAL_NAME = "TERMINAL_NAME"
const val KEY_DEVICE_SERIAL_NUMBER = "DEVICE_SERIAL_NUMBER"
const val KEY_AUTH_KEY = "AUTH_KEY"

open class InternalRemoteService(private val logger: SimpleLogger) {
    open var internalTerminal: IInternalTerminal? = null

    private val commands = LinkedBlockingQueue<() -> Unit>()
    val isTerminalNotConnected
        get() = internalTerminal == null

    var conService: ServiceConnection? = null


    open fun getServiceConnection(context: Context, callback: IRequestCallback<ZCreditTerminalInfo>): ServiceConnection {
        val serviceConnection =  object : ServiceConnection {
            override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
                logger.e(TAG, "Service $name connected on the side of the library")
                internalTerminal = IInternalTerminal.Stub.asInterface(service)
                logger.e(TAG, "$internalTerminal")
//                checkForAwaitingCommands()
                getTerminalInfo(context, callback)
            }

            override fun onServiceDisconnected(name: ComponentName?) {
                logger.e(TAG, "onServiceDisconnected $name")
                internalTerminal = null
            }
        }

        return serviceConnection
    }

    fun checkForAwaitingCommands() {
        while (commands.isNotEmpty()) {
            val executable = commands.take()
            executable.invoke()
        }
    }

    open fun getTerminalInfo(context: Context, callback: IRequestCallback<ZCreditTerminalInfo>) {
        val storage = ZCreditTerminalInfoStorage(context)
        if (internalTerminal != null) {
            callback.onResponse(generateTerminalInfo(internalTerminal!!.parameters as MutableMap<String, String>?, context))
        } else {
            commands.add {
                callback.onResponse(
                    generateTerminalInfo(
                        internalTerminal?.parameters as MutableMap<String, String>?,
                        context
                    )
                )
            }
        }

    }

    fun generateTerminalInfo(response: MutableMap<String, String>?, context: Context): ZCreditTerminalInfo {
        //conService?.let { context.unbindService(it) }
        val storage = ZCreditTerminalInfoStorage(context)
        return if (response != null) {
            internalTerminal = null
            conService?.let { context.unbindService(it) }
            val tpn = response[KEY_TPN] as String ?: ""
            val deviceSerialNumber = response[KEY_DEVICE_SERIAL_NUMBER] as String ?: ""
            val userName = response[KEY_USER_NAME] as String ?: ""
            val terminalNumber = response[KEY_TERMINAL_NUMBER] as String ?: ""
            val terminalName = response[KEY_TERMINAL_NAME] as String ?: ""

            logger.e("HUEHUE tpn: $tpn, SN: $deviceSerialNumber")
            storage.apply {
                this.tpn = tpn
                this.deviceSerialNumber = deviceSerialNumber
                this.userName = userName
                this.terminalNumber
                this.terminalName = terminalName
            }

            logger.e("MES FIN")
            ZCreditTerminalInfo().apply {
                this.tpn = tpn
                this.deviceSerialNumber = deviceSerialNumber
                this.userName = userName
                this.terminalNumber = terminalNumber
                this.terminalName = terminalName
            }
        } else {
            ZCreditTerminalInfo().apply {
                tpn = storage.tpn
                deviceSerialNumber = storage.deviceSerialNumber
                userName = storage.userName
                terminalName = storage.terminalName
                terminalNumber = storage.terminalNumber
            }
        }
    }


    companion object {
        val TAG = InternalRemoteService::class.simpleName ?: "InternalRemoteService"
    }
}