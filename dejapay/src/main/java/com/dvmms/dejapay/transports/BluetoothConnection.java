package com.dvmms.dejapay.transports;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import com.dvmms.dejapay.FakeLogger;
import com.dvmms.dejapay.IDejaPayLogger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.UUID;

/**
 * Created by Platon on 29.06.2016.
 */
class BluetoothConnection {
    private static final UUID MY_UUID_SECURE =
            UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");
    //UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static final UUID MY_UUID_INSECURE =
            //UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");
            UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private final String mDeviceMacAddress;
    private ConnectionThread mConnectionThread;
    private IBluetoothService.BluetoothStatus mStatus = IBluetoothService.BluetoothStatus.None;

    private final BluetoothAdapter mBluetoothAdapter;
    private IBluetoothService.IBluetoothListener listener;
    private IDejaPayLogger logger;

    public BluetoothConnection(String macAddress, BluetoothAdapter bluetoothAdapter) {
        this.mDeviceMacAddress = macAddress;
        this.mBluetoothAdapter = bluetoothAdapter;
        this.logger = new FakeLogger();
    }

    public boolean connect() {
        if (mStatus != IBluetoothService.BluetoothStatus.None) {
            return true;
        }

        try {
            setStatus(IBluetoothService.BluetoothStatus.Connecting);
            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mDeviceMacAddress);
            BluetoothConnector connector = new BluetoothConnector(
                    device,
                    false,
                    mBluetoothAdapter,
                    Arrays.asList(MY_UUID_INSECURE, MY_UUID_SECURE)
            );
            BluetoothConnector.BluetoothSocketWrapper socket = connector.connect();
            mConnectionThread = new ConnectionThread(socket);
            if (mConnectionThread.isInitialized()) {
                mConnectionThread.start();
                setStatus(IBluetoothService.BluetoothStatus.Connected);
                return true;
            } else {
                mConnectionThread = null;
                setStatus(IBluetoothService.BluetoothStatus.None);
                return false;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            setStatus(IBluetoothService.BluetoothStatus.None);
            return false;
        }
    }

    public synchronized boolean write(byte[] bytes) throws InterruptedException {
        try {
            if (mConnectionThread == null) {
                throw new InterruptedException();
            }

            mConnectionThread.write(bytes);
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }


    public synchronized void stop() {
        setStatus(IBluetoothService.BluetoothStatus.None);
        if (mConnectionThread != null) {
            mConnectionThread.cancel();
            mConnectionThread = null;
        }
    }

    public void setStatus(IBluetoothService.BluetoothStatus status) {
        this.mStatus = status;
        if (listener != null) {
            listener.onUpdatedStatus(mDeviceMacAddress, status);
        }

    }

    public void setListener(IBluetoothService.IBluetoothListener listener) {
        this.listener = listener;
    }

    public IBluetoothService.BluetoothStatus status() {
        return mStatus;
    }

    public String getDeviceMacAddress() {
        return mDeviceMacAddress;
    }

    public void setLogger(IDejaPayLogger logger) {
        this.logger = logger;
    }

    private class ConnectionThread extends Thread {
        private final BluetoothConnector.BluetoothSocketWrapper mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private boolean stop = false;

        public ConnectionThread(BluetoothConnector.BluetoothSocketWrapper socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
                logger.e(e, "BT ConnectionThread: Input/Output sockets not available");
            }
            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public boolean isInitialized() {
            boolean isInitialized = (mmInStream != null && mmOutStream != null);
            logger.d("BT ConnectionThread: Is initialized: %s", (isInitialized) ? "YES" : "NO");
            return isInitialized;
        }

        @Override
        public void run() {
            byte[] buffer = new byte[1024];

            ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
            int bytes;
            while (true) {
                if (stop) {
                    logger.d("BT ConnectionThread: Stopped Bluetooth connection thread.");
                    break;
                }

                try {
                    bytes = 0;
                    if (mmInStream.available() > 0) {
                        logger.d("BT ConnectionThread: Available bytes %d in sockets", mmInStream.available());
                        bytes = mmInStream.read(buffer);
                        byteBuffer.write(buffer, 0, bytes);
                    }
//                    logger.d("BT ConnectionThread: Try read bytes %d in sockets", bytes);
                    if (bytes == 0) {
                        if (byteBuffer.size() > 0) {
                            logger.d("BT ConnectionThread: %d received bytes from socket", byteBuffer.size());
                            if (listener != null) {
                                listener.onReadData(mDeviceMacAddress, byteBuffer.toByteArray());
                            }

                            byteBuffer = new ByteArrayOutputStream();
                        }
                    }
                    Thread.sleep(500);


                } catch (Exception e) {
                    e.printStackTrace();
                    logger.e(e, "BT ConnectionThread: Failed read data from socket");
                    break;
                }

            }
        }

        public void write(byte[] buffer) throws IOException {
            logger.d("BT ConnectionThread: Write data with size = %d", buffer.length);
            mmOutStream.write(buffer);
        }

        public void cancel() {
            logger.d("BT ConnectionThread: Canceling connection");
            stop = true;
            try {
                mmSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
                logger.e(e, "BT ConnectionThread: failed cancel");
            }
        }


    }
}

