package com.dvmms.sample.screens;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import com.dvmms.dejapay.models.DejavooTransactionResponse;
import com.dvmms.sample.R;
import com.dvmms.sample.ui.fragments.PaymentFragment;
import com.dvmms.sample.ui.fragments.PaymentReportFragment;
import com.dvmms.sample.ui.fragments.PaymentTransactionFragment;

public class PlainRequestActivity
        extends AppCompatActivity
        implements PaymentTransactionFragment.IPaymentTransactionFragmentListener
{
    FrameLayout frmLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        frmLayout = (FrameLayout)findViewById(R.id.flContainer);

        if (savedInstanceState != null) {
            return;
        }

        getSupportFragmentManager().beginTransaction()
                .add(R.id.flContainer, PaymentFragment.newInstance())
                .commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        if(getFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        }
        else {
            getFragmentManager().popBackStack();
        }
    }

    @Override
    public void onShowTransactionReport(DejavooTransactionResponse response) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.flContainer, PaymentReportFragment.newInstance(response), PaymentReportFragment.TAG)
                .addToBackStack(PaymentReportFragment.TAG)
                .commitAllowingStateLoss();
    }


    public static Intent getCallingIntent(Context context) {
        return new Intent(context, PlainRequestActivity.class);
    }
}
