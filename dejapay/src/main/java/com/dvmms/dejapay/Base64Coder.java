package com.dvmms.dejapay;

import android.util.Base64;

public class Base64Coder {
    public String encode(byte[] data) {
        return Base64.encodeToString(data, Base64.NO_WRAP);
    }

    public byte[] decode(String text) {
        return Base64.decode(text, Base64.NO_WRAP);
    }
}
