package com.dvmms.sample.ui.fields;

import com.dvmms.sample.ui.fields.BaseFieldViewModel;

/**
 * Created by Platon on 03.02.2016.
 */
public interface IUpdatedFieldListener<T extends BaseFieldViewModel> {
    void onUpdatedField(T model);
}
