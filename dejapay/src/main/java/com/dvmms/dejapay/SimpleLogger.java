package com.dvmms.dejapay;

import android.content.Context;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.util.IllegalFormatException;

public class SimpleLogger implements IDejaPayLogger  {
    private static final int MAX_LOG_LENGTH = 4000;
    public static final int MAX_LEVEL_EXCEPTIONS = 3;
    private final DefaultLogFormattedData logFormattedData = new DefaultLogFormattedData("");
    private final String appId;

    public SimpleLogger(String appId) {
        this.appId = appId;
    }

    public void debug(Context context, @NotNull String tag, @NotNull String message, Object... args) {
        prepareLog(context, tag, Log.DEBUG, null, message, args);
    }

    public void info(Context context, @NotNull String tag, @NotNull String message, Object... args) {
        prepareLog(context, tag, Log.INFO, null, message, args);
    }

    public void warn(Context context, @NotNull String tag, @NotNull String message, Object... args) {
        prepareLog(context, tag, Log.WARN, null, message, args);
    }

    public void verbose(Context context, @NotNull String tag, @NotNull String message, Object... args) {
        prepareLog(context, tag, Log.VERBOSE, null, message, args);
    }

    public void error(Context context, @NotNull String tag, @NotNull String message, Object... args) {
        prepareLog(context, tag, Log.ERROR, null, message, args);
    }

    public void debug(Context context, @NotNull String tag, Throwable t, @NotNull String message, Object... args) {
        prepareLog(context, tag, Log.DEBUG, t, message, args);
    }

    public void error(Context context, @NotNull String tag, Throwable t, @NotNull String message, Object... args) {
        prepareLog(context, tag, Log.ERROR, t, message, args);
    }

    private void prepareLog(Context context, String tag, int priority, Throwable t, String message, Object... args) {
        try {
            if (message != null && message.length() == 0) {
                message = null;
            }
            if (message == null) {
                if (t == null) {
                    return;
                }
                message = getStackTraceString(t);
            } else {
                if (args.length > 0) {
                    message = String.format(message, args);
                }
                if (t != null) {
                    message += "\n" + getStackTraceString(t);
                }
            }

            log(context, determineLogPosition(), priority, tag, message);
        } catch (IllegalFormatException e) {
            Log.e("LOGGER", "Mistake in log format", e);
        }
    }

    private String getStackTraceString(Throwable t) {
        if (BuildConfig.DEBUG) {
            Throwable nextException = t;
            int level = 1;
            StringBuilder builder = new StringBuilder();
            while (nextException != null) {
                StackTraceElement[] elems = nextException.getStackTrace();
                builder.append(nextException);
                builder.append("\n");
                int limit = 3;
                int i = 0;
                for (StackTraceElement el : elems) {
                    builder.append(" ");
                    builder.append(el.toString());
                    builder.append("\n");
                    i++;
                    if (i >= limit) {
                        break;
                    }
                }

                nextException = nextException.getCause();
                if (level >= MAX_LEVEL_EXCEPTIONS) {
                    break;
                }
                level++;
            }

            return builder.toString();
        }

        if (t != null) {
            StringBuilder builder = new StringBuilder();
            int level = 1;
            Throwable nextException = t;
            while (nextException != null) {
                builder.append(nextException);
                builder.append(" / ");
                nextException = nextException.getCause();
                if (level >= MAX_LEVEL_EXCEPTIONS) {
                    break;
                }
                level++;
            }

            return builder.toString();
        }
        return "";
    }

    private LogPosition determineLogPosition() {
        Thread thread = Thread.currentThread();
        LogPositionDetermination logPositionDetermination = new LogPositionDetermination(thread.getStackTrace());
        return logPositionDetermination.determine();
    }

    protected void log(Context context, LogPosition position, int priority, String tag, String message) {
        if (message.length() < MAX_LOG_LENGTH) {
            if (priority == Log.ASSERT) {
                Log.wtf(tag, message);
            } else {
                Log.println(priority, tag, message);
            }
        } else {
            for (int i = 0, length = message.length(); i < length; i++) {
                int newline = message.indexOf('\n', i);
                newline = newline != -1 ? newline : length;
                do {
                    int end = Math.min(newline, i + MAX_LOG_LENGTH);
                    String part = message.substring(i, end);
                    if (priority == Log.ASSERT) {
                        Log.wtf(tag, part);
                    } else {
                        Log.println(priority, tag, part);
                    }
                    i = end;
                } while (i < newline);
            }
        }

        AndroidLogger loggerEntry = AndroidLogger.getInstance(context);
        if (loggerEntry != null) {
            LogData logData = logFormattedData.format(position, priority, tag, message, appId);
            LogMessage logMessage = new ElasticLog(
                    logData.getEnv(),
                    logData.getFile(),
                    logData.getMethod(),
                    logData.getLine(),
                    logData.getLevel(),
                    logData.getId(),
                    logData.getMessage(),
                    logData.getTime(),
                    logData.getVersion(),
                    logData.getPosAppId()
            );
            loggerEntry.log(logMessage.toString());
        }
    }

    private String getPriorityName(int priority) {
        String priorityName = "VERBOSE";
        switch (priority) {
            case Log.VERBOSE:
                priorityName = "VERBOSE";
                break;
            case Log.DEBUG:
                priorityName = "DEBUG";
                break;
            case Log.INFO:
                priorityName = "INFO";
                break;
            case Log.WARN:
                priorityName = "WARN";
                break;
            case Log.ERROR:
                priorityName = "ERROR";
                break;
            case Log.ASSERT:
                priorityName = "ASSERT";
                break;
            default:
                break;
        }
        return priorityName;
    }

    @Override
    public void v(String message, Object... args) {

    }

    @Override
    public void v(Throwable t, String message, Object... args) {

    }

    @Override
    public void d(String message, Object... args) {

    }

    @Override
    public void d(Throwable t, String message, Object... args) {

    }

    @Override
    public void w(String message, Object... args) {

    }

    @Override
    public void w(Throwable t, String message, Object... args) {

    }

    @Override
    public void e(String message, Object... args) {

    }

    @Override
    public void e(Throwable t, String message, Object... args) {

    }
}
