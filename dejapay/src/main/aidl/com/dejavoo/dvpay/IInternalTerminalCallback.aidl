package com.dejavoo.dvpay;

interface IInternalTerminalCallback {
    void onResponse(String response);
}