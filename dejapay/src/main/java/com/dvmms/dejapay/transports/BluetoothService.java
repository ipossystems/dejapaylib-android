package com.dvmms.dejapay.transports;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.dvmms.dejapay.FakeLogger;
import com.dvmms.dejapay.IDejaPayLogger;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Platon on 29.06.2016.
 */
class BluetoothService implements IBluetoothService {
    private IBluetoothListener listener;

    private final BluetoothAdapter mBluetoothAdapter;
    private Map<String, BluetoothConnection> mConnections;
    private Context context;
    private static BluetoothService instance;
    private IDejaPayLogger logger;

    public static synchronized BluetoothService getInstance(Context context) {
        if (instance == null) {
            instance = new BluetoothService(context);
        }
        return instance;
    }

    @Override
    public void setLogger(IDejaPayLogger logger) {
        this.logger = logger;
    }

    protected BluetoothService(Context context) {
        this.context = context;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        this.logger = new FakeLogger();
        if (mBluetoothAdapter != null) {
            mConnections = new ConcurrentHashMap<>();

            IntentFilter filter1 = new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED);
            IntentFilter filter2 = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
            IntentFilter filter3 = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED);
            context.registerReceiver(mReceiver, filter1);
            context.registerReceiver(mReceiver, filter2);
            context.registerReceiver(mReceiver, filter3);
        } else {

        }
    }

    @Override
    public boolean connectToDevice(String macAddressDevice, IBluetoothListener connectionListener) {
        synchronized (mConnections) {
            logger.d("[MAC=%s] Trying connect to Bluetooth device", macAddressDevice);
            if (!mConnections.containsKey(macAddressDevice)) {
                logger.d("[MAC=%s] Bluetooth device not found, add to list", macAddressDevice);
                BluetoothConnection connection = new BluetoothConnection(macAddressDevice, mBluetoothAdapter);
                mConnections.put(macAddressDevice, connection);
            }

            BluetoothConnection connection = mConnections.get(macAddressDevice);
            connection.setListener(connectionListener);
            connection.setLogger(logger);
            return connection.connect();
        }
    }

    @Override
    public BluetoothStatus getStatus(String macAddressDevice) {
        synchronized (mConnections) {
            if (mConnections.containsKey(macAddressDevice)) {
                return mConnections.get(macAddressDevice).status();
            }
            return BluetoothStatus.None;
        }
    }

    @Override
    public boolean isEnabled() {
        return mBluetoothAdapter.isEnabled();
    }

    @Override
    public boolean isSupported() {
        return (mBluetoothAdapter != null);
    }

    @Override
    public void write(String macAddressDevice, byte[] packet) throws InterruptedException {
        synchronized (mConnections) {
            logger.d("[MAC=%s] Write data to bluetooth", macAddressDevice);
            BluetoothConnection connection = mConnections.get(macAddressDevice);
            connection.write(packet);
        }
    }

    @Override
    public void stop() {
        synchronized (mConnections) {
            logger.d("Stop bluetooth service");
            for (BluetoothConnection connection : mConnections.values()) {
                connection.stop();
            }
            mConnections.clear();
            try {
                context.unregisterReceiver(mReceiver);
            } catch (IllegalArgumentException ignored) {
            }
        }
    }

    @Override
    public void removeDevice(String macAddressDevice) {
        synchronized (mConnections) {
            logger.d("Remove device: %s", macAddressDevice);
            for (BluetoothConnection connection : mConnections.values()) {
                if (connection.getDeviceMacAddress().equalsIgnoreCase(macAddressDevice)) {
                    connection.stop();
                    mConnections.remove(connection);
                    break;
                }
            }
        }
    }

    private void deviceDisconnected(BluetoothDevice bluetoothDevice) {
        synchronized (mConnections) {
            if (mConnections.containsKey(bluetoothDevice.getAddress())) {
                logger.d("Device disconnected: %s", bluetoothDevice.getAddress());
                BluetoothConnection connection = mConnections.get(bluetoothDevice.getAddress());
                connection.stop();
            } else {
                logger.d("Device wasn't disconnected, not found in list");
            }
        }
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            logger.d("Received '%s' action from Bluetooth device %s", action, device.getAddress());
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {

            } else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {

            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {

            } else if (BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED.equals(action)) {

            } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                deviceDisconnected(device);
            }
        }
    };
}
