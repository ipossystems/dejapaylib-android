package com.dvmms.dejapay

interface LogMessage {
    override fun toString(): String
}