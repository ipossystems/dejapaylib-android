package com.dvmms.dejapay.exception;

/**
 * Not connected with DejaPay terminal
 */
public class DejavooTransportNotConnectedException extends DejavooThrowable {
    public DejavooTransportNotConnectedException() {
    }

    public DejavooTransportNotConnectedException(String message) {
        super(message);
    }

    public DejavooTransportNotConnectedException(Throwable throwable) {
        super(throwable);
    }
}
