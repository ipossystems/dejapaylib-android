package com.dvmms.sample.screens

import com.dvmms.sample.ui.fields.BaseFieldViewModel
import com.dvmms.sample.ui.fields.LabelWithHeaderFieldViewModel
import com.dvmms.sample.ui.fields.NavigationWithHeaderFieldViewModel
import com.dvmms.sample.ui.fragments.AbstractFieldsFragment

class ZCreditInternalListFragment : AbstractFieldsFragment() {

    private lateinit var navigation: InternalNavigation

    override fun setupUI() {
        navigation = navigator(InternalNavigation::class.java)

        val fields = mutableListOf<BaseFieldViewModel>()

        NavigationWithHeaderFieldViewModel.Builder()
            .title("Transaction")
            .clickListener { _, _ ->
                navigation.openZCTransaction()
            }
            .build()
            .let(fields::add)

        NavigationWithHeaderFieldViewModel.Builder()
            .title("Terminal info")
            .clickListener { _, _ ->
                navigation.openZCSerialNumber()
            }
            .build()
            .let(fields::add)

        setForm(fields)
    }

    companion object {
        @JvmStatic
        val TAG: String = ZCreditInternalListFragment::class.java.simpleName

        @JvmStatic
        fun newInstance(): ZCreditInternalListFragment {
            return ZCreditInternalListFragment()
        }
    }
}