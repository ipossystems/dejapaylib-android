package com.dvmms.sample.screens

import com.dvmms.sample.ui.fields.BaseFieldViewModel
import com.dvmms.sample.ui.fields.LabelWithHeaderFieldViewModel
import com.dvmms.sample.ui.fields.NavigationWithHeaderFieldViewModel
import com.dvmms.sample.ui.fragments.AbstractFieldsFragment

class InternalListFragment : AbstractFieldsFragment() {

    private lateinit var navigation: InternalNavigation

    override fun setupUI() {
        navigation = navigator(InternalNavigation::class.java)

        val fields = mutableListOf<BaseFieldViewModel>()

        NavigationWithHeaderFieldViewModel.Builder()
                .title("Internal Transaction")
                .clickListener { _, _ ->
                    navigation.openDppInternalTransaction()
                }
                .build()
                .let(fields::add)

        NavigationWithHeaderFieldViewModel.Builder()
                .title("SPIn transaction")
                .clickListener { _, _ ->
                    navigation.openDppSPInTransaction()
                }
                .build()
                .let(fields::add)

        NavigationWithHeaderFieldViewModel.Builder()
                .title("DvPay Terminal info")
                .clickListener { _, _ ->
                    navigation.openInternalTerminalInfo()
                }
                .build()
                .let(fields::add)

        NavigationWithHeaderFieldViewModel.Builder()
                .title("Printout")
                .clickListener { _, _ ->
                    navigation.openPrintout()
                }
                .build()
                .let(fields::add)

        NavigationWithHeaderFieldViewModel.Builder()
                .title("User choice")
                .clickListener { _, _ ->
                    navigation.openUserChoice()
                }
                .build()
                .let(fields::add)

        NavigationWithHeaderFieldViewModel.Builder()
                .title("Get transactions")
                .clickListener { _, _ ->
                    navigation.openGetTransactions()
                }
                .build()
                .let(fields::add)

        NavigationWithHeaderFieldViewModel.Builder()
            .title("Settlement")
            .clickListener { _, _ ->  navigation.navigateToSettlementSample()}
            .build()
            .let(fields::add)

        NavigationWithHeaderFieldViewModel.Builder()
            .title("Tip Adjust")
            .clickListener { _, _ ->  navigation.navigateToTipAdjust()}
            .build()
            .let(fields::add)


        setForm(fields)
    }

    override fun onResume() {
        super.onResume()
    }

    companion object {
        @JvmStatic
        val TAG: String = InternalListFragment::class.java.simpleName

        @JvmStatic
        fun newInstance(): InternalListFragment {
            return InternalListFragment()
        }
    }
}