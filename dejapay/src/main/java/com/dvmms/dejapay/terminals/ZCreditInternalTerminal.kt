package com.dvmms.dejapay.terminals


import android.app.ActivityManager
import android.content.ComponentName
import android.content.Context
import android.content.Context.ACTIVITY_SERVICE
import android.content.Intent
import android.os.Build
import android.os.Process
import android.util.Log
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.content.ContextCompat.startActivity
import com.dvmms.dejapay.IRequestCallback
import com.dvmms.dejapay.SimpleLogger
import com.dvmms.dejapay.UserChoiceConvertor
import com.dvmms.dejapay.exception.DejavooThrowable
import com.dvmms.dejapay.ktx.fromBase64
import com.dvmms.dejapay.models.DejavooUserChoiceRequest
import com.dvmms.dejapay.models.DejavooUserChoiceResponse


interface ZCreditTerminalCallback<T> {
    fun onResponse(response: T)
    fun onError(t: Throwable)
}



class ZCreditInternalTerminal(appId: String) {
    private var logger: SimpleLogger
    private val internalRemoteService: InternalRemoteService
    private val PACKAGE = if (Build.BRAND.contains("wizarpos", true)) {
        "com.dejavoosystems.terminal"
    } else {
        "com.dejavoosystems.terminal.kozen"
    }
    private val ACTION = "com.dejavoosystems.terminal.InternalTerminalService"
    private var intent: Intent? = null

    init {
        logger = SimpleLogger(appId)
        internalRemoteService = InternalRemoteService(logger)
    }

    fun getTerminalInfo(context: Context, callback: IRequestCallback<ZCreditTerminalInfo>) {
        logger.e("Service connected = ${internalRemoteService.isTerminalNotConnected}")
        if (internalRemoteService.isTerminalNotConnected) {
            connectService(context, callback)
        } else {
            internalRemoteService.getTerminalInfo(context, callback)
        }
    }

    private fun isServiceRunning(context: Context): Boolean {
        val manager = context.getSystemService(ACTIVITY_SERVICE) as ActivityManager?
        val a = manager?.getRunningServices(Int.MAX_VALUE)?.find { it.service.packageName == "com.dejavoo.aura.app.InternalTerminalService" }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val b = a?.service ?: ""
            val f = context.startForegroundService(Intent().setAction(ACTION).setPackage(PACKAGE))
            val h = 0
        }
//        context.stopService()
        return manager?.getRunningServices(Int.MAX_VALUE)?.any { it.service.packageName == "com.dejavoosystems.terminal.kozen:remote" } == true
    }

    private fun connectService(context: Context, callback: IRequestCallback<ZCreditTerminalInfo>) {
        try {
            val s = internalRemoteService.getServiceConnection(context, callback)
            internalRemoteService.conService = s
            //Log.e("HUEHUE", "isServiceRunning^ ${isServiceRunning(context)}, terminalNotConnected: ${internalRemoteService.isTerminalNotConnected}")
            if (isServiceRunning(context) && internalRemoteService.isTerminalNotConnected) {
                Log.e("HUEHUE", "Service is alive, but aidl is dead, Try to unbind service")

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    val storage = ZCreditTerminalInfoStorage(context)
                    Log.e("HUEHUE", "dddddd")
                    callback.onResponse(ZCreditTerminalInfo().apply {
                        tpn = storage.tpn
                        deviceSerialNumber = storage.deviceSerialNumber
                        userName = storage.userName
                        terminalName = storage.terminalName
                        terminalNumber = storage.terminalNumber
                    })
                }
            } else {
                val g = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(newIntent)
                } else {
                    context.startService(newIntent)
                }
                context.bindService(newIntent, s, Context.BIND_AUTO_CREATE)
            }

        } catch (e: Throwable) {

            logger.e( "Kozen Failed with ${e.message}")
        }
    }

    private val newIntent: Intent
        get() = Intent().apply {
            setPackage(PACKAGE)
            action = ACTION
            setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        }


    fun commitTransaction(context: Context, transaction: ZCreditTransaction, callback: ZCreditTerminalCallback<ZCreditTransactionResponse>) {
        // convert to XML
        val packet = RequestDocument("transaction").apply {
            add("version", 1)
            add("provider", "zcredit")
            add("currency", transaction.currency.name)
            add("creditType", transaction.type.name)
            add("transactionType", transaction.transactionType.name)
            add("amount", transaction.amount)

            add("numberOfPayments", transaction.numberOfPayments)
            add("firstPayment", transaction.firstPayment)
            add("otherPayment", transaction.otherPayment)

            add("authNum", transaction.authNum)
            add("billingType", transaction.billingType?.name)
            add("holderId", transaction.holderId)
            add("extraData", transaction.extraData)

            add("customerPresent", transaction.customerPresent)
            add("customerName", transaction.customerName)
            add("customerAddress", transaction.customerAddress)
            add("customerEmail", transaction.customerEmail)

            add("phoneNumber", transaction.phoneNumber)

            add("obeligoAction", transaction.obeligoAction?.name)
            add("obeligoActionReferenceNumber", transaction.obeligoActionReferenceNumber)

            add("itemDescription", transaction.itemDescription)

            add("track2", transaction.track2)
            add("cardNumber", transaction.cardNumber)
            add("cvv", transaction.cvv)
            add("expDate", transaction.expDate)
            add("useAdvancedDuplicatesCheck", transaction.useAdvancedDuplicatesCheck)
            add("transactionUniqueID", transaction.transactionUniqueID)
            add("transactionUniqueIdForQuery", transaction.transactionUniqueIdForQuery)

            add("printReceipt", transaction.printReceipt.name)
        }.asRequest()
        logger.debug(context, TAG, "Send transaction with request: $packet")

        DvPaymentActivity.setCallback(object : TransactionCallback {
            override fun onSuccess(response: String) {
                logger.debug(context, TAG, "Response: $response")
                val zcreditResponse = spinToResponse(response)
                if (zcreditResponse != null) {
                    callback.onResponse(zcreditResponse)
                } else {
                    logger.error(context, TAG, "Response is not parsed")
                    callback.onError(Exception("Response is not parsed"))
                }
            }

            override fun onFailed(t: Throwable) {
                logger.error(context, InternalTerminal.TAG, "Failed with error ${t.cause} and message ${t.message}")
                callback.onError(t)
            }
        })
        logger.debug(context, InternalTerminal.TAG, "Send packet: $packet")
        context.startActivity(
            DvPaymentActivity.getCallingIntent(
                context,
                PaymentApp.DvAndroid,
                packet
            )
        )
    }

    fun isInstalled(context: Context): Boolean {
        return DvPaymentActivity.isAvailable(context, PaymentApp.DvAndroid)
    }

    private fun spinToResponse(xml: String): ZCreditTransactionResponse? {
        return XmlDocument.fromText(xml)?.run {
            val response = ZCreditTransactionResponse()

            response.result = getFieldValueString("response/result")?.equals("success", ignoreCase = true)
                    ?: false
            response.code = getFieldValueInt("response/code")
            response.message = getFieldValueString("response/message")
            response.cardNumber = getFieldValueString("response/cardNumber")
            response.expDate = getFieldValueString("response/expDate")
            response.cardName = getFieldValueString("response/cardName")
            response.cardIssuerCode = getFieldValueString("response/cardIssuerCode")
            response.cardFinancerCode = getFieldValueString("response/cardFinancerCode")
            response.cardBrandCode = getFieldValueString("response/cardBrandCode")
            response.referenceNumber = getFieldValueString("response/referenceNumber")
            response.voucherNumber = getFieldValueString("response/voucherNumber")
            response.approvalNumber = getFieldValueString("response/approvalNumber")
            response.isTelApprovalNeeded = getFieldValueBoolean("response/isTelApprovalNeeded")
            response.token = getFieldValueString("response/token")
            response.clientReceipt = getFieldValueString("response/clientReceipt")?.fromBase64()
            response.sellerReceipt = getFieldValueString("response/sellerReceipt")?.fromBase64()
            response.tpn = getFieldValueString("response/tpn")
            response.intotJson = getFieldValueString("response/intOtJSON")?.fromBase64()
            response.useAdvancedDuplicatesCheck = getFieldValueBoolean("response/useAdvancedDuplicatesCheck")
            response.transactionUniqueID = getFieldValueString("response/transactionUniqueID")
            response.transactionUniqueIdForQuery = getFieldValueString("response/transactionUniqueIdForQuery")

            response
        }
    }

    fun showOptions(context: Context, request: DejavooUserChoiceRequest, callback: IRequestCallback<DejavooUserChoiceResponse>) {
        val converter = UserChoiceConvertor()

        val packet = converter.serialize(request)
        logger.debug(context, TAG, "Send showOptions request: $packet")
        DvPaymentActivity.setCallback(object : TransactionCallback {
            override fun onSuccess(data: String) {
                logger.debug(context, InternalTerminal.TAG, "Response $data")
                callback.onResponse(converter.deserialize(data))
            }

            override fun onFailed(t: Throwable) {
                logger.error(context, InternalTerminal.TAG, "Failed with error ${t.cause} and message ${t.message}")
                callback.onError(DejavooThrowable(t))
            }
        })
        logger.debug(context, InternalTerminal.TAG, "Send packet: $packet")
        context.startActivity(
            DvPaymentActivity.getCallingIntent(
                context,
                PaymentApp.DvAndroid,
                packet
            )
        )
    }

    companion object {
        val TAG: String = "DejaPAY_Library:ZCREDIT"
    }
}