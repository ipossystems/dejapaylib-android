
DejaPay library sample
===================================

Availabled classes for users
------------

1. DejavooTerminal 
2. IDejavooTransport
   * BluetoothDejavooTransport
   * ProxyDejavooTransport
   * WifiDejavooTransport
3. DejavooTransactionRequest
4. DejavooTransactionResponse

<xmp><error>Request format error<code>407</code></error></xmp>

<request><PaymentType>Credit</PaymentType><TransType>Sale</TransType><Amount>10.0</Amount><Tip>1.21</Tip><RefId>52</RefId><GetReceipt>Both</GetReceipt><SignCapable>true</SignCapable></request>


<request><PaymentType>Credit</PaymentType><TransType>Sale</TransType><Amount>12.0</Amount><Tip>0.00</Tip><Frequency>OneTime</Frequency><InvNum></InvNum><RefId>1</RefId><RegisterId>1</RegisterId><AuthKey></AuthKey></request>