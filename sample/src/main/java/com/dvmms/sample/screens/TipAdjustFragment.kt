package com.dvmms.sample.screens

import android.os.Build
import com.dvmms.dejapay.IRequestCallback
import com.dvmms.dejapay.exception.DejavooThrowable
import com.dvmms.dejapay.models.DejavooPaymentType
import com.dvmms.dejapay.models.requests.DejavooResponseTransactionRecords
import com.dvmms.dejapay.terminals.InternalTerminal
import com.dvmms.sample.common.JsonConfig
import com.dvmms.sample.ui.TerminalSettingsModel
import com.dvmms.sample.ui.TransactionConfig
import com.dvmms.sample.ui.fields.*
import com.dvmms.sample.ui.formatter.IntegerFieldFormatter
import com.dvmms.sample.ui.fragments.AbstractFieldsFragment
import java.util.LinkedHashMap


private const val CODE_SKIP_FIELD = 100
private const val CODE_NOSKIP_FIELD = 101

class TipAdjustFragment: AbstractFieldsFragment() {
    private lateinit var transactionConfigs: List<TransactionConfig>
    private var paymentTypes: MutableMap<Any, String> = LinkedHashMap()

    private fun initPaymentTypes() {
        transactionConfigs = JsonConfig.loadConfigList(
            "transactions_configs.json",
            TransactionConfig::class.java,
            context
        )

        for (config in transactionConfigs) {
            paymentTypes[config.paymentType] = config.paymentType
        }
    }
    override fun setupUI() {
        val fields = mutableListOf<BaseFieldViewModel>()
        initPaymentTypes()


        ListPickerFieldViewModel.Builder()
            .activity(activity)
            .possibleValues(paymentTypes)
            .key("PaymentType")
            .title("Payment Type")
            .updatedFieldListener{ model -> updatePaymentType(model.data<String>()) }
            .build()
            .let(fields::add)
        TextFieldViewModel.Builder()
            .title("authKey")
            .key("AuthKey")
            .data("")
            .type(TextFieldViewModel.Type.Text)
            .hidden(false)
            .build()
            .let(fields::add)


        ButtonFieldViewModel.Builder()
            .title("Submit")
            .clickListener { _, _ ->  doTipAdjust()}
            .build()
            .let(fields::add)
        setForm(fields)
    }

    private fun doTipAdjust() {
        onStartProgress("Start")
        val paymentTypeField = findFieldByKey<BaseFieldViewModel>("PaymentType")
        val authKey = findFieldByKey<BaseFieldViewModel>("AuthKey").data<String>()
        val paymentTypeText = paymentTypeField.data<String>()
        var payType: DejavooPaymentType = DejavooPaymentType.Credit
        for (paymentType in DejavooPaymentType.values()) {
            if (paymentType.name.equals(paymentTypeText, ignoreCase = true)) {
                payType = paymentType
                break
            }
        }
        context?.let {
            InternalTerminal(requireContext().packageName).getTransactions(
                it,
                payType,
                1,
                100,
                authKey,
                object : IRequestCallback<DejavooResponseTransactionRecords> {
                    override fun onResponse(response: DejavooResponseTransactionRecords?) {
                        onStopProgress()
                        if (response != null && response.numRecords > 0) {
                            navigator(InternalNavigation::class.java).doTipAdjust(response)
                        }
                        //response?.records
                        showAlertMessage("Number of records: ${response?.numRecords}")
                    }

                    override fun onError(throwable: DejavooThrowable?) {
                        onStopProgress()
                    }
                })
        }
    }

    private fun updatePaymentType(paymentType: String?) {
        val transactionTypes: MutableMap<Any, String> = LinkedHashMap()
        var transType: String? = null
        val transactionConfig = getTransactionConfigForPaymentType(paymentType)!!
        for (transactionType in transactionConfig.transactionTypes) {
            if (transType == null) {
                transType = transactionType
            }
            transactionTypes[transactionType] = transactionType
        }


        // Configuration Fields
//        filterFields({ field: BaseFieldViewModel ->
//            !field.key().equals("PaymentType", ignoreCase = true) &&
//                    !field.key().equals("TransType", ignoreCase = true) &&
//                    !field.key().equals("Submit", ignoreCase = true)
//        }, { field: BaseFieldViewModel ->
//            if (transactionConfig.hasFieldByKey(field.key())) {
//                field.isHidden = false
//                field.setCode(CODE_NOSKIP_FIELD)
//            } else {
//                field.isHidden = true
//                field.setCode(CODE_SKIP_FIELD)
//            }
//        })
        refreshFields()
    }

    private fun getTransactionConfigForPaymentType(paymentType: String?): TransactionConfig? {
        for (config in transactionConfigs) {
            if (config.paymentType.equals(paymentType, ignoreCase = true)) {
                return config
            }
        }
        return null
    }

    private fun <T> getValueByKey(key: String?, defaultValue: T): T {
        val field = findFieldByKey<BaseFieldViewModel>(key)
        return if (field != null) {
            field.data()
        } else defaultValue
    }

    companion object {
        @JvmStatic
        val TAG = TipAdjustFragment::class.java.simpleName

        @JvmStatic
        fun newInstance(): TipAdjustFragment {
            return TipAdjustFragment()
        }
    }
}