package com.dvmms.sample.screens

import com.dvmms.dejapay.models.DejavooTransactionRecord
import com.dvmms.dejapay.models.DejavooTransactionResponse
import com.dvmms.dejapay.models.requests.DejavooResponseTransactionRecords

interface InternalNavigation {
    fun openZCTransaction()
    fun openZCSerialNumber()

    fun openDppInternalTransaction()
    fun openDppSPInTransaction()
    fun openInternalTerminalInfo()
    fun openDppResponse(response: DejavooTransactionResponse)
    fun openPrintout()
    fun openUserChoice()
    fun openGetTransactions()
    fun navigateToSettlementSample()
    fun navigateToTipAdjust()
    fun doTipAdjust(response: DejavooResponseTransactionRecords)
}