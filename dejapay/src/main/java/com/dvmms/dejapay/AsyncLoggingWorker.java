package com.dvmms.dejapay;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

public class AsyncLoggingWorker {
    private static final String TAG = "LogentriesAndroidLogger";

    private static final int MAX_QUEUE_POLL_TIME = 1000; // milliseconds.

    private static final int QUEUE_SIZE = 32768;

    public static final int LOG_LENGTH_LIMIT = 65536;

    private static final int MAX_NETWORK_FAILURES_ALLOWED = 3;
    private static final int MAX_RECONNECT_ATTEMPTS = 3;

    private static final String QUEUE_OVERFLOW = "Logentries Buffer Queue Overflow. Message Dropped!";

    private boolean started = false;

    private final SocketAppender appender;

    private final ArrayBlockingQueue<String> queue;

    private final LogStorage localStorage;

    public AsyncLoggingWorker(Context context) {
        queue = new ArrayBlockingQueue(QUEUE_SIZE);
        localStorage = new FileLogStorage(context, "ElasticLogStorage.log");
        appender = new SocketAppender();
        appender.start();
        started = true;
    }

    public void addLineToQueue(String line) {
        if (!this.started) {
            appender.start();
            started = true;
        }

        if (line.length() > LOG_LENGTH_LIMIT) {
            for (String logChunk : Utils.splitStringToChunks(line, LOG_LENGTH_LIMIT)) {
                tryOfferToQueue(logChunk);
            }

        } else {
            tryOfferToQueue(line);
        }
    }

    public void close(long queueFlushTimeout) {
        if (queueFlushTimeout < 0) {
            throw new IllegalArgumentException("queueFlushTimeout must be greater or equal to zero");
        }

        long now = System.currentTimeMillis();

        while (!queue.isEmpty()) {
            if (queueFlushTimeout != 0) {
                if (System.currentTimeMillis() - now >= queueFlushTimeout) {
                    // The timeout expired - need to stop the appender.
                    break;
                }
            }
        }
        appender.interrupt();
        started = false;
    }

    public void close() {
        close(0);
    }

    private static boolean checkTokenFormat(String token) {

        return Utils.checkValidUUID(token);
    }

    private void tryOfferToQueue(String line) throws RuntimeException {
        if (!queue.offer(line)) {
            Log.e(TAG, "The queue is full - will try to drop the oldest message in it.");
            queue.poll();
            if (!queue.offer(line)) {
                throw new RuntimeException(QUEUE_OVERFLOW);
            }
        }
    }

    private class SocketAppender extends Thread {
        private static final String LINE_SEP_REPLACER = "\u2028";

        private LogClient logClient;

        public SocketAppender() {
            super("Logentries Socket appender");

            setDaemon(true);
        }

        private void openConnection() {
            if (logClient == null) {
                logClient = new ElasticClient();
            }

            logClient.connect();
        }

        private boolean reopenConnection(int maxReConnectAttempts) throws InterruptedException, InstantiationException {
            if (maxReConnectAttempts < 0) {
                throw new IllegalArgumentException("maxReConnectAttempts value must be greater or equal to zero");
            }

            closeConnection();

            for (int attempt = 0; attempt < maxReConnectAttempts; ++attempt) {
                openConnection();
                return true;
            }

            return false;
        }


        private void closeConnection() {
            if (this.logClient != null) {
                this.logClient.close();
            }
        }

        private boolean tryUploadSavedLogs() {
            Queue<String> logs = new ArrayDeque();

            try {
                logs = localStorage.getAllLogs(false);
                for (String msg = logs.peek(); msg != null; msg = logs.peek()) {
                    logClient.send(msg.replace("\n", LINE_SEP_REPLACER));
                    logs.poll();
                }

                try {
                    localStorage.recreateLogFile();
                } catch (IOException ex) {
                    Log.e(TAG, ex.getMessage());
                }

                return true;

            } catch (IOException ioEx) {
                try {
                    localStorage.recreateLogFile();
                    for (String msg : logs) {
                        localStorage.put(msg);
                    }
                } catch (IOException ioEx2) {
                    Log.e(TAG, "Cannot save logs to the local storage - part of messages will be " +
                            "dropped! Error: " + ioEx2.getMessage());
                }
            }

            return false;
        }

        @Override
        public void run() {
            try {
                reopenConnection(MAX_RECONNECT_ATTEMPTS);

                Queue<String> prevSavedLogs = localStorage.getAllLogs(true);

                int numFailures = 0;
                boolean connectionIsBroken = false;
                String message = null;

                while (true) {
                    if (prevSavedLogs.isEmpty()) {
                        message = queue.poll(MAX_QUEUE_POLL_TIME, TimeUnit.MILLISECONDS);
                    } else {
                        message = prevSavedLogs.poll();
                    }

                    while (true) {
                        try {
                            if (connectionIsBroken && reopenConnection(MAX_RECONNECT_ATTEMPTS)) {
                                if (tryUploadSavedLogs()) {
                                    connectionIsBroken = false;
                                    numFailures = 0;
                                }
                            }

                            if (message != null) {
                                logClient.send(message.replace("\n", LINE_SEP_REPLACER));
                                message = null;
                            }

                        } catch (IOException e) {

                            if (numFailures >= MAX_NETWORK_FAILURES_ALLOWED) {
                                connectionIsBroken = true;
                                try {
                                    localStorage.put(message);
                                    message = null;
                                } catch (IOException ex) {
                                    Log.e(TAG, "Cannot save the log message to the local storage! Error: " +
                                            ex.getMessage());
                                }

                            } else {
                                ++numFailures;

                                reopenConnection(MAX_RECONNECT_ATTEMPTS);
                            }

                            continue;
                        }

                        break;
                    }
                }
            } catch (InterruptedException e) {

            } catch (InstantiationException e) {
                Log.e(TAG, "Cannot instantiate LogentriesClient due to improper configuration. Error: " + e.getMessage());

                String message = queue.poll();
                try {
                    while (message != null) {
                        localStorage.put(message);
                        message = queue.poll();
                    }
                } catch (IOException ex) {
                    Log.e(TAG, "Cannot save logs queue to the local storage - all log messages will be dropped! Error: " +
                            e.getMessage());
                }
            }

            closeConnection();
        }
    }
}
