package com.dvmms.dejapay;

/**
 * Created by Platon on 29.06.2016.
 */
interface IDejavooSerializer<T> {
    String serialize(T field);
}
