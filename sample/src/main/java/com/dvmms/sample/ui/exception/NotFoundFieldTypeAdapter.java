package com.dvmms.sample.ui.exception;

/**
 * Created by Platon on 01.07.2016.
 */
public class NotFoundFieldTypeAdapter extends RuntimeException {
    public NotFoundFieldTypeAdapter() {
    }

    public NotFoundFieldTypeAdapter(String detailMessage) {
        super(detailMessage);
    }

    public NotFoundFieldTypeAdapter(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public NotFoundFieldTypeAdapter(Throwable throwable) {
        super(throwable);
    }
}
