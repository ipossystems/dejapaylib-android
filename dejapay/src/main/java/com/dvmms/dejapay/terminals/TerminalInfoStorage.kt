package com.dvmms.dejapay.terminals

import android.content.Context

interface TerminalInfoStorage {
    var tpn: String
    var deviceSerialNumber: String
    var userName: String
    var terminalNumber: String
    var terminalName: String
}


class ZCreditTerminalInfoStorage(context: Context): TerminalInfoStorage {
    private val prefs = context.getSharedPreferences(
        ARG_TERMINAL_INFO_STORAGE,
        Context.MODE_PRIVATE
    )

    override var tpn: String
        get() = prefs.getString(ARG_TPN, "") ?: ""
        set(value) {
            prefs.edit()
                .putString(ARG_TPN, value)
                .apply()
        }
    override var deviceSerialNumber: String
        get() = prefs.getString(ARG_SERIAL, "") ?: ""
        set(value) {
            prefs.edit()
                .putString(ARG_SERIAL, value)
                .apply()
        }
    override var userName: String
        get() = prefs.getString(ARG_USER_NAME, "") ?: ""
        set(value) {
            prefs.edit()
                .putString(ARG_USER_NAME, value)
                .apply()
        }
    override var terminalNumber: String
        get() = prefs.getString(ARG_TERMINAL_NUM, "") ?: ""
        set(value) {
            prefs.edit()
                .putString(ARG_TERMINAL_NUM, value)
                .apply()
        }
    override var terminalName: String
        get() = prefs.getString(ARG_TERMINAL_NAME, "") ?: ""
        set(value) {
            prefs.edit()
                .putString(ARG_TERMINAL_NAME, value)
                .apply()
        }





    companion object {
        const val ARG_TERMINAL_INFO_STORAGE = "ARG_TERMINAL_INFO_STORAGE"
        const val ARG_TPN = "ARG_TPN"
        const val ARG_SERIAL = "ARG_SERIAL"
        const val ARG_USER_NAME = "ARG_USER_NAME"
        const val ARG_TERMINAL_NUM = "ARG_TERMINAL_NUM"
        const val ARG_TERMINAL_NAME = "ARG_TERMINAL_NAME"
    }
}