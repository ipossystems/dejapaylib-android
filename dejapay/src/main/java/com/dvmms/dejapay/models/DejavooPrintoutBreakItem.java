package com.dvmms.dejapay.models;

/**
 * Created by pmalyugin on 10/04/2017.
 */

public class DejavooPrintoutBreakItem implements IDejavooPrintoutItem {
    public DejavooPrintoutBreakItem() { }

    @Override
    public String data() {
        return "<BR/>";
    }
}
