package com.dvmms.sample.ui.fields;

/**
 * Created by Platon on 04.02.2016.
 */
public interface IErrorableView {
    void setErrorMessage(String error);
}
