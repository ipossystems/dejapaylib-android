package com.dvmms.dejapay.models;

/**
 * User Input request
 */
public class DejavooUserInputRequest {
    /**
     * Supported User Input type
     */
    public enum Type {
        /**
         * Only alpha chars
         */
        AlphaOnly("a"),
        /**
         * Only numeric chars
         */
        NumericOnly("n"),
        /**
         * Alphanumeric chars
         */
        AlphaNumeric("an"),
        /**
         * Currency
         */
        Currency("$n"),
        /**
         * Information
         */
        InformationOnly("i");

        private String value;

        public String getValue() {
            return value;
        }

        Type(String value) {
            this.value = value;
        }
    }

    /**
     * Phone Input mask
     */
    public static final String MASK_PHONE_INPUT = "(NNN)_NNN-NNNN";
    /**
     * Zip Code mask
     */
    public static final String MASK_ZIP_CODE = "NNNNN-NNNN";
    /**
     * Social Security mask
     */
    public static final String MASK_SOCIAL_SECURITY = "NNN-NN-NNNN";

    private String registerId;
    private String authKey;
    private String title;
    private Type type;
    private String defaultValue;
    private Boolean hide;
    private String mask;
    private Integer timeout;
    private Integer inputLength;

    /**
     * Alpha numeric reference field.
     * @return Register Id
     */
    public String getRegisterId() {
        return registerId;
    }

    /**
     * Alpha numeric reference field.
     * @param registerId Register Id
     */
    public void setRegisterId(String registerId) {
        this.registerId = registerId;
    }

    /**
     * Get authentication key
     * @return authentication key
     */
    public String getAuthKey() {
        return authKey;
    }

    /**
     * Set authentication key
     * @param authKey authentication key
     */
    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    /**
     * Get title prompt
     * @return title prompt
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set title prompt
     * @param title title prompt
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Get input type
     * @return input type
     * @see Type
     */
    public Type getType() {
        return type;
    }

    /**
     * Set input type
     * @param type input type
     * @see Type
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     * Get default value input
     * @return default value
     */
    public String getDefaultValue() {
        return defaultValue;
    }

    /**
     * Set default value input
     * @param defaultValue default value
     */
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * Hide input with "*"
     * @return true = hide input, false = no
     */
    public Boolean getHide() {
        return hide;
    }

    /**
     * Set hide input with "*"
     * @param hide true = hide input, false = no
     */
    public void setHide(Boolean hide) {
        this.hide = hide;
    }

    /**
     * Get specifies input mask
     * @return input mask
     */
    public String getMask() {
        return mask;
    }

    /**
     * Set input mask
     * @param mask input mask
     */
    public void setMask(String mask) {
        this.mask = mask;
    }

    /**
     * Get timeout prompt
     * @return timeout prompt
     */
    public Integer getTimeout() {
        return timeout;
    }

    /**
     * Set timeout
     * @param timeout timeout prompt
     */
    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    /**
     * Get maximum input length
     * @return input length
     */
    public Integer getInputLength() {
        return inputLength;
    }

    /**
     * Set maximum input length
     * @param inputLength input length
     */
    public void setInputLength(Integer inputLength) {
        this.inputLength = inputLength;
    }
}
