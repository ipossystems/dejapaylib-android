package com.dvmms.dejapay

import okhttp3.MediaType.Companion.toMediaType
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject

class ElasticRequest(
    private val protocol: String,
    private val server: String,
    private val port: Int,
    private val token: String
) {
    fun create(message: String): Request {
        val json = JSONObject(message)
        val jsonType = "application/json; charset=utf-8".toMediaType()
        val body = json.toString().toRequestBody(jsonType)
        return Request.Builder()
            .url("$protocol://$server:$port")
            .addHeader("Authorization", "Basic $token")
            .post(body)
            .build()
    }
}