package com.dvmms.dejapay.transports;

import com.dvmms.dejapay.exception.DejavooRequestCancelledException;
import com.dvmms.dejapay.exception.DejavooRequestTimeoutExpiredException;
import com.dvmms.dejapay.exception.DejavooThrowable;
import com.dvmms.dejapay.exception.DejavooTransportNotConnectedException;

import java.net.ConnectException;
import java.net.NoRouteToHostException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.concurrent.CancellationException;

final class DejavooExceptionAdapter {
    static DejavooThrowable fromException(Throwable t) {
        if (t instanceof NoRouteToHostException) {
            return new DejavooTransportNotConnectedException(t);
        } else if (t instanceof ConnectException) {
            return new DejavooTransportNotConnectedException(t);
        } else if (t instanceof SocketException) {
            return new DejavooRequestTimeoutExpiredException(t);
        } else if (t instanceof UnknownHostException) {
            return new DejavooTransportNotConnectedException(t);
        } else if (t instanceof CancellationException) {
            return new DejavooRequestCancelledException(t);
        } else {
            return new DejavooThrowable(t);
        }
    }
}
