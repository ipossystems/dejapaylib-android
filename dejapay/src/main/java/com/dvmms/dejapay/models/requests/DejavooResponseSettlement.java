package com.dvmms.dejapay.models.requests;

import android.os.Parcel;
import android.os.Parcelable;

import com.dvmms.dejapay.models.DejavooPaymentType;
import com.dvmms.dejapay.models.DejavooResponseExtData;
import com.dvmms.dejapay.models.DejavooTransactionResponse;

import java.util.ArrayList;
import java.util.List;

public class DejavooResponseSettlement implements Parcelable {
    private final boolean success;
    private final DejavooTransactionResponse transactionResponse;
    private final String errorMessage;
    private final List<DejavooResponseExtData> extDatas;

    private DejavooResponseSettlement(Builder builder) {
        this.transactionResponse = builder.transactionResponse;
        this.success = builder.success;
        this.errorMessage = builder.errorMessage;
        this.extDatas = builder.extDatas;
    }

    public DejavooTransactionResponse getTransactionResponse() {
        return transactionResponse;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public List<DejavooPaymentType> getPaymentTypes() {
        List<DejavooPaymentType> paymentTypes = new ArrayList<>();
        for (DejavooResponseExtData extData : extDatas) {
            if (extData.getApp() != null && !extData.getApp().isEmpty()) {
                DejavooPaymentType paymentType = DejavooPaymentType.fromApp(extData.getApp());

                if (paymentType != DejavooPaymentType.Unknown) {
                    paymentTypes.add(paymentType);
                }
            }
        }

        return paymentTypes;
    }

    public boolean isSuccessPaymentType(DejavooPaymentType paymentType) {
        for (DejavooResponseExtData extData : extDatas) {
            if (extData.getApp() != null && !extData.getApp().isEmpty()) {
                DejavooPaymentType extPaymentType = DejavooPaymentType.fromApp(extData.getApp());

                if (extPaymentType == paymentType) {
                     String resultCode = extData.getData().get("ResultCode");
                     return (resultCode != null && resultCode.equalsIgnoreCase("0"));
                }
            }
        }

        return false;
    }

    public String getResponseMessage(DejavooPaymentType paymentType) {
        for (DejavooResponseExtData extData : extDatas) {
            if (extData.getApp() != null && !extData.getApp().isEmpty()) {
                DejavooPaymentType extPaymentType = DejavooPaymentType.fromApp(extData.getApp());

                if (extPaymentType == paymentType) {
                    String rsp = extData.getData().get("Rsp");

                    if (rsp != null) {
                        return rsp;
                    }

                    return "";
                }
            }
        }

        return "";
    }

    public int getBatchNumber(DejavooPaymentType paymentType) {
        for (DejavooResponseExtData extData : extDatas) {
            if (extData.getApp() != null && !extData.getApp().isEmpty()) {
                DejavooPaymentType extPaymentType = DejavooPaymentType.fromApp(extData.getApp());

                if (extPaymentType == paymentType) {
                    String rsp = extData.getData().get("BatchNum");

                    if (rsp != null) {
                        try {
                            return Integer.parseInt(rsp);
                        } catch (NumberFormatException e) {
                            return -1;
                        }
                    }

                    return -1;
                }
            }
        }

        return -1;
    }


    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private boolean success = false;
//        private Map<DejavooTransactionResponse.ReceiptType, String> receipts;
        private DejavooTransactionResponse transactionResponse;
        private String errorMessage = "";
        private List<DejavooResponseExtData> extDatas;

        private Builder() { }

        public Builder setTransactionResponse(DejavooTransactionResponse transactionResponse) {
            this.transactionResponse = transactionResponse;
            return this;
        }

        public Builder setSuccess(boolean success) {
            this.success = success;
            return this;
        }

        public Builder setErrorMessage(String message) {
            this.errorMessage = message != null ? message : "";
            return this;
        }

        public Builder setExtData(List<DejavooResponseExtData> extDatas) {
            this.extDatas = (extDatas != null) ? extDatas : new ArrayList<DejavooResponseExtData>();
            return this;
        }

        public DejavooResponseSettlement build() {
            return new DejavooResponseSettlement(this);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.success ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.transactionResponse, flags);
        dest.writeString(this.errorMessage);
        dest.writeList(this.extDatas);
    }

    protected DejavooResponseSettlement(Parcel in) {
        this.success = in.readByte() != 0;
        this.transactionResponse = in.readParcelable(DejavooTransactionResponse.class.getClassLoader());
        this.errorMessage = in.readString();
        this.extDatas = new ArrayList<DejavooResponseExtData>();
        in.readList(this.extDatas, DejavooResponseExtData.class.getClassLoader());
    }

    public static final Creator<DejavooResponseSettlement> CREATOR = new Creator<DejavooResponseSettlement>() {
        @Override
        public DejavooResponseSettlement createFromParcel(Parcel source) {
            return new DejavooResponseSettlement(source);
        }

        @Override
        public DejavooResponseSettlement[] newArray(int size) {
            return new DejavooResponseSettlement[size];
        }
    };
}
