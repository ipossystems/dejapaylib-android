package com.dvmms.dejapay

interface LogFormattedData {
    fun format(position: LogPosition, level: Int, tag: String, message: String, appId: String): LogData
}