package com.dvmms.dejapay;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Platon on 30.06.2016.
 */
class ValidatorContext {
    private List<String> errors;

    public ValidatorContext() {
        errors = new ArrayList<>();
    }

    public void addError(String message, Object... args) {
        if (message == null) {
            return;
        }
        if (message.length() == 0) {
            return;
        }

        if (args.length > 0) {
            message = String.format(message, args);
        }

        errors.add(message);
    }

    public List<String> getErrors() {
        return errors;
    }
}
