package com.dvmms.sample.screens

import android.os.Build
import com.dvmms.dejapay.GetTransactionRecordsConvertor
import com.dvmms.dejapay.IRequestCallback
import com.dvmms.dejapay.exception.DejavooThrowable
import com.dvmms.dejapay.models.DejavooPaymentType
import com.dvmms.dejapay.models.DejavooTransactionRequest
import com.dvmms.dejapay.models.DejavooTransactionResponse
import com.dvmms.dejapay.models.DejavooTransactionType
import com.dvmms.dejapay.terminals.InternalTerminal
import com.dvmms.sample.ui.fields.BaseFieldViewModel
import com.dvmms.sample.ui.fields.ButtonFieldViewModel
import com.dvmms.sample.ui.fragments.AbstractFieldsFragment
import java.net.URLDecoder

class InternalSettlementFragment: AbstractFieldsFragment() {
    override fun setupUI() {
        val fields = mutableListOf<BaseFieldViewModel>()
        
        ButtonFieldViewModel.Builder()
            .title("Settle batch")
            .clickListener { _, _ ->  doSettle()}
            .build()
            .let(fields::add)
        setForm(fields)
        
    }

    private fun doSettle() {
        onStartProgress("Start")
        val request: DejavooTransactionRequest = DejavooTransactionRequest()
            .apply {
                paymentType = DejavooPaymentType.Batch
                transactionType = DejavooTransactionType.Settle
                parameter = "Close"
                receiptType = DejavooTransactionRequest.ReceiptType.Batch
                printReceipt = DejavooTransactionRequest.ReceiptType.Both
                referenceId = "refId"
                authenticationCode = "f9eGebRfss"
                this.isSignatureCapable = true
            }

        context?.let {
            InternalTerminal(requireContext().packageName).commitTransaction(
                it, request,
                object : IRequestCallback<DejavooTransactionResponse> {
                    override fun onResponse(response: DejavooTransactionResponse?) {
                        onStopProgress()
                        response?.let { it1 ->
                            navigator(InternalNavigation::class.java).openDppResponse(
                                it1
                            )
                        }
                    }

                    override fun onError(throwable: DejavooThrowable?) {
                        onStopProgress()
                        showAlertMessage(throwable?.message)
                    }
                })
        }
    }
    companion object {
        @JvmStatic
        val TAG = InternalSettlementFragment::class.java.simpleName

        @JvmStatic
        fun newInstance(): InternalSettlementFragment {
            return InternalSettlementFragment()
        }
    }
}