package com.dvmms.dejapay.ktx

import android.util.Base64
import java.nio.charset.Charset

fun String.toBase64(): String {
    return Base64.encodeToString(this.toByteArray(), Base64.NO_WRAP)
}

fun String.fromBase64(): String {
    return Base64.decode(this, Base64.NO_WRAP).toString(Charset.forName("UTF-8"))
}