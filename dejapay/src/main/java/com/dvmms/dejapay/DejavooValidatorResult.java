package com.dvmms.dejapay;

import java.util.List;

/**
 * Created by Platon on 30.06.2016.
 */
class DejavooValidatorResult {
    private final List<String> errors;

    public DejavooValidatorResult(List<String> errors) {
        this.errors = errors;
    }

    public List<String> getErrors() {
        return errors;
    }

    public boolean isValid() {
        return (errors.size() == 0);
    }
}
