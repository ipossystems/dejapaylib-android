package com.dvmms.dejapay.models

class DejavooPrintoutFormattedText(
    private val text: String,
    private val large: Boolean,
    private val inverted: Boolean,
    private val condensed: Boolean,
    private val bold: Boolean
): IDejavooPrintoutItem {
    override fun data(): String {
        var data = ""
        var separableText = text
        separableText = separableText.replace("\n", "" +
                "")

        while (separableText.length / 24 > 0) {
            var separatedText = separableText.substring(0, 24)
            separableText = separableText.substring(24)

            if (separatedText.isNotEmpty()) {
                if (isLineStartsWithSpace(separatedText) && isLineEndsWithSpace(separatedText)) {
                    if (isEmptyLine(separatedText)) {
                        data += "<L> </L><R> </R>\n"
                    } else {
                        data += "<C>${escapeText(separatedText)}</C>\n"
                    }
                }
                if (isLineStartsWithSpace(separatedText) && !isLineEndsWithSpace(separatedText)) {
                    data += "<L>${escapeText(separatedText)}</L>\n"
                }
                if (!isLineStartsWithSpace(separatedText) && isLineEndsWithSpace(separatedText)) {
                    data += "<R>${escapeText(separatedText)}</R>\n"
                }
                if (!isLineStartsWithSpace(separatedText) && !isLineEndsWithSpace(separatedText)) {
                    if (separatedText.contains("  ")) {
                        val indexBeginSpace = separatedText.indexOf("  ")
                        val indexEndSpace = separatedText.lastIndexOf("  ")
                        val newT = separatedText.substring(0, indexBeginSpace)
                        val newR = separatedText.substring(indexEndSpace + 1)
                        data += "<L>${escapeText(newT.substringBefore("  "))}</L>"
                        data += "<R>${escapeText(newR.substringAfterLast("  "))}</R>\n"
                    } else {
                        data += "<L>${escapeText(separatedText.substringAfterLast("  "))}</L>\n"
                    }
                }
            }
        }

        if (bold) {
            data = "<B>$data</B>"
        }
        if (large) {
            data = "<LG>$data</LG>"
        }
        if (inverted) {
            data = "<INV>$data</INV>"
        }
        if (condensed) {
            data = "<CD>$data</CD>"
        }

        return data
    }

    private fun escapeText(text: String): String {
        return text.replace("#", "&#35;").replace("%", "&#37;")
            .replace("┃", "")
    }


    class Builder {
        var text = ""
        var large = false
        var inverted = false
        var condensed = false
        var bold = false
        fun setText(text: String): Builder {
            this.text = text
            return this
        }

        fun setLarge(large: Boolean): Builder {
            this.large = large
            return this
        }

        fun setInverted(inverted: Boolean): Builder {
            this.inverted = inverted
            return this
        }

        fun setCondensed(condensed: Boolean): Builder {
            this.condensed = condensed
            return this
        }

        fun setBold(bold: Boolean): Builder {
            this.bold = bold
            return this
        }

        fun build(): DejavooPrintoutFormattedText {
            return DejavooPrintoutFormattedText(text, large, inverted, condensed, bold)
        }
    }

    companion object {
        const val emptyLine = "                        "
        fun isEmptyLine(line: String): Boolean = line == emptyLine
        fun isLineStartsWithSpace(line: String) = line.startsWith(" ")
        fun isLineEndsWithSpace(line: String) = line.endsWith(" ")

        fun builder(): Builder {
            return Builder()
        }
    }
}