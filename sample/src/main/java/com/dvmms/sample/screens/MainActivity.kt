package com.dvmms.sample.screens

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.dvmms.dejapay.models.DejavooTransactionRecord
import com.dvmms.dejapay.models.DejavooTransactionResponse
import com.dvmms.dejapay.models.requests.DejavooResponseTransactionRecords
import com.dvmms.sample.R
import com.dvmms.sample.screens.navigators.MainNavigatorContract
import com.dvmms.sample.ui.fragments.PaymentReportFragment
import com.dvmms.sample.ui.fragments.SettingsFragment
import com.dvmms.sample.ui.fragments.SettingsFragment.ISettingsFragmentListener

class MainActivity : AppCompatActivity(), MainNavigatorContract, ISettingsFragmentListener, InternalNavigation {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, InternalListFragment.newInstance(), InternalListFragment.TAG)
            addToBackStack(InternalListFragment.TAG)
            commitAllowingStateLoss()
        }

        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        0)
            }
        }
    }

    //region MainNavigatorContract
    override fun navigateToSaleSample() {
        startActivity(SaleSampleActivity.getCallingIntent(this))
    }

    override fun navigateToSettlementSample() {
        //startActivity(SettlementSampleActivity.getCallingIntent(this))
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, InternalSettlementFragment.newInstance(), InternalSettlementFragment.TAG)
            addToBackStack(InternalSettlementFragment.TAG)
            commitAllowingStateLoss()
        }
    }

    override fun navigateToTipAdjust() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, TipAdjustFragment.newInstance(), TipAdjustFragment.TAG)
            addToBackStack(TipAdjustFragment.TAG)
            commitAllowingStateLoss()
        }
    }

    override fun doTipAdjust(response: DejavooResponseTransactionRecords) {
        val record = response.records.last()

        supportFragmentManager.beginTransaction().apply {
            replace(
                R.id.flContainer,
                AdjustTipForRecordFragment.newInstance(
                    "DvCredit",
                    record.totalAmount,
                    record.refId,
                    record.invoiceId
                ),
                AdjustTipForRecordFragment.TAG)
            addToBackStack(AdjustTipForRecordFragment.TAG)
            commitAllowingStateLoss()
        }
    }

    override fun navigateToPlainRequestSample() {
        startActivity(PlainRequestActivity.getCallingIntent(this))
    }

    override fun navigateToSettings() {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.flContainer, SettingsFragment.newInstance(), SettingsFragment.TAG)
                .addToBackStack(SettingsFragment.TAG)
                .commitAllowingStateLoss()
    }

    override fun navigateToGetTransactionsSample() {
        startActivity(TransactionListSampleActivity.getCallingIntent(this))
    }

    override fun navigateToGetCardSample() {
        startActivity(GetCardSampleActivity.getCallingIntent(this))
    }

    override fun navigateToGetStatusSample() {
        startActivity(GetStatusSampleActivity.getCallingIntent(this))
    }

    override fun openZCTransaction() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, ZCreditInternalTransactionFragment.newInstance(), ZCreditInternalTransactionFragment.TAG)
            addToBackStack(ZCreditInternalTransactionFragment.TAG)
            commitAllowingStateLoss()
        }
    }

    override fun openZCSerialNumber() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, ZCreditInternalTerminalInfoFragment.newInstance(), ZCreditInternalTerminalInfoFragment.TAG)
            addToBackStack(ZCreditInternalTerminalInfoFragment.TAG)
            commitAllowingStateLoss()
        }
    }

    override fun openDppInternalTransaction() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, DppInternalTransactionFragment.newInstance(), DppInternalTransactionFragment.TAG)
            addToBackStack(DppInternalTransactionFragment.TAG)
            commitAllowingStateLoss()
        }
    }

    override fun openDppSPInTransaction() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, DppSPInTransactionFragment.newInstance(), DppSPInTransactionFragment.TAG)
            addToBackStack(DppSPInTransactionFragment.TAG)
            commitAllowingStateLoss()
        }
    }

    override fun openInternalTerminalInfo() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, InternalTerminalInfoFragment.newInstance(), InternalTerminalInfoFragment.TAG)
            addToBackStack(InternalTerminalInfoFragment.TAG)
            commitAllowingStateLoss()
        }
    }

    override fun openDppResponse(response: DejavooTransactionResponse) {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, PaymentReportFragment.newInstance(response), PaymentReportFragment.TAG)
            addToBackStack(PaymentReportFragment.TAG)
            commitAllowingStateLoss()
        }
    }



    override fun openPrintout() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, InternalPrintoutFragment.newInstance(), InternalPrintoutFragment.TAG)
            addToBackStack(InternalPrintoutFragment.TAG)
            commitAllowingStateLoss()
        }
    }

    override fun openUserChoice() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, InternalUserChoiceFragment.newInstance(), InternalUserChoiceFragment.TAG)
            addToBackStack(InternalUserChoiceFragment.TAG)
            commitAllowingStateLoss()
        }
    }

    override fun openGetTransactions() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, InternalGetTransactionsFragment.newInstance(), InternalGetTransactionsFragment.TAG)
            addToBackStack(InternalGetTransactionsFragment.TAG)
            commitAllowingStateLoss()
        }
    }

    override fun onRequestBluetoothPermissions(): Boolean {
        return if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION),
                    1)
            false
        }
    }
}