package com.dvmms.sample.ui.formatter;

import java.util.regex.Pattern;

/**
 * Created by Platon on 13.01.2016.
 */
public class RegexpFieldValidator implements IValidator<String> {
    private final String format;
    private final Pattern pattern;
    private final boolean nullable;

    public RegexpFieldValidator(String format) {
       this(format, false);
    }

    public RegexpFieldValidator(String format, boolean nullable) {
        this.format = format;
        this.pattern = Pattern.compile(format);
        this.nullable = nullable;
    }

    @Override
    public boolean validate(String value) {
        if (nullable && (value == null || value.isEmpty())) {
            return true;
        }

        if (value == null) return false;
        return pattern.matcher(value).matches();
    }

    @Override
    public String toString() {
        return "RegexpFieldValidator{" +
                "format='" + format + '\'' +
                '}';
    }
}
