package com.dvmms.sample.ui.formatter;

/**
 * Created by Platon on 16.01.2016.
 */
public class PhoneFieldFormatter implements IFieldFormatter {
    @Override
    public String format(Object value) {
        return (String)value;
    }

    @Override
    public Object parse(String value) {
        return value;
    }

    @Override
    public String transformText(String oldValue, String value) {
        String prValue = value.replaceAll("[\\(\\) \\-]", "");

        if (prValue.isEmpty()) {
            return "";
        }

        int pos = 0;
        String newValue = "(";

        newValue += prValue.substring(pos, Math.min(pos + 3, prValue.length()));
        pos += 3;

        if (pos >= prValue.length()) {
            return newValue;
        }

        newValue += ") " + prValue.substring(pos, Math.min(pos+3, prValue.length()));
        pos += 3;

        if (pos >= prValue.length()) {
            return newValue;
        }

        newValue += "-" + prValue.substring(pos, Math.min(pos+4, prValue.length()));

        return newValue;
    }
}
