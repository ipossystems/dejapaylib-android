package com.dvmms.dejapay

import java.io.IOException

interface LogClient {
    fun connect()
    @Throws(IOException::class)
    fun send(message: String?)
    fun close()
}