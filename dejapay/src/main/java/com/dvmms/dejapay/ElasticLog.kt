package com.dvmms.dejapay

import android.os.Build
import org.json.JSONObject

class ElasticLog(
    private val env: String,
    private val file: String,
    private val method: String,
    private val line: String,
    private val level: String,
    private val id: String,
    private val message: String,
    private val time: String,
    private val version: String,
    private val posAppId: String
) : LogMessage {
    override fun toString(): String {
        val jsonObject = JSONObject().apply {
            put("app", "DejaPay library")
            put("env", env)
            put("platform", "android")
            put("file", file)
            put("method", method)
            put("line", line)
            put("level", level)
            put("id", id)
            put("message", message)
            put("serialNumber", Build.SERIAL)
            put("time", time)
            put("version", version)
            put("posAppId", posAppId)
        }
        return jsonObject.toString()
    }
}