package com.dvmms.dejapay.transports;

public interface IWebCallback<T> {
    void success(T o);
    void failure(Throwable t);
}
