package com.dvmms.dejapay.transports;

public interface IWebClientRequest<T> extends ICancelable, IRunnable<IWebCallback<WebResponse<T>>> {

}
