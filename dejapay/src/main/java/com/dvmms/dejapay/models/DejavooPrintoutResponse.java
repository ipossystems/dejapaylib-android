package com.dvmms.dejapay.models;

/**
 * PrintOut response
 *
 */
public class DejavooPrintoutResponse {
    private String responseMessage;

    /**
     * Result Code printout
     */
    public enum ResultCode {
        Succeded, Failed
    }
    private String registerId;
    private String message;

    ResultCode resultCode;

    public ResultCode getResultCode() {
        return resultCode;
    }

    public void setResultCode(ResultCode resultCode) {
        this.resultCode = resultCode;
    }

    public String getRegisterId() {
        return registerId;
    }

    public void setRegisterId(String registerId) {
        this.registerId = registerId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getResponseMessage() {
        return responseMessage;
    }
}
