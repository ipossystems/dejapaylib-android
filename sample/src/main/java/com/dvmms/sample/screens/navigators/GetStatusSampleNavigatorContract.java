package com.dvmms.sample.screens.navigators;

import com.dvmms.dejapay.models.DejavooTransactionResponse;

public interface GetStatusSampleNavigatorContract {
    void onShowTransactionReport(DejavooTransactionResponse response);
}
