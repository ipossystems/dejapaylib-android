package com.dvmms.sample.ui.fields;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.LinearLayout;

import com.dvmms.sample.ui.formatter.IValidator;

import java.util.Map;

/**
 * Created by Platon on 06.01.2016.
 */
public abstract class AbstractFieldView <T extends BaseFieldViewModel> extends LinearLayout implements IFieldView<T>, IErrorableView {
    protected T fieldViewModel;
    protected static final String TAG = "FieldViewModel";
    public AbstractFieldView(Context context) {
        super(context);
        initialize(context);
    }

    public AbstractFieldView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    public AbstractFieldView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context);
    }

    private void initialize(Context context) {
        Log.d(TAG, String.format("Initialize UI field: %s", toString()));
        initializeUI(context);
    }

    protected abstract void initializeUI(Context context);
    protected abstract void initializeViewModel();

    @Override
    public void setFieldViewModel(T fieldViewModel) {
        this.fieldViewModel = null;
        this.fieldViewModel = fieldViewModel;
        Log.d(TAG, String.format("Initialize view model field: %s", toString()));
        initializeViewModel();
    }

    protected void validate() {
        for (Map.Entry<IValidator, IInvalidListener> entry : fieldViewModel.validators().entrySet()) {
            if (entry.getKey() != null && !entry.getKey().validate(fieldViewModel.data())) {
                if (fieldViewModel.shownValidationError()) {
                    entry.getValue().onInvalid(this);
                }
                return;
            }
        }


        setErrorMessage(null);
    }



}
