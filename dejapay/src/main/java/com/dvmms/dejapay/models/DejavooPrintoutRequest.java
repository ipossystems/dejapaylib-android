package com.dvmms.dejapay.models;

import java.util.List;

/**
 * PrintOut Request
 */
public class DejavooPrintoutRequest {
    private final int width;
    private final List<IDejavooPrintoutItem> items;
    private String authenticationKey;
    private String registerId;
    private String tpn;

    public DejavooPrintoutRequest(int width, List<IDejavooPrintoutItem> items) {
        this.width = width;
        this.items = items;
    }

    public DejavooPrintoutRequest(List<IDejavooPrintoutItem> items) {
        this(24, items);
    }

    public List<IDejavooPrintoutItem> getItems() {
        return items;
    }

    public void addItem(IDejavooPrintoutItem item) {
        items.add(item);
    }

    public int getWidth() {
        return width;
    }

    public String getAuthenticationKey() {
        return authenticationKey;
    }

    public void setAuthenticationKey(String authenticationKey) {
        this.authenticationKey = authenticationKey;
    }

    public String getRegisterId() {
        return registerId;
    }

    public void setRegisterId(String registerId) {
        this.registerId = registerId;
    }

    public String getTpn() {
        return tpn;
    }

    public void setTpn(String tpn) {
        this.tpn = tpn;
    }
}
