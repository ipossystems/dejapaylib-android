package com.dvmms.dejapay.terminals

import android.content.Context
import android.util.Log
import com.dvmms.dejapay.*
import com.dvmms.dejapay.exception.DejavooInvalidResponseException
import com.dvmms.dejapay.exception.DejavooThrowable
import com.dvmms.dejapay.models.*
import com.dvmms.dejapay.models.requests.DejavooRequestTransactionRecord
import com.dvmms.dejapay.models.requests.DejavooResponseTransactionRecords

class AuraInternalTerminal {
    private val transactionConverter = TransactionConvertor()
    private val printoutConverter = PrintoutConvertor()

    val serialNumber: String = android.os.Build.SERIAL

    fun commitTransaction(context: Context, request: DejavooTransactionRequest, callback: IRequestCallback<DejavooTransactionResponse>) {
        val xml = transactionConverter.serialize(request)

        if (xml.isNotEmpty()) {
            DvPaymentActivity.setCallback(object : TransactionCallback {
                override fun onSuccess(response: String) {
                    Log.d("InternalTerminal", "Response $response")
                    callback.onResponse(transactionConverter.deserialize(response))
                }

                override fun onFailed(t: Throwable) {
                    Log.e("InternalTerminal", "Failed", t)
                    callback.onError(DejavooThrowable(t))
                }
            })
            context.startActivity(DvPaymentActivity.getCallingIntent(context, PaymentApp.Aura, xml))
        } else {
            callback.onError(DejavooThrowable("Can not parse request"))
        }
    }

    fun print(context: Context, request: DejavooPrintoutRequest, callback: IRequestCallback<DejavooPrintoutResponse>) {
        val xml = printoutConverter.serialize(request)

        if (xml.isNotEmpty()) {
            DvPaymentActivity.setCallback(object : TransactionCallback {
                override fun onSuccess(response: String) {
                    Log.d("InternalTerminal", "Response $response")
                    callback.onResponse(printoutConverter.deserialize(response))
                }

                override fun onFailed(t: Throwable) {
                    Log.e("InternalTerminal", "Failed", t)
                    callback.onError(DejavooThrowable(t))
                }
            })
            context.startActivity(DvPaymentActivity.getCallingIntent(context, PaymentApp.Aura, xml))
        } else {
            callback.onError(DejavooThrowable("Can not parse request"))
        }
    }

    fun getTransaction(
        context: Context,
        record: DejavooRequestTransactionRecord,
        callback: IRequestCallback<DejavooTransactionResponse>
    ) {
        val request = DejavooTransactionRequest()
        request.paymentType = record.paymentType
        request.transactionType = DejavooTransactionType.Status
        request.referenceId = record.refId
        request.isSignatureCapable = true
        request.invoiceNumber = record.invoiceId
        request.receiptType = record.returnReceipt
        request.printReceipt = record.printReceipt
        request.setOnBypassSignature()
        if (record.transNum > 0) {
            request.transNum = record.transNum
        }
        commitTransaction(context, request, callback)
    }

    fun showOptions(context: Context, request: DejavooUserChoiceRequest, callback: IRequestCallback<DejavooUserChoiceResponse>) {
        val converter = UserChoiceConvertor()

        val packet = converter.serialize(request)

        DvPaymentActivity.setCallback(object : TransactionCallback {
            override fun onSuccess(data: String) {
                Log.d("InternalTerminal", "Response $data")
                callback.onResponse(converter.deserialize(data))
            }

            override fun onFailed(t: Throwable) {
                Log.e("InternalTerminal", "Failed", t)
                callback.onError(DejavooThrowable(t))
            }
        })
        context.startActivity(DvPaymentActivity.getCallingIntent(context, PaymentApp.Aura, packet))
    }

    fun getTransactions(
        context: Context,
        paymentType: DejavooPaymentType?,
        offset: Int,
        limit: Int,
        callback: IRequestCallback<DejavooResponseTransactionRecords>
    ) {
        val converter = GetTransactionRecordsConvertor()

        val packet: String = converter.serialize(
                "",
                paymentType,
                "1",
                "",
                offset,
                offset + limit - 1
        )

//        val packet = RequestDocument("printout").apply {
//            add("version", 1)
//            add("provider", "vega")
//
//            addFields(fields)
//
//        }.asRequest()

        DvPaymentActivity.setCallback(object : TransactionCallback {
            override fun onSuccess(data: String) {
                Log.d("InternalTerminal", "Response $data")
                val response: DejavooResponseTransactionRecords = converter.deserialize(data)

                if (response != null) {
                    var start = offset
                    for (record in response.records) {
                        record.transNumber = start
                        start++
                    }
                    callback.onResponse(response)
                } else {
                    callback.onError(DejavooInvalidResponseException())
                }
            }

            override fun onFailed(t: Throwable) {
                Log.e("InternalTerminal", "Failed", t)
                callback.onError(DejavooThrowable(t))
            }
        })
        context.startActivity(DvPaymentActivity.getCallingIntent(context, PaymentApp.Aura, packet))
    }

    fun isInstalled(context: Context): Boolean {
        return DvPaymentActivity.isAvailable(context, PaymentApp.Aura)
    }
}