package com.dvmms.sample.ui.fields;

/**
 * Created by Platon on 06.01.2016.
 */
public interface IFieldViewModelListener {
    void onFieldViewModelUpdated(BaseFieldViewModel fieldViewModel);
}
