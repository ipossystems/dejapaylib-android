package com.dvmms.sample.screens

import android.os.Build
import android.util.Log
import com.dvmms.dejapay.IRequestCallback
import com.dvmms.dejapay.exception.DejavooThrowable
import com.dvmms.dejapay.terminals.ZCreditInternalTerminal
import com.dvmms.dejapay.terminals.ZCreditTerminalInfo
import com.dvmms.sample.ui.fields.BaseFieldViewModel
import com.dvmms.sample.ui.fields.LabelWithHeaderFieldViewModel
import com.dvmms.sample.ui.fragments.AbstractFieldsFragment

class ZCreditInternalTerminalInfoFragment : AbstractFieldsFragment() {
    private lateinit var navigation: InternalNavigation

    override fun setupUI() {
        navigation = navigator(InternalNavigation::class.java)

        context?.let {
            ZCreditInternalTerminal(requireContext().packageName).getTerminalInfo(it,
                    object : IRequestCallback<ZCreditTerminalInfo> {
                        override fun onResponse(response: ZCreditTerminalInfo) {
                            Log.e("HUEHUE", "TPN: ${response.tpn}")
                            setFieldsWithData(response)
                        }

                        override fun onError(throwable: DejavooThrowable?) {
                            Log.e("TEST", "Callback error has happened")
                        }
                    }
            )
        }
    }

    private fun setFieldsWithData(data: ZCreditTerminalInfo) {
        val fields = mutableListOf<BaseFieldViewModel>()

        LabelWithHeaderFieldViewModel.Builder()
                .title("Serial Number")
                .data(data.deviceSerialNumber)
                .build()
                .let(fields::add)
        LabelWithHeaderFieldViewModel.Builder()
                .title("TPN")
                .data(data.tpn)
                .build()
                .let(fields::add)
        LabelWithHeaderFieldViewModel.Builder()
                .title("Terminal number")
                .data(data.terminalNumber)
                .build()
                .let(fields::add)
        LabelWithHeaderFieldViewModel.Builder()
                .title("Terminal name")
                .data(data.terminalName)
                .build()
                .let(fields::add)
        LabelWithHeaderFieldViewModel.Builder()
                .title("User name")
                .data(data.userName)
                .build()
                .let(fields::add)

        setForm(fields)
    }

    companion object {
        @JvmStatic
        val TAG = ZCreditInternalTerminalInfoFragment::class.java.simpleName

        @JvmStatic
        fun newInstance(): ZCreditInternalTerminalInfoFragment {
            return ZCreditInternalTerminalInfoFragment()
        }
    }
}