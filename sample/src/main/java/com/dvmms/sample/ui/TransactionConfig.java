package com.dvmms.sample.ui;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Platon on 04.07.2016.
 */
public class TransactionConfig {
    @SerializedName("PaymentType")
    String paymentType;
    @SerializedName("TransactionTypes")
    List<String> transactionTypes;
    @SerializedName("Fields")
    List<String> fields;

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public List<String> getTransactionTypes() {
        return transactionTypes;
    }

    public void setTransactionTypes(List<String> transactionTypes) {
        this.transactionTypes = transactionTypes;
    }

    public boolean hasFieldByKey(String fieldKey) {
        for (String key : fields) {
            if (key.equalsIgnoreCase(fieldKey)) {
                return true;
            }
        }
        return false;
    }
}
