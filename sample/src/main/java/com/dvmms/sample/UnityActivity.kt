package com.dvmms.sample

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.dvmms.dejapay.models.DejavooTransactionResponse
import com.dvmms.dejapay.models.requests.DejavooResponseTransactionRecords
import com.dvmms.sample.screens.*
import com.dvmms.sample.ui.fragments.PaymentReportFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import java.util.*

class UnityActivity : AppCompatActivity(), InternalNavigation {
    lateinit var bottomMenu: BottomNavigationView
    lateinit var tvSampleVersion: TextView
    lateinit var tvDejapayLibraryVersion: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_unity)
        tvSampleVersion = findViewById(R.id.tvSampleVersionTextView)
        tvDejapayLibraryVersion = findViewById(R.id.tvLibVersionTextView)
        val ser = Build.SERIAL
        tvSampleVersion.text = "Sample version: ${BuildConfig.VERSION_NAME}b${BuildConfig.VERSION_CODE} ${ser}"
        tvDejapayLibraryVersion.text = "Dejapay library version: ${com.dvmms.dejapay.BuildConfig.VERSION_NAME}"
        bottomMenu = findViewById(R.id.bnvAppBar)
        bottomMenu.setOnNavigationItemSelectedListener { item ->
            when(item.itemId) {
                R.id.dvAndroid -> {
                    supportFragmentManager.beginTransaction().apply {
                        replace(R.id.flContainer, ZCreditInternalListFragment.newInstance(), ZCreditInternalListFragment.TAG)
                        commitAllowingStateLoss()
                    }
                    true
                }
                R.id.dvPay -> {
                    supportFragmentManager.beginTransaction().apply {
                        replace(R.id.flContainer, InternalListFragment.newInstance(), InternalListFragment.TAG)
                        commitAllowingStateLoss()
                    }
                    true
                }
                else -> {true}
            }
        }
        showBottomMenu()
        if (Locale.getDefault().country.contains("IL", true)) {
            bottomMenu.selectedItemId = R.id.dvAndroid
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.flContainer, ZCreditInternalListFragment.newInstance(), ZCreditInternalListFragment.TAG)
                commitAllowingStateLoss()
            }
        } else {
            bottomMenu.selectedItemId = R.id.dvPay
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.flContainer, InternalListFragment.newInstance(), InternalListFragment.TAG)
                commitAllowingStateLoss()
            }
        }
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    0)
            }
        }
        Log.e("ERROR", supportFragmentManager.backStackEntryCount.toString())
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount >= 0) {
            super.onBackPressed()
            if (supportFragmentManager.backStackEntryCount == 0) {
                showBottomMenu()
            }
        }
    }

    private fun showBottomMenu() {
        bottomMenu.visibility = View.VISIBLE
    }

    private fun hideBottomMenu() {
        bottomMenu.visibility = View.GONE
    }

    override fun navigateToSettlementSample() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, InternalSettlementFragment.newInstance(), InternalSettlementFragment.TAG)
            addToBackStack(InternalSettlementFragment.TAG)
            commitAllowingStateLoss()
        }
        hideBottomMenu()
    }

    override fun navigateToTipAdjust() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, TipAdjustFragment.newInstance(), TipAdjustFragment.TAG)
            addToBackStack(TipAdjustFragment.TAG)
            commitAllowingStateLoss()
        }
        hideBottomMenu()
    }

    override fun doTipAdjust(response: DejavooResponseTransactionRecords) {
        val record = response.records.last()

        supportFragmentManager.beginTransaction().apply {
            replace(
                R.id.flContainer,
                AdjustTipForRecordFragment.newInstance(
                    "DvCredit",
                    record.totalAmount,
                    record.refId,
                    record.invoiceId
                ),
                AdjustTipForRecordFragment.TAG)
            addToBackStack(AdjustTipForRecordFragment.TAG)
            commitAllowingStateLoss()
        }
    }

    override fun openZCTransaction() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, ZCreditInternalTransactionFragment.newInstance(), ZCreditInternalTransactionFragment.TAG)
            addToBackStack(ZCreditInternalTransactionFragment.TAG)
            commitAllowingStateLoss()
        }
        hideBottomMenu()
    }

    override fun openZCSerialNumber() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, ZCreditInternalTerminalInfoFragment.newInstance(), ZCreditInternalTerminalInfoFragment.TAG)
            addToBackStack(ZCreditInternalTerminalInfoFragment.TAG)
            commitAllowingStateLoss()
        }
        hideBottomMenu()
    }

    override fun openDppInternalTransaction() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, DppInternalTransactionFragment.newInstance(), DppInternalTransactionFragment.TAG)
            addToBackStack(DppInternalTransactionFragment.TAG)
            commitAllowingStateLoss()
        }
        hideBottomMenu()
    }

    override fun openDppSPInTransaction() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, DppSPInTransactionFragment.newInstance(), DppSPInTransactionFragment.TAG)
            addToBackStack(DppSPInTransactionFragment.TAG)
            commitAllowingStateLoss()
        }
        hideBottomMenu()
    }

    override fun openInternalTerminalInfo() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, InternalTerminalInfoFragment.newInstance(), InternalTerminalInfoFragment.TAG)
            addToBackStack(InternalTerminalInfoFragment.TAG)
            commitAllowingStateLoss()
        }
        hideBottomMenu()
    }

    override fun openDppResponse(response: DejavooTransactionResponse) {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, PaymentReportFragment.newInstance(response), PaymentReportFragment.TAG)
            addToBackStack(PaymentReportFragment.TAG)
            commitAllowingStateLoss()
        }
    }



    override fun openPrintout() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, InternalPrintoutFragment.newInstance(), InternalPrintoutFragment.TAG)
            addToBackStack(InternalPrintoutFragment.TAG)
            commitAllowingStateLoss()
        }
        hideBottomMenu()
    }

    override fun openUserChoice() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, InternalUserChoiceFragment.newInstance(), InternalUserChoiceFragment.TAG)
            addToBackStack(InternalUserChoiceFragment.TAG)
            commitAllowingStateLoss()
        }
        hideBottomMenu()
    }

    override fun openGetTransactions() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer, InternalGetTransactionsFragment.newInstance(), InternalGetTransactionsFragment.TAG)
            addToBackStack(InternalGetTransactionsFragment.TAG)
            commitAllowingStateLoss()
        }
        hideBottomMenu()
    }
}