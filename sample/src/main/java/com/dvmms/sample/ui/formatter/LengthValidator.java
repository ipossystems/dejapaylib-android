package com.dvmms.sample.ui.formatter;

/**
 * Created by Platon on 14.01.2016.
 */
public class LengthValidator implements IValidator<String> {
    private final int minLength;
    private final int maxLength;

    public LengthValidator(int minLength) {
        this(minLength, Integer.MAX_VALUE);
    }

    public LengthValidator(int minLength, int maxLength) {
        this.minLength = minLength;
        this.maxLength = maxLength;
    }

    @Override
    public boolean validate(String value) {
        return (value != null && value.length() >= minLength && value.length() <= maxLength);
    }
}
