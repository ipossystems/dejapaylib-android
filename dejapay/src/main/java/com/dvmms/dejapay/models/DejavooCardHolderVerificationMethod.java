package com.dvmms.dejapay.models;

/*
Cardholder Verification Method Result:
0-No CVM,
2-Signature required,
4-Verified by PIN
 */
public enum DejavooCardHolderVerificationMethod {
    Unknown(-1), NoCVM(0), SignatureRequired(2), VerifiedByPin(4);

    private final int number;

    DejavooCardHolderVerificationMethod(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public static DejavooCardHolderVerificationMethod fromNumber(int number) {
        for (DejavooCardHolderVerificationMethod method : DejavooCardHolderVerificationMethod.values()) {
            if (method.number == number) {
                return method;
            }
        }

        return Unknown;
    }
}
