package com.dvmms.dejapay.models.requests;

import android.os.Parcel;
import android.os.Parcelable;

import com.dvmms.dejapay.DejavooUtils;
import com.dvmms.dejapay.models.DejavooPaymentType;
import com.dvmms.dejapay.models.DejavooTransactionRequest;

public class DejavooRequestPreAuth implements Parcelable {
    private final double amount;
    private final String refId;
    private final DejavooTransactionRequest.ReceiptType receipt;
    private final DejavooPaymentType type;
    private final int invoiceId;

    private DejavooRequestPreAuth(Builder builder) {
        this.type = builder.type;
        this.amount = builder.amount;
        this.refId = builder.refId;
        this.receipt = builder.receipt;
        this.invoiceId = builder.invoiceId;
    }

    public double getAmount() {
        return amount;
    }

    public String getRefId() {
        return refId;
    }

    public DejavooTransactionRequest.ReceiptType getReceipt() {
        return receipt;
    }

    public DejavooPaymentType getType() {
        return type;
    }

    public int getInvoiceId() {
        return invoiceId;
    }

    public static class Builder {
        private DejavooPaymentType type = DejavooPaymentType.Credit;
        private double amount = 0;
        private String refId = DejavooUtils.generateRandomRefId();
        private DejavooTransactionRequest.ReceiptType receipt = DejavooTransactionRequest.ReceiptType.Both;
        private int invoiceId = DejavooUtils.generateRandomInvoiceId();

        private Builder() { }

        public Builder setAmount(double amount) {
            this.amount = amount;
            return this;
        }

        public Builder setRefId(String refId) {
            this.refId = refId;
            return this;
        }

        public Builder setReceipt(DejavooTransactionRequest.ReceiptType receipt) {
            this.receipt = receipt;
            return this;
        }

        public Builder setType(DejavooPaymentType type) {
            this.type = type;
            return this;
        }

        public Builder setInvoiceId(int invoiceId) {
            this.invoiceId = invoiceId;
            return this;
        }

        public DejavooRequestPreAuth build() {
            return new DejavooRequestPreAuth(this);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.amount);
        dest.writeString(this.refId);
        dest.writeInt(this.receipt == null ? -1 : this.receipt.ordinal());
        dest.writeInt(this.type == null ? -1 : this.type.ordinal());
        dest.writeInt(this.invoiceId);
    }

    protected DejavooRequestPreAuth(Parcel in) {
        this.amount = in.readDouble();
        this.refId = in.readString();
        int tmpReceipt = in.readInt();
        this.receipt = tmpReceipt == -1 ? null : DejavooTransactionRequest.ReceiptType.values()[tmpReceipt];
        int tmpType = in.readInt();
        this.type = tmpType == -1 ? null : DejavooPaymentType.values()[tmpType];
        this.invoiceId = in.readInt();
    }

    public static final Parcelable.Creator<DejavooRequestPreAuth> CREATOR = new Parcelable.Creator<DejavooRequestPreAuth>() {
        @Override
        public DejavooRequestPreAuth createFromParcel(Parcel source) {
            return new DejavooRequestPreAuth(source);
        }

        @Override
        public DejavooRequestPreAuth[] newArray(int size) {
            return new DejavooRequestPreAuth[size];
        }
    };
}
