package com.dvmms.dejapay.models.requests;

import android.os.Parcel;
import android.os.Parcelable;

import com.dvmms.dejapay.models.DejavooPaymentType;
import com.dvmms.dejapay.models.DejavooTransactionResponse;

import java.util.HashMap;
import java.util.Map;

public class DejavooResponseVoid implements Parcelable {
    private final DejavooPaymentType paymentType;
    private final String refId;
    private final Map<DejavooTransactionResponse.ReceiptType, String> receipts;
    private final DejavooTransactionResponse response;
    private final boolean success;
    private final String errorMessage;

    private DejavooResponseVoid(Builder builder) {
        this.success = builder.success;
        this.errorMessage = builder.errorMessage;
        this.paymentType = builder.paymentType;
        this.refId = builder.refId;
        this.receipts = builder.receipts;
        this.response = builder.response;
    }

    public DejavooPaymentType getPaymentType() {
        return paymentType;
    }

    public String getRefId() {
        return refId;
    }

    public Map<DejavooTransactionResponse.ReceiptType, String> getReceipts() {
        return receipts;
    }

    public DejavooTransactionResponse getResponse() {
        return response;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private DejavooPaymentType paymentType = DejavooPaymentType.Credit;
        private String refId;
        private Map<DejavooTransactionResponse.ReceiptType, String> receipts;
        private DejavooTransactionResponse response;
        private boolean success = false;
        private String errorMessage = "";

        private Builder() { }

        public Builder setPaymentType(DejavooPaymentType paymentType) {
            this.paymentType = paymentType;
            return this;
        }

        public Builder setRefId(String refId) {
            this.refId = refId;
            return this;
        }

        public Builder setReceipts(Map<DejavooTransactionResponse.ReceiptType, String> receipts) {
            this.receipts = receipts;
            return this;
        }

        public Builder setResponse(DejavooTransactionResponse response) {
            this.response = response;
            return this;
        }

        public Builder setSuccess(boolean success) {
            this.success = success;
            return this;
        }

        public Builder setErrorMessage(String message) {
            this.errorMessage = message != null ? message : "";
            return this;
        }

        public DejavooResponseVoid build() {
            return new DejavooResponseVoid(this);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.paymentType == null ? -1 : this.paymentType.ordinal());
        dest.writeString(this.refId);
        dest.writeInt(this.receipts.size());
        for (Map.Entry<DejavooTransactionResponse.ReceiptType, String> entry : this.receipts.entrySet()) {
            dest.writeInt(entry.getKey() == null ? -1 : entry.getKey().ordinal());
            dest.writeString(entry.getValue());
        }
        dest.writeParcelable(this.response, flags);
        dest.writeByte(this.success ? (byte) 1 : (byte) 0);
        dest.writeString(this.errorMessage);
    }

    protected DejavooResponseVoid(Parcel in) {
        int tmpPaymentType = in.readInt();
        this.paymentType = tmpPaymentType == -1 ? null : DejavooPaymentType.values()[tmpPaymentType];
        this.refId = in.readString();
        int receiptsSize = in.readInt();
        this.receipts = new HashMap<DejavooTransactionResponse.ReceiptType, String>(receiptsSize);
        for (int i = 0; i < receiptsSize; i++) {
            int tmpKey = in.readInt();
            DejavooTransactionResponse.ReceiptType key = tmpKey == -1 ? null : DejavooTransactionResponse.ReceiptType.values()[tmpKey];
            String value = in.readString();
            this.receipts.put(key, value);
        }
        this.response = in.readParcelable(DejavooTransactionResponse.class.getClassLoader());
        this.success = in.readByte() != 0;
        this.errorMessage = in.readString();
    }

    public static final Parcelable.Creator<DejavooResponseVoid> CREATOR = new Parcelable.Creator<DejavooResponseVoid>() {
        @Override
        public DejavooResponseVoid createFromParcel(Parcel source) {
            return new DejavooResponseVoid(source);
        }

        @Override
        public DejavooResponseVoid[] newArray(int size) {
            return new DejavooResponseVoid[size];
        }
    };
}
