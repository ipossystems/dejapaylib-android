package com.dvmms.sample.ui.formatter;

/**
 * Created by Platon on 24.03.2016.
 */
public class FactorFieldFormater implements IFieldFormatter<String> {
    @Override
    public String format(String value) {
        return value;
    }

    @Override
    public String parse(String value) {
        if (value.equalsIgnoreCase("0.") || value.equalsIgnoreCase("0.")) {
            return "0";
        }
        return value;
    }

    @Override
    public String transformText(String oldValue, String value) {
        if (value.length() == 1 && (value.equalsIgnoreCase(".") || value.equalsIgnoreCase(","))) {
            value = "0" + value;
        }

        if (!value.isEmpty()) {
            String[] values = value.replace(",", ".").split("\\.");
            Double val = null;
            try {
                val = Double.parseDouble(value);
            } catch (NumberFormatException e) {
                return oldValue;
            }

            if (val > 100.) {
                return oldValue;
            }

            if (values.length == 2 && values[1].length() > 2) {
                 return oldValue;
            }


        }

        return value;
    }
}
