package com.dvmms.dejapay.models;

/**
 * Created by pmalyugin on 10/04/2017.
 */

public class DejavooPrintoutLineItem implements IDejavooPrintoutItem {
    final String left;
    final String right;
    final String center;

    private final boolean large;
    private final boolean inverted;
    private final boolean condensed;
    private final boolean bold;

    private DejavooPrintoutLineItem(String left, String right, String center, boolean large, boolean inverted, boolean condensed, boolean bold) {
        this.left = left;
        this.right = right;
        this.center = center;

        this.bold = bold;
        this.large = large;
        this.condensed = condensed;
        this.inverted = inverted;
    }

    private DejavooPrintoutLineItem(Builder builder) {
        this.left = builder.left;
        this.right = builder.right;
        this.center = builder.center;

        this.bold = builder.bold;
        this.large = builder.large;
        this.condensed = builder.condensed;
        this.inverted = builder.inverted;
    }

    @Override
    public String data() {
        String data = "";
        if (!isEmptyOrNull(center)) {
            data += "<C>" + escapeText(center) + "</C>";
        }

        if (!isEmptyOrNull(left)) {
            data += "<L>" + escapeText(left) + "</L>";
        }

        if (!isEmptyOrNull(right)) {
            data += "<R>" + escapeText(right) + "</R>";
        }

        if (bold) {
            data = "<B>"+data+"</B>";
        }

        if (large) {
            data = "<LG>"+data+"</LG>";
        }

        if (inverted) {
            data = "<INV>"+data+"</INV>";
        }

        if (condensed) {
            data = "<CD>"+data+"</CD>";
        }

        return data;
    }

    // #, % - symbols break SPIN/WiFi request,
    // Prevent parsing failed
    private String escapeText(String text) {
        return text.replace("#", "&#35;").replace("%", "&#37;");
    }

    public static DejavooPrintoutLineItem center(String line, boolean large, boolean inverted, boolean condensed, boolean bold) {
        return new DejavooPrintoutLineItem("", "", line, large, inverted, condensed, bold);
    }

    public static DejavooPrintoutLineItem leftRight(String left, String right, boolean large, boolean inverted, boolean condensed, boolean bold) {
        return new DejavooPrintoutLineItem(left, right, "", large, inverted, condensed, bold);
    }

    public static DejavooPrintoutLineItem left(String left, boolean large, boolean inverted, boolean condensed, boolean bold) {
        return new DejavooPrintoutLineItem(left, "", "", large, inverted, condensed, bold);
    }

    public static DejavooPrintoutLineItem right(String right, boolean large, boolean inverted, boolean condensed, boolean bold) {
        return new DejavooPrintoutLineItem("", right, "", large, inverted, condensed, bold);
    }

    private boolean isEmptyOrNull(String text) {
        return (text != null && text.length() == 0);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String left = "";
        private String right = "";
        private String center = "";

        private boolean large = false;
        private boolean inverted = false;
        private boolean condensed = false;
        private boolean bold = false;

        private Builder() { }

        public Builder setLeft(String left) {
            this.left = left;
            return this;
        }

        public Builder setRight(String right) {
            this.right = right;
            return this;
        }

        public Builder setCenter(String center) {
            this.center = center;
            return this;
        }

        public Builder setLarge(boolean large) {
            this.large = large;
            return this;
        }

        public Builder setInverted(boolean inverted) {
            this.inverted = inverted;
            return this;
        }

        public Builder setCondensed(boolean condensed) {
            this.condensed = condensed;
            return this;
        }

        public Builder setBold(boolean bold) {
            this.bold = bold;
            return this;
        }

        public DejavooPrintoutLineItem build() {
            return new DejavooPrintoutLineItem(this);
        }
    }
}
