package com.dvmms.dejapay.exception;

/**
 * Sending request timeout was expired
 */
public class DejavooRequestTimeoutExpiredException extends DejavooThrowable {
    public DejavooRequestTimeoutExpiredException() {
    }

    public DejavooRequestTimeoutExpiredException(String message) {
        super(message);
    }

    public DejavooRequestTimeoutExpiredException(Throwable throwable) {
        super(throwable);
    }
}
