package com.dvmms.sample.ui.formatter;

/**
 * Created by admin on 12/02/16.
 */
public class IPFieldFormatter implements IFieldFormatter<String> {
    @Override
    public String format(String value) {
        if (value == null) {
            return "";
        }

        return value;
    }

    @Override
    public String parse(String value) {
        return value;
    }

    // TODO: Improve algorithm
    @Override
    public String transformText(String oldValue, String value) {
        //192.168.0.1
        if (value.isEmpty()) {
            return "";
        }

        String values[] = value.split("\\.", -1);



//        if (value.startsWith("0") || value.startsWith(".")) {
//            return "";
//        }

        if (!value.matches("[0-9\\.]*")) {
            return oldValue;
        }

        return value;

//        int length = values.length;
//
//        if (length <= 4) {
//            String oct = values[length-1];
//
//            if (!oct.isEmpty()) {
//                try {
//                    Integer digit = Integer.valueOf(oct);
//
//                    if (digit > 254) {
//                        if (length != 4) {
//                            String lastSymbol = value.substring(value.length() - 1);
//                            return value.substring(0, value.length()) + "." + lastSymbol;
//                        }
//                    } else if(digit == 0) {
//                        if (length != 4) {
//                            String lastSymbol = value.substring(value.length() - 1);
//                            return value.substring(0, value.length()) + "." + lastSymbol;
//                        }
//                    } else {
//                        return value;
//                    }
//
//                } catch (NumberFormatException ex) {
//                    return oldValue;
//                }
//            } else {
//                return value;
//            }
//
//        }
//
//        return oldValue;
    }
}
