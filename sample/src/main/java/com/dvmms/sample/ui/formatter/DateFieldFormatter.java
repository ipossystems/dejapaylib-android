package com.dvmms.sample.ui.formatter;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Platon on 14.01.2016.
 */
public class DateFieldFormatter implements IFieldFormatter<Date> {
    private final Locale locale;
    private final DateFormat dateFormat;
    private final static String TAG = DateFieldFormatter.class.getSimpleName();

    public DateFieldFormatter(String format) {
        this (format, Locale.ENGLISH);
    }

    public DateFieldFormatter(String format, Locale locale) {
        this.locale = locale;
        this.dateFormat = new SimpleDateFormat(format, locale);
    }

    @Override
    public String format(Date value) {
        if (value == null) {
            Log.w(TAG, "Value is null for format");
            return "";
        }

        return dateFormat.format(value);
    }

    @Override
    public Date parse(String value) {
        try {
            return dateFormat.parse(value);
        } catch (Exception e) {
            Log.e(TAG, "Parse date failed", e);
            // TODO: Add Log
            return null;
        }
    }

    @Override
    public String transformText(String oldValue, String value) {
        return value;
    }

    @Override
    public String toString() {
        return "DateFieldFormatter{" +
                "locale=" + locale +
                ", dateFormat=" + dateFormat +
                '}';
    }
}
