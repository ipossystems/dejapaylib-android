package com.dvmms.sample.ui.formatter;

/**
 * Created by Platon on 13.01.2016.
 */
public interface IFieldFormatter <T> {
    String format(T value);
    T parse(String value);
    String transformText(String oldValue, String value);
}
