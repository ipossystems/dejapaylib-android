package com.dvmms.sample.ui.formatter;

import java.util.regex.Pattern;

/**
 * Created by Platon on 01.04.2016.
 */
public class RegexpFieldFormatter implements IFieldFormatter {
    private final Pattern pattern;

    public RegexpFieldFormatter(String format) {
        this.pattern =  Pattern.compile(format);
    }

    @Override
    public String format(Object value) {
        if (value == null) {
            return "";
        }

        if (value instanceof Integer) {
            return String.valueOf(value);
        }

        return (String)value;
    }

    @Override
    public Object parse(String value) {
        return value;
    }

    @Override
    public String transformText(String oldValue, String value) {
        if (!value.isEmpty()) {

            if (!pattern.matcher(value).matches()) {
                return oldValue;
            }

        }

        return value;
    }
}
