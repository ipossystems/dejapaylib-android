package com.dvmms.dejapay.exception;

/**
 * General exception
 */
public class DejavooThrowable extends Exception {
    public DejavooThrowable() {
    }

    public DejavooThrowable(String message) {
        super(message);
    }

    public DejavooThrowable(Throwable throwable) {
        super(throwable);
    }
}
