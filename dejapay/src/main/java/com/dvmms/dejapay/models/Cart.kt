package com.dvmms.dejapay.models

data class Cart(
    var amounts: List<Amount>?,
    var cashAmounts: List<Amount>?,
    var items: List<Item>?
)

data class Amount(
    var name: String,
    var value: String
)

data class Item(
    var name: String,
    var price: String,
    var unitPrice: String,
    var quantity: String,
    var customInfos: List<CustomInfo>?,
    var additionalInfo: String,
    var modifiers: List<Modifier>?
)

data class CustomInfo(
    var name: String,
    var value: String
)

data class Modifier(
    var name: String,
    var options: List<Option>?
)

data class Option(
    var name: String,
    var price: String,
    var quantity: String
)
