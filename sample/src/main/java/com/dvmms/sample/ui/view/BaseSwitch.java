package com.dvmms.sample.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Switch;

/**
 * Created by Platon on 16.01.2016.
 */
public class BaseSwitch extends Switch {
    public BaseSwitch(Context context) {
        super(context);
    }

    public BaseSwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseSwitch(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
