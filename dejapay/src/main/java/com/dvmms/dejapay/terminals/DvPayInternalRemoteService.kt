package com.dvmms.dejapay.terminals

import android.content.ComponentName
import android.content.Context
import android.content.ServiceConnection
import android.os.IBinder
import android.util.Log
import com.dejavoo.dvpay.IInternalTerminal
import com.dejavoo.dvpay.IInternalTerminalCallback
import com.dvmms.dejapay.IRequestCallback
import com.dvmms.dejapay.SimpleLogger
import java.util.concurrent.LinkedBlockingQueue


class DvPayInternalRemoteService {
    var dvPayInternalTerminal: IInternalTerminal? = null
    val commands = LinkedBlockingQueue<() -> Unit>()

    fun getServiceConnection(): ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            Log.d(TAG, "Service connected on the side of the library to DvPay")
            dvPayInternalTerminal = IInternalTerminal.Stub.asInterface(service)
            checkForAwaitingCommands()
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            Log.v(TAG, "onServiceDisconnected")
            dvPayInternalTerminal = null
        }
    }

    fun checkForAwaitingCommands() {
        while (commands.isNotEmpty()) {
            val executable = commands.take()
            executable.invoke()
        }
    }

    fun getTerminalInfo(context: Context, logger: SimpleLogger, callback: IRequestCallback<DvPayTerminalInfo>) {
        logger.debug(context, TAG, "Get terminal info")
        dvPayInternalTerminal?.let {
            callback.onResponse(generateTerminalInfo(it.parameters as MutableMap<String, String?>?))
        } ?: commands.add { callback.onResponse(generateTerminalInfo(dvPayInternalTerminal?.parameters as MutableMap<String, String?>?)) }
    }

    fun sendTransaction(context: Context, logger: SimpleLogger, request: String, callback: IRequestCallback<String>) {
        logger.debug(context, TAG, "Get terminal info")
        dvPayInternalTerminal?.let {
            it.sendTransaction(request, object : IInternalTerminalCallback.Stub() {
                override fun onResponse(response: String?) {
                    callback.onResponse(response)
                }
            })
        } ?: commands.add { dvPayInternalTerminal?.sendTransaction(request, object : IInternalTerminalCallback.Stub() {
            override fun onResponse(response: String?) {
                callback.onResponse(response)
            }
        }) }
    }

    companion object {
        val TAG: String = DvPayInternalRemoteService::class.simpleName ?: "DvPayInternalRemoteService"
    }
    private fun generateTerminalInfo(response: MutableMap<String, String?>?): DvPayTerminalInfo {
        return if (response != null) {
            DvPayTerminalInfo().apply {
                tpn = response[KEY_TPN] ?: ""
                deviceSerialNumber = response[KEY_DEVICE_SERIAL_NUMBER] ?: ""
                userName = response[KEY_USER_NAME] ?: ""
                terminalNumber = response[KEY_TERMINAL_NUMBER] ?: ""
                terminalName = response[KEY_TERMINAL_NAME] ?: ""
                authKey = response[KEY_AUTH_KEY]
            }
        } else {
            DvPayTerminalInfo()
        }
    }

}