package com.dvmms.sample.ui.formatter;

/**
 * Created by Platon on 13.01.2016.
 */
public interface IValidator<T> {
    boolean validate(T value);
}
