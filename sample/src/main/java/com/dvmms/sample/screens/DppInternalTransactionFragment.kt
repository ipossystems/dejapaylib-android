package com.dvmms.sample.screens

import android.graphics.Bitmap
import android.os.Build
import com.dvmms.dejapay.IRequestCallback
import com.dvmms.dejapay.TransactionConvertor
import com.dvmms.dejapay.exception.DejavooThrowable
import com.dvmms.dejapay.models.DejavooPaymentType
import com.dvmms.dejapay.models.DejavooTransactionRequest
import com.dvmms.dejapay.models.DejavooTransactionResponse
import com.dvmms.dejapay.models.DejavooTransactionType
import com.dvmms.dejapay.terminals.InternalTerminal
import com.dvmms.dejapay.terminals.RequestDocument
import com.dvmms.dejapay.terminals.XmlDocument
import com.dvmms.sample.common.JsonConfig
import com.dvmms.sample.ui.TerminalSettingsModel
import com.dvmms.sample.ui.TransactionConfig
import com.dvmms.sample.ui.fields.BaseFieldViewModel
import com.dvmms.sample.ui.fields.ButtonFieldViewModel
import com.dvmms.sample.ui.fields.FieldsManager.IFieldAction
import com.dvmms.sample.ui.fields.FieldsManager.IFilterAction
import com.dvmms.sample.ui.fields.ListPickerFieldViewModel
import com.dvmms.sample.ui.fields.TextFieldViewModel
import com.dvmms.sample.ui.formatter.IntegerFieldFormatter
import com.dvmms.sample.ui.formatter.PriceFieldFormatter
import com.dvmms.sample.ui.fragments.AbstractFieldsFragment
import com.dvmms.sample.ui.fragments.SignatureDialogFragment
import com.dvmms.sample.ui.fragments.SignatureDialogFragment.ISignatureDialogListener
import java.io.ByteArrayOutputStream
import java.util.*

private const val CODE_SKIP_FIELD = 100 //
private const val CODE_NOSKIP_FIELD = 101 //

class DppInternalTransactionFragment:
        AbstractFieldsFragment() {

    private lateinit var transactionConfigs: List<TransactionConfig>

    override fun setupUI() {
        val fields = mutableListOf<BaseFieldViewModel>()

        transactionConfigs = JsonConfig.loadConfigList(
                "transactions_configs.json",
                TransactionConfig::class.java,
                context
        )

        val paymentTypes: MutableMap<Any, String> = LinkedHashMap()

        for (config in transactionConfigs) {
            paymentTypes[config.paymentType] = config.paymentType
        }

        ListPickerFieldViewModel.Builder()
                .activity(activity)
                .possibleValues(paymentTypes)
                .key("PaymentType")
                .title("Payment Type")
                .data("Credit")
                .updatedFieldListener{ model -> updatePaymentType(model.data<String>()) }
                .build()
                .let(fields::add)


        ListPickerFieldViewModel.Builder()
                .activity(activity)
                .possibleValues(LinkedHashMap())
                .key("TransType")
                .title("Transaction Type")
                .hidden(true)
                .build()
                .let(fields::add)

        val settlementTypes: MutableMap<Any, String> = TreeMap()
        settlementTypes["Close"] = "Close"
        settlementTypes["Clear"] = "Clear"
        settlementTypes["Force"] = "Force"

        ListPickerFieldViewModel.Builder()
                .activity(activity)
                .possibleValues(settlementTypes)
                .key("Param")
                .title("Settlement Type")
                .hidden(true)
                .build()
                .let(fields::add)


        TextFieldViewModel.Builder()
                .title("Reference ID")
                .key("RefId")
                .data(TerminalSettingsModel.getReferenceId(context).toString())
                .type(TextFieldViewModel.Type.Number)
                .hidden(true)
                .build()
                .let(fields::add)

        TextFieldViewModel.Builder()
                .title("Amount")
                .type(TextFieldViewModel.Type.Number)
                .key("Amount")
                .formatter(PriceFieldFormatter())
                .hidden(true)
                .build()
                .let(fields::add)


        TextFieldViewModel.Builder()
                .title("Tip")
                .key("Tip")
                .type(TextFieldViewModel.Type.Number)
                .formatter(PriceFieldFormatter())
                .hidden(true)
                .build()
                .let(fields::add)

        TextFieldViewModel.Builder()
                .title("Points")
                .key("Points")
                .type(TextFieldViewModel.Type.Number)
                .formatter(IntegerFieldFormatter())
                .hidden(true)
                .build()
                .let(fields::add)

        TextFieldViewModel.Builder()
                .title("Account last 4 digits")
                .key("AcntLast4")
                .hidden(true)
                .build()
                .let(fields::add)

        TextFieldViewModel.Builder()
                .title("Auth code")
                .key("AuthCode")
                .hidden(true)
                .build()
                .let(fields::add)


        TextFieldViewModel.Builder()
                .title("Token")
                .key("Token")
                .hidden(true)
                .build()
                .let(fields::add)

        TextFieldViewModel.Builder()
                .title("Invoice Number")
                .key("InvNum")
                .hidden(true)
                .build()
                .let(fields::add)

        TextFieldViewModel.Builder()
                .title("Clerk ID")
                .key("ClerkId")
                .hidden(true)
                .build()
                .let(fields::add)

        TextFieldViewModel.Builder()
                .title("Table Number")
                .key("TableNum")
                .hidden(true)
                .build()
                .let(fields::add)

        TextFieldViewModel.Builder()
                .title("Ticket Number")
                .key("TicketNum")
                .hidden(true)
                .build()
                .let(fields::add)

        ButtonFieldViewModel.Builder()
                .title("Submit")
                .key("Submit")
                .clickListener { _, _ ->
                    sendTransaction(null)
                }
                .hidden(true)
                .build()
                .let(fields::add)

        setForm(fields)

        updatePaymentType("Credit")
    }

    fun updatePaymentType(paymentType: String?) {
        findFieldByKey<BaseFieldViewModel>("Submit").isHidden = false
        val transactionTypes: MutableMap<Any, String> = LinkedHashMap()
        var transType: String? = null
        val transactionConfig = getTransactionConfigForPaymentType(paymentType)!!
        for (transactionType in transactionConfig.transactionTypes) {
            if (transType == null) {
                transType = transactionType
            }
            transactionTypes[transactionType] = transactionType
        }
        val transactionTypeField = findFieldByKey<ListPickerFieldViewModel>("TransType")!!
        assert(transType != null)
        transactionTypeField.setPossibleValues(transactionTypes)
        transactionTypeField.setData(transType)
        transactionTypeField.isHidden = false


        // Configuration Fields
        filterFields(IFilterAction { field: BaseFieldViewModel ->
            !field.key().equals("PaymentType", ignoreCase = true) &&
                    !field.key().equals("TransType", ignoreCase = true) &&
                    !field.key().equals("Submit", ignoreCase = true)
        }, IFieldAction { field: BaseFieldViewModel ->
            if (transactionConfig.hasFieldByKey(field.key())) {
                field.isHidden = false
                field.setCode(CODE_NOSKIP_FIELD)
            } else {
                field.isHidden = true
                field.setCode(CODE_SKIP_FIELD)
            }
        })
        refreshFields()
    }

    fun getTransactionConfigForPaymentType(paymentType: String?): TransactionConfig? {
        for (config in transactionConfigs) {
            if (config.paymentType.equals(paymentType, ignoreCase = true)) {
                return config
            }
        }
        return null
    }

    fun showSignatureDialog() {
        SignatureDialogFragment.newInstance(object : ISignatureDialogListener {
            override fun onSkipSignature(dialog: SignatureDialogFragment) {
                dialog.dismiss()
                sendSignature(null)
            }

            override fun onSendSignature(dialog: SignatureDialogFragment, sign: Bitmap) {
                dialog.dismiss()
                sendSignature(sign)
            }
        }).show(fragmentManager!!, "Signature")
    }

    private fun sendSignature(signature: Bitmap?) {
        var bSign = signature
        val MAX_SIGNATURE_WIDTH = 384
        var signBytes: ByteArray? = null
        if (bSign != null) {
            if (bSign.width > MAX_SIGNATURE_WIDTH) {
                val aspect = bSign.width / bSign.height.toFloat()
                val scaledHeight = Math.round(MAX_SIGNATURE_WIDTH / aspect)
                bSign = Bitmap.createScaledBitmap(bSign, MAX_SIGNATURE_WIDTH, scaledHeight, false)
            }
            val quality = 100
            val byteArrayOutputStream = ByteArrayOutputStream()
            bSign!!.compress(Bitmap.CompressFormat.PNG, quality, byteArrayOutputStream)
            signBytes = byteArrayOutputStream.toByteArray()
        }
        sendTransaction(signBytes)
    }

    private fun sendTransaction(signatureBytes: ByteArray?) {
        onStartProgress("Send Transaction")
        val request = DejavooTransactionRequest()
        val paymentTypeField = findFieldByKey<BaseFieldViewModel>("PaymentType")
        val paymentTypeText = paymentTypeField.data<String>()
        for (paymentType in DejavooPaymentType.values()) {
            if (paymentType.name.equals(paymentTypeText, ignoreCase = true)) {
                request.paymentType = paymentType
                break
            }
        }
        val transactionTypeField = findFieldByKey<BaseFieldViewModel>("TransType")
        val transactionTypeText = transactionTypeField.data<String>()
        for (transactionType in DejavooTransactionType.values()) {
            if (transactionType.name.equals(transactionTypeText, ignoreCase = true)) {
                request.transactionType = transactionType
                break
            }
        }
        request.parameter = getValueByKey<String?>("Param", null)
        request.amount = getValueByKey("Amount", 0.0f)
        request.tip = getValueByKey("Tip", 0f)
        request.acntLast4 = getValueByKey("AcntLast4", "")
        request.authenticationCode = getValueByKey("AuthCode", "")
        request.token = getValueByKey<String?>("Token", null)
        request.referenceId = getValueByKey("RefId", "")

//        request.setRegisterId(this.getValueByKey("RegisterId", ""));
        request.invoiceNumber = getValueByKey("InvNum", "")
        request.clerkId = getValueByKey<Int?>("ClerkId", null)
        request.tableNumber = getValueByKey<Int?>("TableNum", null)
        request.tickerNumber = getValueByKey<String?>("TicketNum", null)
        request.receiptType = DejavooTransactionRequest.ReceiptType.Both
        request.signature = signatureBytes
        request.isSignatureCapable = true
        val settingsModel = TerminalSettingsModel.load(context)
        if (settingsModel.connection == TerminalSettingsModel.Connecvity.Proxy) {
            request.registerId = settingsModel.proxyRegisterId
            request.authenticationKey = settingsModel.proxyAuthKey
        } else {
            request.registerId = "1"
        }
        val xml = TransactionConvertor().serialize(request)
        val fields = XmlDocument.fromText(xml)?.getFields("request") ?: listOf()
        var packet = ""
        if (fields.isNotEmpty()) {
            packet = RequestDocument("transaction").apply {
                add("version", 1)
                add("provider", "vega")

                addFields(fields)

            }.asRequest()
        }
        InternalTerminal(requireContext().packageName)
            .sendTransaction(requireContext(), packet, object : IRequestCallback<String> {
                override fun onResponse(response: String) {
                    if (response.isNotEmpty()){
                        val dppResponse = TransactionConvertor().deserialize(response)
                        onStopProgress()
                        if (dppResponse.isSignatureRequired) {
                            showSignatureDialog()
                        } else {
                            if (dppResponse.resultCode == DejavooTransactionResponse.ResultCode.Failed) {
                                increaseReferenceId(10)
                            } else {
                                increaseReferenceId(1)
                            }

                            navigator(InternalNavigation::class.java).openDppResponse(dppResponse)
                        }
                    }

                }

                override fun onError(throwable: DejavooThrowable) {
                    onStopProgress()
                        showAlertMessage("Transaction Failed: $throwable")
                }

            })



//        InternalTerminal(requireContext().packageName)
//                .commitTransaction(
//                        requireContext(), request, object : IRequestCallback<DejavooTransactionResponse> {
//                    override fun onResponse(response: DejavooTransactionResponse) {
//                        onStopProgress()
//                        if (response.isSignatureRequired) {
//                            showSignatureDialog()
//                        } else {
//                            if (response.resultCode == DejavooTransactionResponse.ResultCode.Failed) {
//                                increaseReferenceId(10)
//                            } else {
//                                increaseReferenceId(1)
//                            }
//
//                            navigator(InternalNavigation::class.java).openDppResponse(response)
//                        }
//                    }
//
//                    override fun onError(throwable: DejavooThrowable) {
//                        onStopProgress()
//                        showAlertMessage("Transaction Failed: $throwable")
//                    }
//                })
    }

    fun increaseReferenceId(step: Int) {
        val refIdField = findFieldByKey<BaseFieldViewModel>("RefId")
        var refId = TerminalSettingsModel.getReferenceId(context)
        if (refIdField != null) {
            refId = refIdField.data<String>().toInt()
        }
        TerminalSettingsModel.saveReferenceId(
                context, refId + step)
    }

    private fun <T> getValueByKey(key: String?, defaultValue: T): T {
        val field = findFieldByKey<BaseFieldViewModel>(key)
        return if (field != null) {
            field.data()
        } else defaultValue
    }

    companion object {
        @JvmStatic
        val TAG = DppInternalTransactionFragment::class.java.simpleName

        @JvmStatic
        fun newInstance(): DppInternalTransactionFragment {
            return DppInternalTransactionFragment()
        }
    }

}