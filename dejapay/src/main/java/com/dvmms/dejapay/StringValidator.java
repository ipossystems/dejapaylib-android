package com.dvmms.dejapay;

/**
 * Created by Platon on 30.06.2016.
 */
class StringValidator
    extends DefaultValidator<String>
    implements IValidator<String>
{
    private final String fieldName;
    private final boolean required;
    private int minLength = 0;
    private int maxLength = Integer.MAX_VALUE;

    public StringValidator(String fieldName, boolean required, int minLength, int maxLength) {
        this.fieldName = fieldName;
        this.required = required;
        this.minLength = minLength;
        this.maxLength = maxLength;
    }

    @Override
    public boolean validate(ValidatorContext context, String field) {
        if (field == null) {
            if (required) {
                context.addError("'%s' required", fieldName);
                return false;
            }
            return true;
        }

        if (field.length() < minLength && field.length() > maxLength) {
            if (minLength == 0) {
                context.addError("'%s' length can not be great then %d", fieldName, maxLength);
            } else {
                context.addError("'%s' length must be in range [%d,%d]", fieldName, minLength, maxLength);
            }

            return false;
        }

        return true;
    }
}
