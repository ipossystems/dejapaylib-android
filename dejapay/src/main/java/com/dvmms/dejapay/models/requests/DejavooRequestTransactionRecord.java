package com.dvmms.dejapay.models.requests;

import com.dvmms.dejapay.models.DejavooPaymentType;
import com.dvmms.dejapay.models.DejavooTransactionRequest;

public class DejavooRequestTransactionRecord {
    private final DejavooPaymentType paymentType;
    private final String refId;
    private final String invoiceId;
    private final DejavooTransactionRequest.ReceiptType printReceipt;
    private final DejavooTransactionRequest.ReceiptType returnReceipt;
    private final int transNum;

    private DejavooRequestTransactionRecord(Builder builder) {
        this.paymentType = builder.paymentType;
        this.refId = builder.refId;
        this.invoiceId = builder.invoiceId;
        this.printReceipt = builder.printReceipt;
        this.returnReceipt = builder.returnReceipt;
        this.transNum = builder.transNum;
    }

    public DejavooPaymentType getPaymentType() {
        return paymentType;
    }

    public String getRefId() {
        return refId;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public DejavooTransactionRequest.ReceiptType getPrintReceipt() {
        return printReceipt;
    }

    public DejavooTransactionRequest.ReceiptType getReturnReceipt() {
        return returnReceipt;
    }

    public int getTransNum() {
        return transNum;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private DejavooPaymentType paymentType = DejavooPaymentType.Credit;
        private String refId = "";
        private String invoiceId = "";
        private DejavooTransactionRequest.ReceiptType printReceipt = DejavooTransactionRequest.ReceiptType.No;
        private DejavooTransactionRequest.ReceiptType returnReceipt = DejavooTransactionRequest.ReceiptType.Both;
        private int transNum = -1;

        private Builder() { }

        public Builder setPaymentType(DejavooPaymentType paymentType) {
            this.paymentType = paymentType;
            return this;
        }

        public Builder setRefId(String refId) {
            this.refId = refId;
            return this;
        }

        public Builder setInvoiceId(String invoiceId) {
            this.invoiceId = invoiceId;
            return this;
        }

        public Builder setPrintReceipt(DejavooTransactionRequest.ReceiptType printReceipt) {
            this.printReceipt = printReceipt;
            return this;
        }

        public Builder setReturnReceipt(DejavooTransactionRequest.ReceiptType returnReceipt) {
            this.returnReceipt = returnReceipt;
            return this;
        }

        public Builder setTransNum(int transNum) {
            this.transNum = transNum;
            return this;
        }

        public DejavooRequestTransactionRecord build() {
            return new DejavooRequestTransactionRecord(this);
        }
    }
}
