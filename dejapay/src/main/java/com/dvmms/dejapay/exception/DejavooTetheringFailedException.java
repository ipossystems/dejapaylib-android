package com.dvmms.dejapay.exception;

/**
 * Tethering was failed
 */
public class DejavooTetheringFailedException extends DejavooThrowable {
}
