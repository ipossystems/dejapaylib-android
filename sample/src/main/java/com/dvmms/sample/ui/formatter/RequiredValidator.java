package com.dvmms.sample.ui.formatter;

/**
 * Created by admin on 17/02/16.
 */
public class RequiredValidator implements IValidator<Object> {
    @Override
    public boolean validate(Object value) {
        if (value == null) {
            return false;
        }


        if (value instanceof String) {
            return !(((String) value).isEmpty());
        }

        return true;
    }
}
