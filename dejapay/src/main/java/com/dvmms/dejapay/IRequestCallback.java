package com.dvmms.dejapay;

import com.dvmms.dejapay.exception.DejavooThrowable;

/**
 * Callback for requests
 * @param <T> request type (DejavooTransactionRequest or DejavooPrintoutRequest)
 */
public interface IRequestCallback<T> {
    /**
     * Called when a request completes successfully.
     * Method calls in Main thread
     * @param response Response
     */
    void onResponse(T response);

    /**
     * Called when a request fails to complete normally.
     * Method calls in Main thread
     * @param throwable error
     */
    void onError(DejavooThrowable throwable);
}
