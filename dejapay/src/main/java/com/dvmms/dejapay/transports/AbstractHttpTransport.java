package com.dvmms.dejapay.transports;

/**
 * Abstract http transport
 */
class AbstractHttpTransport {
    protected IPaymentApi api;
    protected IPaymentApi apiCheckState;

    public AbstractHttpTransport(String path, IPaymentApi api) {
        this.api = api;
        this.apiCheckState = new ProxyWebPaymentApiClient(path, 4, 4);
    }

}
