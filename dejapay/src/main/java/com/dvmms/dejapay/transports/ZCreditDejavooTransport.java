package com.dvmms.dejapay.transports;

import com.dvmms.dejapay.FakeLogger;
import com.dvmms.dejapay.ICallback;
import com.dvmms.dejapay.IDejaPayLogger;
import com.dvmms.dejapay.exception.DejavooInvalidResponseException;
import com.dvmms.dejapay.exception.DejavooRequestFailedException;
import com.dvmms.dejapay.exception.*;

public class ZCreditDejavooTransport extends AbstractHttpTransport implements IDejavooTransport {
    private final static String TAG = "[ZCREDIT] ";
    private final String authKey;
    private final String registerId;
    private final String tpn;
    private WebRequest<String> activeRequest;
    private IDejaPayLogger logger = new FakeLogger();

    /**
     * Initialize Proxy transport
     *
     * @param tpn     tpn
     * @param path    path to proxy (ex: https://spin.zcredit.co.il)
     * @param authKey authentication key
     * @param registerId register id
     * @param timeoutSeconds wait before cancels a request
     * @param production true - production/ false - test
     */
    public ZCreditDejavooTransport(String tpn, String path, String authKey, String registerId, int timeoutSeconds, boolean production) {
        super(path, new ZCreditWebPaymentClient(path, timeoutSeconds, timeoutSeconds, production));
        this.authKey = authKey;
        this.registerId = registerId;
        this.tpn = tpn;
    }

    public ZCreditDejavooTransport(String tpn, String path, String authKey, String registerId, boolean production) {
        this(tpn, path, authKey, registerId, 800, production);
    }

    @Override
    public void sendPacket(SPinType sPinType, String packet, final ITransportCallback callback) {
        logger.v(TAG + "Sending packet");

        if (sPinType == SPinType.Transaction) {
            activeRequest = api.sendTransaction(packet.replace("\n", ""));
        } else {
            activeRequest = api.printout(packet.replace("\n", ""));
        }

        activeRequest.enqueue(new IWebCallback<WebResponse<String>>() {
            @Override
            public void success(WebResponse<String> response) {
                activeRequest = null;
                if (response.getStatus() == 200) {
                    if (response.getData() != null && !response.getData().isEmpty()) {
                        logger.d(TAG + "200 OK status code received, the response is valid");
                        callback.onResponse(response.getData());
                    } else {
                        logger.e(new DejavooInvalidResponseException(), TAG + "200 OK status code received, the response is not valid");
                        callback.onError(new DejavooInvalidResponseException());
                    }
                } else {
                    logger.e(new DejavooRequestFailedException(), TAG + "Failed status code %d received", response.getStatus());
                    callback.onError(new DejavooRequestFailedException());
                }
            }

            @Override
            public void failure(Throwable t) {
                activeRequest = null;
                logger.e(t, TAG + "SPIN request failed");
                callback.onError(DejavooExceptionAdapter.fromException(t));
            }
        });
    }

    @Override
    public void cancel() {
        logger.d(TAG + "Cancel SPIN request");
        if (activeRequest != null) {
            activeRequest.cancel();
        }
    }

    @Override
    public void stop() {
        logger.d(TAG + "Stop SPIN request");
        if (activeRequest != null) {
            activeRequest.cancel();
        }
    }

    @Override
    public boolean connect() {
        return true;
    }

    @Override
    public void getState(String tpn, final ICallback<State> callback) {
        logger.v(TAG + "Getting SPIN status from the backend");
        api.status(tpn).enqueue(new IWebCallback<WebResponse<String>>() {
            @Override
            public void success(WebResponse<String> response) {
                try {
                    boolean connected = response.getData().equalsIgnoreCase("Online");
                    callback.call((connected) ? State.Connected : State.Disconnected);

                } catch (Exception ex) {
                    ex.printStackTrace();
                    callback.call(State.None);
                }
            }

            @Override
            public void failure(Throwable t) {
                callback.call(State.None);
            }
        });
    }

    @Override
    public void setLogger(IDejaPayLogger logger) {
        if (logger == null) {
            this.logger = new FakeLogger();
        } else {
            this.logger = logger;
        }
    }

    @Override
    public String getAuthKey() {
        return (authKey != null) ? authKey : "";
    }

    @Override
    public String getRegisterId() {
        return (registerId != null) ? registerId : "1";
    }

    @Override
    public String getTpn() {
        return (tpn != null) ? tpn : "";
    }
}
