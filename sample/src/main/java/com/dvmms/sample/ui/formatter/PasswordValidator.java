package com.dvmms.sample.ui.formatter;

/**
 * Created by Platon on 18.03.2016.
 */
public class PasswordValidator extends RegexpFieldValidator {
    private final static String pattern = "^(?=.*[a-zA-Z])(?=.*\\d)(?=.*[$@!?#$%^&;*+(){}\\[\\]':./|~\\-_\\\\&lt;&gt;=])[A-Za-z\\d$@!?#$%^&;*+(){}\\[\\]':./|~\\-_\\\\&lt;&gt;=]{8,20}";

    public PasswordValidator() {
        super(pattern);
    }

    public PasswordValidator(boolean nullable) {
        super(pattern, nullable);
    }
}
