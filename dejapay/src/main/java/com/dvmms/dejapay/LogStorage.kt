package com.dvmms.dejapay

import java.io.IOException
import java.util.*

interface LogStorage {
    @Throws(IOException::class, RuntimeException::class)
    fun put(message: String?)

    fun getAllLogs(removeFile: Boolean): Queue<String>

    @Throws(IOException::class)
    fun recreateLogFile()

    @Throws(IOException::class)
    fun removeLogs()
}