package com.dvmms.dejapay

data class LogData(
    val env: String,
    val file: String,
    val method: String,
    val line: String,
    val level: String,
    val id: String,
    val message: String,
    val tag: String,
    val thread: String,
    val time: String,
    val version: String,
    val user: String,
    val posAppId: String
)