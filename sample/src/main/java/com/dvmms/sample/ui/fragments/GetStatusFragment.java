package com.dvmms.sample.ui.fragments;

import android.content.Context;

import com.dvmms.dejapay.DejavooTerminal;
import com.dvmms.dejapay.IRequestCallback;
import com.dvmms.dejapay.exception.DejavooThrowable;
import com.dvmms.dejapay.models.DejavooPaymentType;
import com.dvmms.dejapay.models.DejavooTransactionRequest;
import com.dvmms.dejapay.models.DejavooTransactionResponse;
import com.dvmms.dejapay.models.DejavooTransactionType;
import com.dvmms.sample.screens.navigators.GetStatusSampleNavigatorContract;
import com.dvmms.sample.ui.TerminalService;
import com.dvmms.sample.ui.TerminalSettingsModel;
import com.dvmms.sample.ui.fields.BaseFieldViewModel;
import com.dvmms.sample.ui.fields.ButtonFieldViewModel;
import com.dvmms.sample.ui.fields.TextFieldViewModel;

import java.util.ArrayList;
import java.util.List;

public class GetStatusFragment extends AbstractFieldsFragment {
    public static final String TAG = GetStatusFragment.class.getSimpleName();
    private GetStatusSampleNavigatorContract mNavigator;

    @Override
    protected void setupUI() {
        List<BaseFieldViewModel> fields = new ArrayList<>();


        fields.add(new TextFieldViewModel.Builder()
                .title("Reference ID")
                .key("RefId")
                .data(String.valueOf(TerminalSettingsModel.getReferenceId(getContext())))
                .build()
        );


        fields.add(new ButtonFieldViewModel.Builder()
                .title("Get Status")
                .clickListener((f,v) -> getStatus(
                        getDataFieldByKey("RefId", "")
                ))
                .build()
        );


        setForm(fields);
    }

    private void getStatus(String referenceId) {
        DejavooTerminal terminal = TerminalService.getInstance().getTerminal();

        if (terminal == null) {
            showAlertMessage("DejaPay not configured");
            return;
        }

        onStartProgress("Processing GetStatus request");
        final DejavooTransactionRequest request = new DejavooTransactionRequest();
        request.setPaymentType(DejavooPaymentType.Credit);
        request.setTransactionType(DejavooTransactionType.Status);
        request.setReferenceId(referenceId);
        request.setPrintReceipt(DejavooTransactionRequest.ReceiptType.No);
//        request.setReceiptType(DejavooTransactionRequest.ReceiptType.No);

        TerminalSettingsModel settingsModel = TerminalSettingsModel.load(getContext());

        if (settingsModel.getConnection() == TerminalSettingsModel.Connecvity.Proxy) {
            request.setRegisterId(settingsModel.getProxyRegisterId());
            request.setAuthenticationKey(settingsModel.getProxyAuthKey());
        } else {
            request.setRegisterId("1");
        }

        terminal.sendTransactionRequest(request, new IRequestCallback<DejavooTransactionResponse>() {
            @Override
            public void onResponse(DejavooTransactionResponse response) {
                onStopProgress();
                mNavigator.onShowTransactionReport(response);
            }

            @Override
            public void onError(DejavooThrowable throwable) {
                onStopProgress();
                showAlertMessage("Transaction Failed: " + throwable.toString());
            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof GetStatusSampleNavigatorContract) {
            mNavigator = (GetStatusSampleNavigatorContract) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement GetStatusSampleNavigatorContract");
        }
    }

    public static GetStatusFragment newInstance() {
        return new GetStatusFragment();
    }
}
