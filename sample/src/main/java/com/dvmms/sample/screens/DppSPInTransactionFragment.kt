package com.dvmms.sample.screens

import android.graphics.Bitmap
import android.os.Build
import android.util.Log
import com.dvmms.dejapay.DejavooTerminal
import com.dvmms.dejapay.IRequestCallback
import com.dvmms.dejapay.exception.DejavooThrowable
import com.dvmms.dejapay.models.DejavooPaymentType
import com.dvmms.dejapay.models.DejavooTransactionRequest
import com.dvmms.dejapay.models.DejavooTransactionResponse
import com.dvmms.dejapay.models.DejavooTransactionType
import com.dvmms.dejapay.transports.ProxyDejavooTransport
import com.dvmms.sample.common.JsonConfig
import com.dvmms.sample.ui.TerminalSettingsModel
import com.dvmms.sample.ui.TransactionConfig
import com.dvmms.sample.ui.fields.*
import com.dvmms.sample.ui.fields.FieldsManager.IFieldAction
import com.dvmms.sample.ui.fields.FieldsManager.IFilterAction
import com.dvmms.sample.ui.formatter.PriceFieldFormatter
import com.dvmms.sample.ui.formatter.TextFieldFormatter
import com.dvmms.sample.ui.fragments.AbstractFieldsFragment
import com.dvmms.sample.ui.fragments.SignatureDialogFragment
import com.dvmms.sample.ui.fragments.SignatureDialogFragment.ISignatureDialogListener
import java.io.ByteArrayOutputStream
import java.util.*

private const val CODE_SKIP_FIELD = 100 //
private const val CODE_NOSKIP_FIELD = 101 //

class DppSPInTransactionFragment :
        AbstractFieldsFragment() {

    private lateinit var transactionConfigs: List<TransactionConfig>

    override fun setupUI() {
        val fields = mutableListOf<BaseFieldViewModel>()

        transactionConfigs = JsonConfig.loadConfigList(
                "transactions_configs.json",
                TransactionConfig::class.java,
                context
        )

        val paymentTypes: MutableMap<Any, String> = LinkedHashMap()

        for (config in transactionConfigs) {
            paymentTypes[config.paymentType] = config.paymentType
        }

        TextFieldViewModel.Builder()
                .title("Reference ID")
                .key("RefId")
                .data(TerminalSettingsModel.getReferenceId(context).toString())
                .type(TextFieldViewModel.Type.Number)
                .hidden(true)
                .build()
                .let(fields::add)

        ListPickerFieldViewModel.Builder()
                .activity(activity)
                .possibleValues(paymentTypes)
                .key("PaymentType")
                .title("Payment Type")
                .data("Credit")
                .updatedFieldListener { model -> updatePaymentType(model.data<String>()) }
                .build()
                .let(fields::add)

        ListPickerFieldViewModel.Builder()
                .activity(activity)
                .possibleValues(LinkedHashMap())
                .key("TransType")
                .title("Transaction Type")
                .hidden(true)
                .build()
                .let(fields::add)

        TextFieldViewModel.Builder()
                .title("Amount")
                .type(TextFieldViewModel.Type.Number)
                .key("Amount")
            .data(2.0.toFloat())
                .formatter(PriceFieldFormatter())
                .build()
                .let(fields::add)

        TextFieldViewModel.Builder()
                .title("TPN")
                .type(TextFieldViewModel.Type.Text)
                .key("TPN")
                .data("")
                .formatter(TextFieldFormatter())
                .hidden(false)
                .build()
                .let(fields::add)

        TextFieldViewModel.Builder()
                .title("URL")
                .type(TextFieldViewModel.Type.Text)
                .key("URL")
                .data("https://spinpos.net/spin")
                .formatter(TextFieldFormatter())
                .hidden(false)
                .build()
                .let(fields::add)

        TextFieldViewModel.Builder()
                .title("AuthKey")
                .type(TextFieldViewModel.Type.Text)
                .data("")
                .key("AuthKey")
                .formatter(TextFieldFormatter())
                .hidden(false)
                .build()
                .let(fields::add)

        ButtonFieldViewModel.Builder()
                .title("Submit")
                .key("Submit")
                .clickListener { _, _ ->
                    sendTransaction(null)
                }
                .hidden(true)
                .build()
                .let(fields::add)

        setForm(fields)

        updatePaymentType("Credit")
    }

    private fun updatePaymentType(paymentType: String?) {
        findFieldByKey<BaseFieldViewModel>("Submit").isHidden = false
        val transactionTypes: MutableMap<Any, String> = LinkedHashMap()
        var transType: String? = null
        val transactionConfig = getTransactionConfigForPaymentType(paymentType)!!
        for (transactionType in transactionConfig.transactionTypes) {
            if (transType == null) {
                transType = transactionType
            }
            transactionTypes[transactionType] = transactionType
        }
        val transactionTypeField = findFieldByKey<ListPickerFieldViewModel>("TransType")!!
        assert(transType != null)
        transactionTypeField.setPossibleValues(transactionTypes)
        transactionTypeField.setData(transType)
        transactionTypeField.isHidden = false


        // Configuration Fields
        filterFields(IFilterAction { field: BaseFieldViewModel ->
            !field.key().equals("PaymentType", ignoreCase = true) &&
                    !field.key().equals("TransType", ignoreCase = true) &&
                    !field.key().equals("Submit", ignoreCase = true) &&
                    !field.key().equals("Amount", ignoreCase = true) &&
                    !field.key().equals("TPN", ignoreCase = true) &&
                    !field.key().equals("URL", ignoreCase = true) &&
                    !field.key().equals("AuthKey", ignoreCase = true)
        }, IFieldAction { field: BaseFieldViewModel ->
            if (transactionConfig.hasFieldByKey(field.key())) {
                field.isHidden = false
                field.setCode(CODE_NOSKIP_FIELD)
            } else {
                field.isHidden = true
                field.setCode(CODE_SKIP_FIELD)
            }
        })
        refreshFields()
    }

    private fun getTransactionConfigForPaymentType(paymentType: String?): TransactionConfig? {
        for (config in transactionConfigs) {
            if (config.paymentType.equals(paymentType, ignoreCase = true)) {
                return config
            }
        }
        return null
    }

    fun showSignatureDialog() {
        SignatureDialogFragment.newInstance(object : ISignatureDialogListener {
            override fun onSkipSignature(dialog: SignatureDialogFragment) {
                dialog.dismiss()
                sendSignature(null)
            }

            override fun onSendSignature(dialog: SignatureDialogFragment, sign: Bitmap) {
                dialog.dismiss()
                sendSignature(sign)
            }
        }).show(fragmentManager!!, "Signature")
    }

    private fun sendSignature(signature: Bitmap?) {
        var bSign = signature
        val MAX_SIGNATURE_WIDTH = 384
        var signBytes: ByteArray? = null
        if (bSign != null) {
            if (bSign.width > MAX_SIGNATURE_WIDTH) {
                val aspect = bSign.width / bSign.height.toFloat()
                val scaledHeight = Math.round(MAX_SIGNATURE_WIDTH / aspect)
                bSign = Bitmap.createScaledBitmap(bSign, MAX_SIGNATURE_WIDTH, scaledHeight, false)
            }
            val quality = 100
            val byteArrayOutputStream = ByteArrayOutputStream()
            bSign!!.compress(Bitmap.CompressFormat.PNG, quality, byteArrayOutputStream)
            signBytes = byteArrayOutputStream.toByteArray()
        }
        sendTransaction(signBytes)
    }

    private fun sendTransaction(signatureBytes: ByteArray?) {
        onStartProgress("Send Transaction")
        val request = DejavooTransactionRequest()
        val paymentTypeField = findFieldByKey<BaseFieldViewModel>("PaymentType")
        val paymentTypeText = paymentTypeField.data<String>()
        for (paymentType in DejavooPaymentType.values()) {
            if (paymentType.name.equals(paymentTypeText, ignoreCase = true)) {
                request.paymentType = paymentType
                break
            }
        }
        val transactionTypeField = findFieldByKey<BaseFieldViewModel>("TransType")
        val transactionTypeText = transactionTypeField.data<String>()
        for (transactionType in DejavooTransactionType.values()) {
            if (transactionType.name.equals(transactionTypeText, ignoreCase = true)) {
                request.transactionType = transactionType
                break
            }
        }
        request.parameter = getValueByKey<String?>("Param", null)
        request.amount = getValueByKey("Amount", 0.0f)
        request.tip = getValueByKey("Tip", 0f)
        request.acntLast4 = getValueByKey("AcntLast4", "")
        request.authenticationCode = getValueByKey("AuthCode", "")
        request.token = getValueByKey<String?>("Token", null)
        request.referenceId = getValueByKey("RefId", "")

//        request.setRegisterId(this.getValueByKey("RegisterId", ""));
        request.invoiceNumber = getValueByKey("InvNum", "")
        request.clerkId = getValueByKey<Int?>("ClerkId", null)
        request.tableNumber = getValueByKey<Int?>("TableNum", null)
        request.tickerNumber = getValueByKey<String?>("TicketNum", null)
        request.receiptType = DejavooTransactionRequest.ReceiptType.Both
        request.signature = signatureBytes
        request.isSignatureCapable = true
        val settingsModel = TerminalSettingsModel.load(context)
        if (settingsModel.connection == TerminalSettingsModel.Connecvity.Proxy) {
            request.registerId = settingsModel.proxyRegisterId
            request.authenticationKey = settingsModel.proxyAuthKey
        } else {
            request.registerId = "1"
        }

        DejavooTerminal(
                ProxyDejavooTransport(
                        getValueByKey<String?>("TPN", ""),
                        getValueByKey<String?>("URL", ""),
                        getValueByKey<String?>("AuthKey", ""),
                        ""),
            requireContext().packageName
        )
                .sendTransactionRequest(request, object : IRequestCallback<DejavooTransactionResponse> {
                    override fun onResponse(response: DejavooTransactionResponse) {
                        onStopProgress()
                        if (response.isSignatureRequired) {
                            showSignatureDialog()
                        } else {
                            if (response.resultCode == DejavooTransactionResponse.ResultCode.Failed) {
                                increaseReferenceId(10)
                            } else {
                                increaseReferenceId(1)
                            }

                            navigator(InternalNavigation::class.java).openDppResponse(response)
                        }
                    }

                    override fun onError(throwable: DejavooThrowable) {
                        onStopProgress()
                        showAlertMessage("Transaction Failed: $throwable")
                    }
                })
    }

    fun increaseReferenceId(step: Int) {
        val refIdField = findFieldByKey<BaseFieldViewModel>("RefId")
        var refId = TerminalSettingsModel.getReferenceId(context)
        if (refIdField != null) {
            refId = refIdField.data<String>().toInt()
        }
        TerminalSettingsModel.saveReferenceId(
                context, refId + step)
    }

    private fun <T> getValueByKey(key: String?, defaultValue: T): T {
        val field = findFieldByKey<BaseFieldViewModel>(key)
        return if (field != null) {
            field.data()
        } else defaultValue
    }

    companion object {
        @JvmStatic
        val TAG = DppSPInTransactionFragment::class.java.simpleName

        @JvmStatic
        fun newInstance(): DppSPInTransactionFragment {
            return DppSPInTransactionFragment()
        }
    }

}