package com.dvmms.sample.screens

import android.os.Build
import android.util.Log
import com.dvmms.dejapay.IRequestCallback
import com.dvmms.dejapay.exception.DejavooThrowable
import com.dvmms.dejapay.models.DejavooUserChoiceRequest
import com.dvmms.dejapay.models.DejavooUserChoiceResponse
import com.dvmms.dejapay.terminals.InternalTerminal
import com.dvmms.sample.ui.fields.BaseFieldViewModel
import com.dvmms.sample.ui.fields.ButtonFieldViewModel
import com.dvmms.sample.ui.fragments.AbstractFieldsFragment

class InternalUserChoiceFragment : AbstractFieldsFragment() {
    private lateinit var navigation: InternalNavigation

    override fun setupUI() {
        navigation = navigator(InternalNavigation::class.java)

        val fields = mutableListOf<BaseFieldViewModel>()

        val request = DejavooUserChoiceRequest().apply {
            timeout = 120
            title = "Test"
            addChoice("Option 1")
            addChoice("Option 2")
        }

        ButtonFieldViewModel.Builder()
                .type(ButtonFieldViewModel.Type.Plain)
                .title("Test")
                .clickListener { _, _ ->
                    onStartProgress("Processing")

                    InternalTerminal(requireContext().packageName)
                            .showOptions(activity!!, request, object :
                                IRequestCallback<DejavooUserChoiceResponse> {
                                override fun onResponse(response: DejavooUserChoiceResponse) {
                                    onStopProgress()
                                    Log.d("InternalUserChoiceFr", "Receive response $response")

                                    showAlertMessage("Success")

                                }

                                override fun onError(t: DejavooThrowable) {
                                    onStopProgress()
                                    Log.e("InternalUserChoiceFr", "Transaction failed", t)
                                    showAlertMessage("Transaction Failed ${t.localizedMessage}")
                                }
                            })
                }
                .build()
                .let(fields::add)

        setForm(fields)
    }

    companion object {
        @JvmStatic
        val TAG = InternalUserChoiceFragment::class.java.simpleName

        @JvmStatic
        fun newInstance(): InternalUserChoiceFragment {
            return InternalUserChoiceFragment()
        }
    }
}