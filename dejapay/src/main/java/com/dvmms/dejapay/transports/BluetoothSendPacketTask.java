package com.dvmms.dejapay.transports;

import android.os.AsyncTask;

import com.dvmms.dejapay.IDejaPayLogger;
import com.dvmms.dejapay.exception.DejavooInvalidResponseException;
import com.dvmms.dejapay.exception.DejavooRequestCancelledException;
import com.dvmms.dejapay.exception.DejavooThrowable;
import com.dvmms.dejapay.exception.DejavooRequestTimeoutExpiredException;
import com.dvmms.dejapay.exception.DejavooTransportNotConnectedException;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by Platon on 06.07.2016.
 */
class BluetoothSendPacketTask extends AsyncTask<String, Void, BluetoothSendPacketTask.SendPacketResult> {
    private final String macAddress;
    private final IBluetoothService bluetoothService;
    private final ITransportCallback callback;
    private final int timeoutMilliseconds;
    private final IDejaPayLogger logger;

    private boolean cancelled = false;

    private final Charset UTF8_CHARSET = Charset.forName("UTF-8");
    CountDownLatch timeoutLatch;
    private String response;

    public BluetoothSendPacketTask(IDejaPayLogger logger, String macAddress, IBluetoothService bluetoothService, ITransportCallback callback, int timeoutSeconds) {
        this.logger = logger;
        this.macAddress = macAddress;
        this.bluetoothService = bluetoothService;
        this.callback = callback;
        this.timeoutMilliseconds = timeoutSeconds;
    }

    @Override
    protected BluetoothSendPacketTask.SendPacketResult doInBackground(String... params) {
        String packet =  params[0];

        IBluetoothService.IBluetoothListener connectionListener = new IBluetoothService.IBluetoothListener() {
            @Override
            public void onReadData(String macAddress, byte[] bytes) {
                String bytesText = new String(bytes, UTF8_CHARSET);
                logger.d("[MAC=%s] Bluetooth onReadData = %s", macAddress, bytesText);

                if (macAddress.equals(BluetoothSendPacketTask.this.macAddress)) {
                    response = bytesText;
                    if (timeoutLatch != null) {
                        timeoutLatch.countDown();
                    }
                }
            }

            @Override
            public void onUpdatedStatus(String macAddress, IBluetoothService.BluetoothStatus status) {
                logger.d("[MAC=%s] Bluetooth updated status: %s", macAddress, status.name());
                if (macAddress.equals(BluetoothSendPacketTask.this.macAddress)) {
                    if (status == IBluetoothService.BluetoothStatus.None) {
                        response = "";
                        if (timeoutLatch != null) {
                            timeoutLatch.countDown();
                        }
                    }
                }
            }
        };


        if (bluetoothService.connectToDevice(macAddress, connectionListener)) {
            logger.d("[MAC=%s] Bluetooth device connected", macAddress);
            packet = packet.replace(" ", "").replace("\n", "");
            ByteBuffer buffer = Charset.forName("UTF-8").encode(packet);

            try {
                timeoutLatch = new CountDownLatch(1);
                bluetoothService.write(macAddress, buffer.array());

                if (!timeoutLatch.await(timeoutMilliseconds, TimeUnit.SECONDS)) {
                    logger.d("[MAC=%s] Bluetooth sending timeout has been expired", macAddress);
                    bluetoothService.removeDevice(macAddress);
                    return new SendPacketResult(null, new DejavooRequestTimeoutExpiredException()); // Timeout
                } else {
                    if (cancelled) {
                        logger.d("[MAC=%s] Request has been canceled", macAddress);
                        return new SendPacketResult(null, new DejavooRequestCancelledException()); // Cancelled
                    } else {
                        if (response != null && response.length() > 0) {
                            return new SendPacketResult(response, null);
                        } else {
                            return new SendPacketResult(null, new DejavooInvalidResponseException());
                        }
                    }
                }

            } catch (InterruptedException ex) {
                return new SendPacketResult(null, new DejavooThrowable(ex));
            } finally {
                timeoutLatch = null;
            }
        } else {
            logger.d("[MAC=%s] Bluetooth device disconnected", macAddress);
            return new SendPacketResult(null, new DejavooTransportNotConnectedException());
        }
    }

    @Override
    protected void onPostExecute(SendPacketResult result) {
        if (!isCancelled()) {
            if (result.hasError()) {
                callback.onError(result.getThrowable());
                logger.e(result.getThrowable(), "Sending packet via bluetooth failed");
            } else {
                callback.onResponse(result.getResponse());
            }
        }
    }

    public void cancel() {
        cancelled = true;
        if (timeoutLatch != null) {
            timeoutLatch.countDown();
        }
    }

    static class SendPacketResult {
        private String response;
        private DejavooThrowable throwable;

        public SendPacketResult(String response, DejavooThrowable throwable) {
            this.response = response;
            this.throwable = throwable;
        }

        public boolean hasError() {
            return throwable != null;
        }

        public String getResponse() {
            return response;
        }

        public void setResponse(String response) {
            this.response = response;
        }

        public DejavooThrowable getThrowable() {
            return throwable;
        }

        public void setThrowable(DejavooThrowable throwable) {
            this.throwable = throwable;
        }
    }
}
