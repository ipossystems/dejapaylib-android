package com.dvmms.dejapay;

import android.app.Application;
import android.content.Context;

public class AndroidLogger {

    private static AndroidLogger instance;

    private AsyncLoggingWorker loggingWorker;

    private AndroidLogger(Context context) {
        loggingWorker = new AsyncLoggingWorker(context);
    }

    public static synchronized AndroidLogger createInstance(Context context) {
        if (instance != null) {
            instance.loggingWorker.close();
        }

        instance = new AndroidLogger(context);
        return instance;
    }

    public static synchronized AndroidLogger getInstance(Context context) {
        if (instance != null) {
            return instance;
        } else {
            return createInstance(context);
//            throw new IllegalArgumentException("Logger instance is not initialized. Call createInstance() first!");
        }
    }

    public void log(String message) {
        loggingWorker.addLineToQueue(message);
    }
}
