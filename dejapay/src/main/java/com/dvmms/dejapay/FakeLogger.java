package com.dvmms.dejapay;

import android.util.Log;

/**
 * Created by Platon on 02.08.2016.
 */
public class FakeLogger implements IDejaPayLogger {
    private static final String TAG = "DEJAPAY";

    @Override
    public void v(String s, Object... objects) {
        Log.d(TAG, String.format(s, objects));
    }

    @Override
    public void v(Throwable throwable, String s, Object... objects) {
        Log.v(TAG, String.format(s, objects), throwable);
    }

    @Override
    public void d(String s, Object... objects) {
        Log.d(TAG, String.format(s, objects));
    }

    @Override
    public void d(Throwable throwable, String s, Object... objects) {
        Log.d(TAG, String.format(s, objects), throwable);
    }

    @Override
    public void w(String s, Object... objects) {
        Log.w(TAG, String.format(s, objects));
    }

    @Override
    public void w(Throwable throwable, String s, Object... objects) {
        Log.w(TAG, String.format(s, objects), throwable);
    }

    @Override
    public void e(String s, Object... objects) {
        Log.e(TAG, String.format(s, objects));
    }

    @Override
    public void e(Throwable throwable, String s, Object... objects) {
        Log.e(TAG, String.format(s, objects), throwable);
    }
}
