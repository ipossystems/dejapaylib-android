package com.dvmms.dejapay

class LogPositionDetermination(
    private val stacktrace: Array<StackTraceElement>
) {
    private val elementIndex: Int = 5

    fun determine(): LogPosition {
        val file = stacktrace[elementIndex].className
        val method = stacktrace[elementIndex].methodName
        val line = stacktrace[elementIndex].lineNumber.toString()
        return LogPosition(file, method, line)
    }
}