package com.dvmms.dejapay.terminals

import java.math.BigDecimal

enum class ZCreditType {
    Regular,
    Delayed,
    Debit,
    RegularInstalment,
    CreditInstalment;
}

enum class ZCreditCurrency {
    NIS, USD, EUR
}

enum class ZCreditTransactionType {
    Sale, Refund
}

/* 'J' - Transaction billing type
  0 - Regular
  2 - Authentication Only (Card won't be charged!)
  5 - Authorization (The amount will be secured for future use)
  */
enum class ZCreditBillingType {
    Regular, AuthenticationOnly, Authorization
}

enum class ZCreditObeligoAction {
    None, Capture, Release
}

enum class ZCreditPrintReceipt {
    No, Merchant, Customer, Both
}

class ZCreditTransaction {
    private var _authNum: String? = null
    private var _holderId: String? = null
    private var _extraData: String? = null
    private var _customerName: String? = null
    private var _customerAddress: String? = null
    private var _customerEmail: String? = null
    private var _phoneNumber: String? = null
    private var _obeligoActionReferenceNumber: String? = null
    private var _itemDescription: String? = null
    private var _track2: String? = null
    private var _cardNumber: String? = null
    private var _cvv: String? = null
    private var _expDate: String? = null
    var type: ZCreditType = ZCreditType.Regular
    var transactionType: ZCreditTransactionType = ZCreditTransactionType.Sale
    var currency: ZCreditCurrency = ZCreditCurrency.NIS
    var printReceipt: ZCreditPrintReceipt = ZCreditPrintReceipt.Both

    var amount: BigDecimal? = null

    var numberOfPayments: Int? = null
    var firstPayment: BigDecimal? = null
    var otherPayment: BigDecimal? = null

    var authNum: String?
        get() = _authNum
        set(value) {
            _authNum = escapeXMLSymbols(value)
        }
    var billingType: ZCreditBillingType? = null

    var holderId: String?
        get() = _holderId
        set(value) {
            _holderId = escapeXMLSymbols(value)
        }

    var extraData: String?
        get() = _extraData
        set(value) {
            _extraData = escapeXMLSymbols(value)
        }

    var customerPresent: Boolean? = null

    var customerName: String?
        get() = _customerName
        set(value) {
            _customerName = escapeXMLSymbols(value)
        }
    var customerAddress: String?
        get() = _customerAddress
        set(value) {
            _customerAddress = escapeXMLSymbols(value)
        }
    var customerEmail: String?
        get() = _customerEmail
        set(value) {
            _customerEmail = escapeXMLSymbols(value)
        }

    var phoneNumber: String?
        get() = _phoneNumber
        set(value) {
            _phoneNumber = escapeXMLSymbols(value)
        }

    var obeligoAction: ZCreditObeligoAction? = null

    var obeligoActionReferenceNumber: String?
        get() = _obeligoActionReferenceNumber
        set(value) {
            _obeligoActionReferenceNumber = escapeXMLSymbols(value)
        }

    var itemDescription: String?
        get() = _itemDescription
        set(value) {
            _itemDescription = escapeXMLSymbols(value)
        }

    var track2: String?
        get() = _track2
        set(value) {
            _track2 = escapeXMLSymbols(value)
        }
    var cardNumber: String?
        get() = _cardNumber
        set(value) {
            _cardNumber = escapeXMLSymbols(value)
        }
    var cvv: String?
        get() = _cvv
        set(value) {
            _cvv = escapeXMLSymbols(value)
        }
    var expDate: String?
        get() = _expDate
        set(value) {
            _expDate = escapeXMLSymbols(value)
        }
    var useAdvancedDuplicatesCheck: Boolean? = null
    var transactionUniqueID: String? = null
    var transactionUniqueIdForQuery: String? = null

    override fun toString(): String {
        return "ZCreditTransaction(type=$type, transactionType=$transactionType, currency=$currency, amount=$amount, numberOfPayments=$numberOfPayments, firstPayment=$firstPayment, otherPayment=$otherPayment, authNum=$authNum, billingType=$billingType, holderId=$holderId, extraData=$extraData, customerPresent=$customerPresent, customerName=$customerName, customerAddress=$customerAddress, customerEmail=$customerEmail, phoneNumber=$phoneNumber, obeligoAction=$obeligoAction, obeligoActionReferenceNumber=$obeligoActionReferenceNumber, itemDescription=$itemDescription, track2=$track2, cardNumber=$cardNumber, cvv=$cvv, expDate=$expDate, " +
                "useAdvancedDuplicatesCheck=$useAdvancedDuplicatesCheck, transactionUniqueID=$transactionUniqueID, transactionUniqueIdForQuery=$transactionUniqueIdForQuery)"
    }

    private fun escapeXMLSymbols(original: String?): String? {
        if (original == null) {
            return original
        }

        var replaced = original.replace("\"", "&quot;")
        replaced = replaced.replace("\'", "&apos;")
        replaced = replaced.replace("<", "&lt;")
        replaced = replaced.replace(">", "&gt;")
        replaced = replaced.replace("&", "&amp;")
        return replaced
    }
}

class ZCreditTransactionResponse {
    var result: Boolean? = null
    var code: Int? = null
    var message: String? = null
    var cardNumber: String? = null
    var expDate: String? = null
    var cardName: String? = null
    var cardIssuerCode: String? = null
    var cardFinancerCode: String? = null
    var cardBrandCode: String? = null
    var referenceNumber: String? = null
    var voucherNumber: String? = null
    var approvalNumber: String? = null
    var isTelApprovalNeeded: Boolean? = null
    var token: String? = null
    var clientReceipt: String? = null
    var sellerReceipt: String? = null
    var tpn: String? = null
    var intotJson: String? = null
    var useAdvancedDuplicatesCheck: Boolean? = null
    var transactionUniqueID: String? = null
    var transactionUniqueIdForQuery: String? = null

    override fun toString(): String {
        return "ZCreditTransactionResponse(result=$result, code=$code, message=$message, cardNumber=$cardNumber, expDate=$expDate, cardName=$cardName, cardIssuerCode=$cardIssuerCode, cardFinancerCode=$cardFinancerCode, cardBrandCode=$cardBrandCode, referenceNumber=$referenceNumber, voucherNumber=$voucherNumber, approvalNumber=$approvalNumber, isTelApprovalNeeded=$isTelApprovalNeeded, token=$token, clientReceipt=$clientReceipt, sellerReceipt=$sellerReceipt, tpn=$tpn, intot_json=$intotJson," +
                " useAdvancedDuplicatesCheck = $useAdvancedDuplicatesCheck, transactionUniqueID = $transactionUniqueID, transactionUniqueIdForQuery = $transactionUniqueIdForQuery)"
    }
}

