package com.dvmms.sample.screens;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.dvmms.dejapay.models.DejavooTransactionResponse;
import com.dvmms.sample.R;
import com.dvmms.sample.screens.navigators.GetStatusSampleNavigatorContract;
import com.dvmms.sample.ui.TerminalService;
import com.dvmms.sample.ui.TerminalSettingsModel;
import com.dvmms.sample.ui.fragments.GetStatusFragment;
import com.dvmms.sample.ui.fragments.PaymentReportFragment;

public class GetStatusSampleActivity
        extends AppCompatActivity
    implements GetStatusSampleNavigatorContract
{
    private static final String ROOT_BACK_TAG = "ROOT_BACK_TAG";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TerminalService.getInstance().setSettingsModel(this, TerminalSettingsModel.load(this));

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.flContainer, GetStatusFragment.newInstance(), GetStatusFragment.TAG)
                .addToBackStack(ROOT_BACK_TAG)
                .commitAllowingStateLoss();
    }

    @Override
    public void onShowTransactionReport(DejavooTransactionResponse response) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.flContainer, PaymentReportFragment.newInstance(response), PaymentReportFragment.TAG)
                .addToBackStack(PaymentReportFragment.TAG)
                .commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            super.onBackPressed();
        } else {
            finish();
        }
    }

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, GetStatusSampleActivity.class);
    }
}
