package com.dvmms.sample.ui.fragments

import android.os.Build
import android.os.Bundle
import android.util.Base64OutputStream
import androidx.annotation.RequiresApi
import com.dvmms.dejapay.Base64Coder
import com.dvmms.dejapay.models.DejavooTransactionResponse
import com.dvmms.sample.common.ReceiptHelper
import com.dvmms.sample.ui.fields.BaseFieldViewModel
import com.dvmms.sample.ui.fields.LabelWithHeaderFieldViewModel
import com.dvmms.sample.ui.fields.SectionFieldViewModel
import java.net.URLDecoder
import java.util.*

/**
 * Created by Platon on 05.07.2016.
 */
class PaymentDetailsFragment : AbstractFieldsFragment() {
    private lateinit var transactionResponse: DejavooTransactionResponse

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            transactionResponse = arguments!!.getSerializable(TRANSACTION_RESPONSE_KEY) as DejavooTransactionResponse
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun setupUI() {
        val fields: MutableList<BaseFieldViewModel> = ArrayList()
        fields.add(LabelWithHeaderFieldViewModel.Builder()
                .title("Payment Type")
                .data(transactionResponse.paymentType?.name)
                .build()
        )
        fields.add(LabelWithHeaderFieldViewModel.Builder()
                .title("Transaction Type")
                .data(transactionResponse.transactionType?.name)
                .build()
        )
        fields.add(LabelWithHeaderFieldViewModel.Builder()
                .title("Reference ID")
                .data(transactionResponse.referenceId)
                .build()
        )
        fields.add(LabelWithHeaderFieldViewModel.Builder()
                .title("Register ID")
                .data(transactionResponse.registerId)
                .build()
        )
        fields.add(LabelWithHeaderFieldViewModel.Builder()
                .title("Invoice Number")
                .data(transactionResponse.invoiceNumber)
                .build()
        )
        fields.add(LabelWithHeaderFieldViewModel.Builder()
                .title("Result Code")
                .data(transactionResponse.resultCode.name)
                .build()
        )
        fields.add(LabelWithHeaderFieldViewModel.Builder()
                .title("Response Message")
                .data(transactionResponse.responseMessage)
                .build()
        )
        fields.add(LabelWithHeaderFieldViewModel.Builder()
                .title("Message")
                .data(transactionResponse.message)
                .build()
        )
        fields.add(LabelWithHeaderFieldViewModel.Builder()
                .title("Authentication Code")
                .data(transactionResponse.authenticationCode)
                .build()
        )
        fields.add(LabelWithHeaderFieldViewModel.Builder()
                .title("PN Reference")
                .data(transactionResponse.pnReference)
                .build()
        )
        fields.add(LabelWithHeaderFieldViewModel.Builder()
                .title("EMV Data")
                .data(transactionResponse.emvData)
                .build()
        )
        fields.add(LabelWithHeaderFieldViewModel.Builder()
                .title("TID")
                .data(transactionResponse.terminalId)
                .build()
        )
        fields.add(LabelWithHeaderFieldViewModel.Builder()
                .title("MID")
                .data(transactionResponse.merchantId)
                .build()
        )
        fields.add(LabelWithHeaderFieldViewModel.Builder()
                .title("SN")
                .data(transactionResponse.serialNumber)
                .build()
        )
        fields.add(LabelWithHeaderFieldViewModel.Builder()
                .title("HostSpecific")
                .data(transactionResponse.hostSpecific)
                .build()
        )
        //val receipt = ReceiptHelper.htmlFromDejavooReceipt(transactionResponse.extData["Receipt"], null)
        fields.add(LabelWithHeaderFieldViewModel.Builder()
                .title("Print Receipt")
                .data(transactionResponse.getReceipt(DejavooTransactionResponse.ReceiptType.Merchant))
                .build()
        )
        //        fields.add(new LabelWithHeaderFieldViewModel.Builder()
//                .title("Error")
//                .data(mResponse.getError())
//                .build()
//        );
//        fields.add(new LabelWithHeaderFieldViewModel.Builder()
//                .title("Error Code")
//                .data(mResponse.getErrorCode())
//                .build()
//        );
        if (transactionResponse.getExtData("") != null) {
            fields.add(SectionFieldViewModel.Builder()
                    .title("Extension Data")
                    .build()
            )
            for ((key, value) in transactionResponse.getExtData("")) {
                val data = if (key == "Receipt") {
                    URLDecoder.decode(value)
                } else {
                    value
                }
                fields.add(LabelWithHeaderFieldViewModel.Builder()
                        .title(key)
                        .data(data)
                        .build()
                )
            }
        }
        setForm(fields)
    }

    companion object {
        private const val TRANSACTION_RESPONSE_KEY = "TRANSACTION_RESPONSE_KEY"
        @JvmStatic
        fun newInstance(response: DejavooTransactionResponse?): PaymentDetailsFragment {
            val fragment = PaymentDetailsFragment()
            val args = Bundle()
            args.putSerializable(TRANSACTION_RESPONSE_KEY, response)
            fragment.arguments = args
            return fragment
        }
    }
}