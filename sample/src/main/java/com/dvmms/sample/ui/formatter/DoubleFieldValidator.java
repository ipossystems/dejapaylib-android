package com.dvmms.sample.ui.formatter;

/**
 * Created by Platon on 14.01.2016.
 */
public class DoubleFieldValidator implements IValidator<Double> {
    private final double minValue;
    private final double maxValue;
    private final boolean nullable;

    public DoubleFieldValidator(double minValue, double maxValue, boolean nullable) {
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.nullable = nullable;
    }

    @Override
    public boolean validate(Double value) {
        if (nullable && value == null) {
            return true;
        }

        return (value != null && value >= minValue && value <= maxValue);
    }
}
