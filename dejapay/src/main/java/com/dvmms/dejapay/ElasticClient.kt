package com.dvmms.dejapay

import android.util.Log
import okhttp3.OkHttpClient
import okhttp3.Response
import org.json.JSONException
import java.io.IOException
import java.util.concurrent.TimeUnit

class ElasticClient: LogClient {
    private val protocol: String = "http"
    private val server: String = "logstash.dvmms.com"
    private val port: Int = 40123
    private val token: String = "ZGVqYXZvbzpZeEQ0V2JBMjZKM1B3dlFKaFFFZQ=="

    private lateinit var client: OkHttpClient

    override fun connect() {
        client = OkHttpClient.Builder()
            .connectTimeout(15, TimeUnit.SECONDS)
            .build()
    }

    @Throws(IOException::class)
    override fun send(message: String?) {
        if (message.isNullOrEmpty()) {
            return
        }

        var response: Response? = null
        try {
            val request = ElasticRequest(protocol, server, port, token).create(message)
            val client = OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .build()
            response = client.newCall(request).execute()
//        } catch (ex: HttpResponseException) {
//            Log.e(tag, "Received status code:" + ex.statusCode)
//            Log.e(tag, "Error message:" + ex.message)
        } catch (ex: JSONException) {
            Log.e(tag, "JSON error:" + ex.message)
        } finally {
            response?.close()
        }
    }

    override fun close() {
    }

    companion object {
        val tag = ElasticClient::class.java.canonicalName ?: ElasticClient::class.java.simpleName
    }
}