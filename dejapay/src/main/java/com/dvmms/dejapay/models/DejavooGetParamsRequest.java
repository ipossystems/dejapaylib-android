package com.dvmms.dejapay.models;

public class DejavooGetParamsRequest {
    private String registerId;
    private String authKey;
    private String file;
    private String params;

    /**
     * Alpha numeric reference field.
     * @return Register Id
     */
    public String getRegisterId() {
        return registerId;
    }

    /**
     * Alpha numeric reference field.
     * @param registerId Register Id
     */
    public void setRegisterId(String registerId) {
        this.registerId = registerId;
    }

    /**
     * Get authentication key
     * @return authentication key
     */
    public String getAuthKey() {
        return authKey;
    }

    /**
     * Set authentication key
     * @param authKey authentication key
     */
    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }
}
