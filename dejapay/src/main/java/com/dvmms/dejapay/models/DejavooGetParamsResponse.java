package com.dvmms.dejapay.models;

import java.util.List;

public class DejavooGetParamsResponse {
    private String registerId;
    private List<Param> parameters;

    public String getRegisterId() {
        return registerId;
    }

    public void setRegisterId(String registerId) {
        this.registerId = registerId;
    }

    public List<Param> getParameters() {
        return parameters;
    }

    public void setParameters(List<Param> parameters) {
        this.parameters = parameters;
    }

    public String getValue(String name) {
        if (parameters != null && parameters.size() > 0) {
            for (Param param : parameters) {
                if (param.getName().equalsIgnoreCase(name)) {
                    return param.getValue();
                }
            }
        }

        return null;
    }


    public static class Param {
        private String file;
        private String name;
        private String value;
        private String perm;

        public String getFile() {
            return file;
        }

        public void setFile(String file) {
            this.file = file;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getPerm() {
            return perm;
        }

        public void setPerm(String perm) {
            this.perm = perm;
        }
    }
}
