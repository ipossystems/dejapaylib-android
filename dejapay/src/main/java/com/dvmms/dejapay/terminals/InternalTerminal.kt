package com.dvmms.dejapay.terminals

import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import com.dvmms.dejapay.*
import com.dvmms.dejapay.exception.DejavooInvalidResponseException
import com.dvmms.dejapay.exception.DejavooThrowable
import com.dvmms.dejapay.models.*
import com.dvmms.dejapay.models.requests.DejavooRequestTransactionRecord
import com.dvmms.dejapay.models.requests.DejavooResponseTransactionRecords
import kotlin.coroutines.coroutineContext
import kotlin.math.log


class InternalTerminal(appId: String) {
    private val transactionConverter = TransactionConvertor()
    private val printoutConverter = PrintoutConvertor()

    val dvPayPackage = if (Build.BRAND.contains("wizarpos", true) || Build.BRAND.contains("smartpos", true)) {
        "com.dejavoo.dvpay"
    } else {
        "com.dejavoo.dvpay.kozen"
    }
    val dvPayAction = "com.dejavoo.dvpay.InternalTerminalService"
    private val logger: SimpleLogger = SimpleLogger(appId)

    private var internalRemoteService: DvPayInternalRemoteService? = null

    fun getTerminalInfo(context: Context, callback: IRequestCallback<DvPayTerminalInfo>) {
        if (internalRemoteService == null) {
            connectService(context)
        }
        internalRemoteService?.getTerminalInfo(context, logger, callback)
    }

    fun sendTransaction(context: Context, request: String, callback: IRequestCallback<String>) {
        logger.error(context, "HUEHUE", request)
        if (internalRemoteService == null) {
            connectService(context)
        }
        internalRemoteService?.sendTransaction(context, logger, request, callback = object :
            IRequestCallback<String> {
            override fun onResponse(response: String?) {
                response?.let {
                    logger.debug(context, "h", it)
                    callback.onResponse(it)
                }
            }

            override fun onError(throwable: DejavooThrowable?) {
                throwable?.message?.let {
                    callback.onError(throwable)
                    logger.debug(context, "h", it) }
            }
        })
    }

    private fun newIntent(): Intent {
        return Intent().apply {
            `package` = dvPayPackage
            action = dvPayAction
        }
    }

    private fun connectService(context: Context) {
        internalRemoteService = DvPayInternalRemoteService()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(newIntent())
        } else {
            context.startService(newIntent())
        }
        internalRemoteService?.getServiceConnection()?.let { context.bindService(newIntent(), it, Context.BIND_AUTO_CREATE) }
    }

    fun commitTransaction(context: Context, request: DejavooTransactionRequest, callback: IRequestCallback<DejavooTransactionResponse>) {
        val xml = transactionConverter.serialize(request)
        logger.verbose(context, TAG, "Start transaction with request: $xml")
        val fields = XmlDocument.fromText(xml)?.getFields("request") ?: listOf()
        if (fields.isNotEmpty()) {
            val packet = RequestDocument("transaction").apply {
                add("version", 1)
                add("provider", "vega")

                addFields(fields)

            }.asRequest()

            DvPaymentActivity.setCallback(object : TransactionCallback {
                override fun onSuccess(response: String) {
                    logger.debug(context, TAG, "Response: $response")
                    callback.onResponse(transactionConverter.deserialize(response))
                }

                override fun onFailed(t: Throwable) {
                    logger.error(context, TAG, "Failed with error ${t.cause} and message ${t.message}", t)
                    callback.onError(DejavooThrowable(t))
                }
            })
            logger.debug(context, TAG, "Send packet: $packet")
            context.startActivity(
                DvPaymentActivity.getCallingIntent(
                    context,
                    PaymentApp.Aura,
                    packet
                )
            )
        } else {
            logger.error(context, TAG, "Can not parse request")
            callback.onError(DejavooThrowable("Can not parse request"))
        }
    }

    fun print(context: Context, request: DejavooPrintoutRequest, callback: IRequestCallback<DejavooPrintoutResponse>) {
        val xml = printoutConverter.serialize(request)
        logger.debug(context, TAG, "Start print with request: $xml")
        val fields = XmlDocument.fromText(xml)?.getFields("request") ?: listOf()

        if (fields.isNotEmpty()) {
            val packet = RequestDocument("printout").apply {
                add("version", 1)
                add("provider", "vega")

                addFields(fields)

            }.asRequest()

            DvPaymentActivity.setCallback(object : TransactionCallback {
                override fun onSuccess(response: String) {
                    logger.debug(context, TAG, "Response $response")
                    callback.onResponse(printoutConverter.deserialize(response))
                }

                override fun onFailed(t: Throwable) {
                    logger.error(context, TAG, "Failed with error ${t.cause} and message ${t.message}")
                    callback.onError(DejavooThrowable(t))
                }
            })
            logger.debug(context, TAG, "Send packet: $packet")
            context.startActivity(
                DvPaymentActivity.getCallingIntent(
                    context,
                    PaymentApp.Aura,
                    packet
                )
            )
        } else {
            logger.error(context, TAG, "Can not parse request")
            callback.onError(DejavooThrowable("Can not parse request"))
        }
    }

    fun getTransaction(
        context: Context,
        record: DejavooRequestTransactionRecord,
        tpn: String,
        callback: IRequestCallback<DejavooTransactionResponse>
    ) {
        getTransaction(context, record, tpn, "", callback)
    }

    fun getTransaction(
        context: Context,
        record: DejavooRequestTransactionRecord,
        tpn: String,
        authKey: String,
        callback: IRequestCallback<DejavooTransactionResponse>
    ) {
        val request = DejavooTransactionRequest()
        request.tpn = tpn
        request.authenticationKey = authKey
        request.paymentType = record.paymentType
        request.transactionType = DejavooTransactionType.Status
        request.referenceId = record.refId
        request.isSignatureCapable = true
        request.invoiceNumber = record.invoiceId
        request.receiptType = record.returnReceipt
        request.printReceipt = record.printReceipt
        request.setOnBypassSignature()
        if (record.transNum > 0) {
            request.transNum = record.transNum
        }
        commitTransaction(context, request, callback)
    }

    fun showOptions(context: Context, request: DejavooUserChoiceRequest, callback: IRequestCallback<DejavooUserChoiceResponse>) {
        val converter = UserChoiceConvertor()
        logger.debug(context, TAG, "Show options")
        val packet = converter.serialize(request)

        DvPaymentActivity.setCallback(object : TransactionCallback {
            override fun onSuccess(data: String) {
                logger.debug(context, TAG, "Response $data")
                callback.onResponse(converter.deserialize(data))
            }

            override fun onFailed(t: Throwable) {
                logger.error(context, TAG, "Failed with error ${t.cause} and message ${t.message}")
                callback.onError(DejavooThrowable(t))
            }
        })
        logger.debug(context, TAG, "Send packet: $packet")
        context.startActivity(
            DvPaymentActivity.getCallingIntent(
                context,
                PaymentApp.Aura,
                packet
            )
        )
    }


    fun getTransactions(
        context: Context,
        paymentType: DejavooPaymentType?,
        offset: Int,
        limit: Int,
        tpn: String,
        callback: IRequestCallback<DejavooResponseTransactionRecords>
    ) {
        getTransactions(context, paymentType, offset, limit, tpn,"", callback)
    }

    fun getTransactions(
        context: Context,
        paymentType: DejavooPaymentType?,
        offset: Int,
        limit: Int,
        tpn: String,
        authKey: String,
        callback: IRequestCallback<DejavooResponseTransactionRecords>
    ) {
        val converter = GetTransactionRecordsConvertor()

        val packet: String = converter.serialize(
            tpn,
            paymentType,
            "1",
            authKey,
            offset,
            offset + limit - 1
        )

        DvPaymentActivity.setCallback(object : TransactionCallback {
            override fun onSuccess(data: String) {
                logger.debug(context, TAG, "Response $data")
                val response: DejavooResponseTransactionRecords = converter.deserialize(data)

                if (response != null) {
                    var start = offset
                    for (record in response.records) {
                        record.transNumber = start
                        start++
                    }
                    logger.debug(context, TAG, "Response ${response.message}")
                    callback.onResponse(response)
                } else {
                    logger.error(context, TAG, "Response is null")
                    callback.onError(DejavooInvalidResponseException())
                }
            }

            override fun onFailed(t: Throwable) {
                logger.error(context, TAG, "Failed with error ${t.cause} and message ${t.message}")
                callback.onError(DejavooThrowable(t))
            }
        })
        logger.debug(context, TAG, "Send packet: $packet")
        context.startActivity(
            DvPaymentActivity.getCallingIntent(
                context,
                PaymentApp.Aura,
                packet
            )
        )
    }

    fun isInstalled(context: Context): Boolean {
        return DvPaymentActivity.isAvailable(context, PaymentApp.Aura)
    }

    companion object {
        val TAG: String = "DejaPAY_Library"
    }
}