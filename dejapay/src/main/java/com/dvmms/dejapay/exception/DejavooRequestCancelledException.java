package com.dvmms.dejapay.exception;

/**
 * DejaPay request was canceled
 */
public class DejavooRequestCancelledException extends DejavooThrowable {
    public DejavooRequestCancelledException() {
    }

    public DejavooRequestCancelledException(String message) {
        super(message);
    }

    public DejavooRequestCancelledException(Throwable throwable) {
        super(throwable);
    }
}
