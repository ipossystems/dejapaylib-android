package com.dvmms.sample.screens

import android.os.Build
import android.util.Log
import com.dvmms.dejapay.terminals.ZCreditCurrency
import com.dvmms.dejapay.terminals.ZCreditInternalTerminal
import com.dvmms.dejapay.terminals.ZCreditPrintReceipt
import com.dvmms.dejapay.terminals.ZCreditTerminalCallback
import com.dvmms.dejapay.terminals.ZCreditTransaction
import com.dvmms.dejapay.terminals.ZCreditTransactionResponse
import com.dvmms.dejapay.terminals.ZCreditTransactionType
import com.dvmms.dejapay.terminals.ZCreditType
import com.dvmms.sample.ui.fields.*
import com.dvmms.sample.ui.formatter.IntegerFieldFormatter
import com.dvmms.sample.ui.formatter.PriceBigDecimalFieldFormatter
import com.dvmms.sample.ui.fragments.AbstractFieldsFragment
import java.math.BigDecimal

class ZCreditInternalTransactionFragment:
        AbstractFieldsFragment() {

    private val transaction = ZCreditTransaction().apply {
        amount = BigDecimal(1.01).setScale(2, BigDecimal.ROUND_HALF_UP)
    }

    override fun setupUI() {
        loadForm()
    }

    private fun loadForm() {
        val fields = mutableListOf<BaseFieldViewModel>()

        ListPickerFieldViewModel.Builder()
                .activity(activity)
                .title("Credit Type")
                .possibleValues(
                    ZCreditType.values().associate { Pair(it, it.name) })
                .data(transaction.type)
                .updatedFieldListener {
                    transaction.type = it.data()
                    loadForm()
                }
                .build()
                .let(fields::add)

        ListPickerFieldViewModel.Builder()
                .activity(activity)
                .title("Transaction Type")
                .possibleValues(
                    ZCreditTransactionType.values()
                        .map { Pair(it, it.name) }
                        .toMap())
                .data(transaction.transactionType)
                .updatedFieldListener {
                    transaction.transactionType = it.data()
                }
                .build()
                .let(fields::add)


        ListPickerFieldViewModel.Builder()
                .title("Currency")
                .activity(activity)
                .possibleValues(
                    ZCreditCurrency.values()
                        .map { Pair(it, it.name) }
                        .toMap())
                .data(transaction.currency)
                .updatedFieldListener {
                    transaction.currency = it.data()
                    loadForm()
                }
                .build()
                .let(fields::add)

        TextFieldViewModel.Builder()
                .title("Amount")
                .maxLength(11)
                .type(TextFieldViewModel.Type.Number)
                .formatter(PriceBigDecimalFieldFormatter())
                .data(transaction.amount)
                .updatedFieldListener {
                    transaction.amount = it.data()
                }
                .build()
                .let(fields::add)

        if (transaction.type == ZCreditType.CreditInstalment || transaction.type == ZCreditType.RegularInstalment) {
            TextFieldViewModel.Builder()
                    .title("Number Of Payments")
                    .maxLength(2)
                    .type(TextFieldViewModel.Type.Number)
                    .formatter(IntegerFieldFormatter())
                    .data(transaction.numberOfPayments ?: 12)
                    .updatedFieldListener { transaction.numberOfPayments = it.data() }
                    .build()
                    .let(fields::add)

            if (transaction.type == ZCreditType.RegularInstalment) {
                TextFieldViewModel.Builder()
                        .title("First Payment")
                        .maxLength(11)
                        .type(TextFieldViewModel.Type.Number)
                        .formatter(PriceBigDecimalFieldFormatter())
                        .data(transaction.firstPayment)
                        .updatedFieldListener {
                            transaction.firstPayment = it.data()
                        }
                        .build()
                        .let(fields::add)

                TextFieldViewModel.Builder()
                        .title("Other Payment")
                        .maxLength(11)
                        .type(TextFieldViewModel.Type.Number)
                        .formatter(PriceBigDecimalFieldFormatter())
                        .data(transaction.otherPayment)
                        .updatedFieldListener {
                            transaction.otherPayment = it.data()
                        }
                        .build()
                        .let(fields::add)
            } else {
                transaction.firstPayment = null
                transaction.otherPayment = null
            }

        } else {
            transaction.numberOfPayments = null
            transaction.firstPayment = null
            transaction.otherPayment = null
        }

        ListPickerFieldViewModel.Builder()
                .activity(activity)
                .title("Print Receipt")
                .possibleValues(
                    ZCreditPrintReceipt.values()
                        .map { Pair(it, it.name) }
                        .toMap())
                .data(transaction.printReceipt)
                .updatedFieldListener {
                    transaction.printReceipt = it.data()
                }
                .build()
                .let(fields::add)

        TextFieldViewModel.Builder()
                .title("Holder ID")
                .maxLength(256)
                .type(TextFieldViewModel.Type.Text)
                .data(transaction.holderId)
                .updatedFieldListener {
                    transaction.holderId = it.data()
                }
                .build()
                .let(fields::add)

        TextFieldViewModel.Builder()
                .title("Phone Number")
                .maxLength(60)
                .type(TextFieldViewModel.Type.Number)
                .data(transaction.phoneNumber)
                .updatedFieldListener {
                    transaction.phoneNumber = it.data()
                }
                .build()
                .let(fields::add)

        TextFieldViewModel.Builder()
                .title("Item Description")
                .maxLength(256)
                .type(TextFieldViewModel.Type.Text)
                .data(transaction.itemDescription)
                .updatedFieldListener {
                    transaction.itemDescription = it.data()
                }
                .build()
                .let(fields::add)

        TextFieldViewModel.Builder()
                .title("Extra Data")
                .maxLength(256)
                .type(TextFieldViewModel.Type.Text)
                .data(transaction.extraData)
                .updatedFieldListener {
                    transaction.extraData = it.data()
                }
                .build()
                .let(fields::add)

        TextFieldViewModel.Builder()
                .title("Auth num")
                .maxLength(256)
                .type(TextFieldViewModel.Type.Text)
                .data(transaction.authNum)
                .updatedFieldListener {
                    transaction.authNum = it.data()
                }
                .build()
                .let(fields::add)


        SectionFieldViewModel.Builder()
                .title("Customer Details")
                .build()
                .let(fields::add)

        TextFieldViewModel.Builder()
                .title("Customer Name")
                .maxLength(256)
                .type(TextFieldViewModel.Type.Text)
                .data(transaction.customerName)
                .updatedFieldListener {
                    transaction.customerName = it.data()
                }
                .build()
                .let(fields::add)


        TextFieldViewModel.Builder()
                .title("Customer Address")
                .maxLength(256)
                .type(TextFieldViewModel.Type.Text)
                .data(transaction.customerAddress)
                .updatedFieldListener {
                    transaction.customerAddress = it.data()
                }
                .build()
                .let(fields::add)

        TextFieldViewModel.Builder()
                .title("Customer Email")
                .maxLength(256)
                .type(TextFieldViewModel.Type.Text)
                .data(transaction.customerEmail)
                .updatedFieldListener {
                    transaction.customerEmail = it.data()
                }
                .build()
                .let(fields::add)

        SectionFieldViewModel.Builder()
                .title("Additional fields")
                .build()
                .let(fields::add)

        SwitchFieldViewModel.Builder()
                .title("Use Advanced Duplicates Check")
                .data(transaction.useAdvancedDuplicatesCheck)
                .updatedFieldListener {
                    transaction.useAdvancedDuplicatesCheck = it.data()
                }
                .build()
                .let(fields::add)

        TextFieldViewModel.Builder()
                .title("Transaction Unique ID")
                .maxLength(256)
                .type(TextFieldViewModel.Type.Text)
                .data(transaction.transactionUniqueID)
                .updatedFieldListener {
                    transaction.transactionUniqueID = it.data()
                }
                .build()
                .let(fields::add)

        TextFieldViewModel.Builder()
                .title("Transaction Unique ID For Query")
                .maxLength(256)
                .type(TextFieldViewModel.Type.Text)
                .data(transaction.transactionUniqueIdForQuery)
                .updatedFieldListener {
                    transaction.transactionUniqueIdForQuery = it.data()
                }
                .build()
                .let(fields::add)

        ButtonFieldViewModel.Builder()
                .type(ButtonFieldViewModel.Type.Plain)
                .title("Send")
                .clickListener { _, _ ->
                    onStartProgress("Processing")
                    Log.d("ZCreditInternalTerminal", "Send transaction $transaction")
                    ZCreditInternalTerminal(requireContext().packageName)
                            .commitTransaction(requireContext(), transaction, object :
                                ZCreditTerminalCallback<ZCreditTransactionResponse> {
                                override fun onResponse(response: ZCreditTransactionResponse) {
                                    onStopProgress()
                                    Log.d("ZCreditInternalTerminal", "Receive response $response")

                                    response.result?.let {
                                        if (it) {
                                            showAlertMessage("Success")
                                        } else {
                                            showAlertMessage("Failed ${response.code} ${response.message}")
                                        }
                                    }

                                }

                                override fun onError(t: Throwable) {
                                    onStopProgress()
                                    Log.e("ZCreditInternalTerminal", "Transaction failed", t)
                                    showAlertMessage("Transaction Failed ${t.localizedMessage}")
                                }
                            })
                }
                .build()
                .let(fields::add)



        setForm(fields)
    }

    companion object {
        @JvmStatic
        val TAG = ZCreditInternalTransactionFragment::class.java.simpleName

        @JvmStatic
        fun newInstance(): ZCreditInternalTransactionFragment {
            return ZCreditInternalTransactionFragment()
        }
    }

}