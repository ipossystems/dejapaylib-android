package com.dvmms.dejapay

import android.util.Log
import java.text.SimpleDateFormat
import java.util.*

class DefaultLogFormattedData constructor(
    private val tpn: String
) : LogFormattedData {
    override fun format(position: LogPosition, level: Int, tag: String, message: String, appId: String): LogData {
        return LogData(
            env = "DejaPay Library",
            file = position.file,
            method = position.method,
            line = position.line,
            level = formatLevel(level),
            id = UUID.randomUUID().toString(),
            message = message,
            tag = tag,
            thread = formatThread(),
            time = formatDate(),
            version = formatVersion(),
            user = formatUsername(),
            posAppId = appId
        )
    }

    private fun formatDate(): String {
        val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.ENGLISH)
        return formatter.format(Date())
    }

    private fun formatLevel(level: Int): String {
        return when (level) {
            Log.DEBUG -> "DEBUG"
            Log.INFO -> "INFO"
            Log.WARN -> "WARN"
            Log.ERROR -> "ERROR"
            Log.ASSERT -> "ASSERT"
            else -> "VERBOSE"
        }
    }

    private fun formatThread(): String {
        val threadName = Thread.currentThread().name
        val threadId = Thread.currentThread().id
        return "$threadName(#$threadId)"
    }

    private fun formatVersion(): String {
        val versionName = BuildConfig.VERSION_NAME
        val versionCode = BuildConfig.VERSION_CODE
        val flavor = BuildConfig.FLAVOR
        return "${versionName}b$versionCode-$flavor"
    }

    private fun formatUsername(): String {
        return tpn
    }
}