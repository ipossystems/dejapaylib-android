package com.dvmms.sample.ui.formatter

import java.math.BigDecimal
import java.math.BigDecimal.ROUND_HALF_UP
import java.text.DecimalFormat

/**
 * Created by Platon on 13.01.2016.
 */
class PriceBigDecimalFieldFormatter : IFieldFormatter<BigDecimal?> {
    override fun format(value: BigDecimal?): String {
        if (value == null) {
            return ""
        }

        val df = DecimalFormat("#.##")
        df.maximumFractionDigits = 2
        df.minimumFractionDigits = 2
        return df.format(value).replace(",", ".")
    }

    override fun parse(value: String): BigDecimal? {
        return if (value.isNotEmpty()) {
            try {
                value.replace("[^0-9]".toRegex(), "")
                        .toBigDecimal()
                        .setScale(2, ROUND_HALF_UP)
                        .divide(100.0.toBigDecimal())
            } catch (e: NumberFormatException) {
                // TODO: Add log
                null
            }
        } else null
    }

    override fun transformText(oldValue: String, value: String): String {
        if (value.isEmpty()) {
            return ""
        }
        return parse(value)?.let(this::format) ?: return format(BigDecimal.ZERO)
    }
}