package com.dejavoo.dvpay;

import com.dejavoo.dvpay.IInternalTerminalCallback;

interface IInternalTerminal {
    Map getParameters();
    void sendTransaction(String transaction, IInternalTerminalCallback callback);
}