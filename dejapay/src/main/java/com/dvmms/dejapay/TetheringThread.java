package com.dvmms.dejapay;

import android.os.Build;

import com.dvmms.dejapay.exception.DejavooTetheringFailedException;
import com.dvmms.dejapay.exception.DejavooThrowable;
import com.dvmms.dejapay.models.DejavooPaymentType;
import com.dvmms.dejapay.models.DejavooTransactionResponse;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

/**
 * Created by Platon on 22.09.2016.
 */
public class TetheringThread extends Thread {
    public enum QueueCommand {
        OpenSocket, SendData, CloseSocket
    }

    private static final int DATA_LENGTH = 1024;

    class TetheringQueueItem {
        final QueueCommand command;
        String host;
        int port;
        String security;
        DejavooTransactionResponse.TetheringProtocol protocol;
        byte[] data;
        ISpinTetheringListener listener;
        DejavooPaymentType paymentType;

        public TetheringQueueItem(String host, int port, String security) {
            this.command = QueueCommand.OpenSocket;
            this.host = host;
            this.port = port;
            this.security = security;
        }

        public TetheringQueueItem(DejavooTransactionResponse.TetheringProtocol protocol, byte[] data, DejavooPaymentType paymentType, ISpinTetheringListener listener) {
            this.command = QueueCommand.SendData;
            this.protocol = protocol;
            this.data = data;
            this.listener = listener;
            this.paymentType = paymentType;
        }

        public TetheringQueueItem(QueueCommand command) {
            this.command = command;
        }

        public String getHost() {
            return host;
        }

        public int getPort() {
            return port;
        }

        public String getSecurity() {
            return security;
        }

        public DejavooTransactionResponse.TetheringProtocol getProtocol() {
            return protocol;
        }

        public byte[] getData() {
            return data;
        }

        public ISpinTetheringListener getListener() {
            return listener;
        }

        public DejavooPaymentType getPaymentType() {
            return paymentType;
        }
    }

    SSLSocket socket;
    OutputStream outputStream;
    InputStream inputStream;

    Deque<TetheringQueueItem> queueItemQueue;

    private static final int CONNECT_TIMEOUT_MS = 300000;
    private static final int ATTEMPT_TIMEOUT_MS = 2000;
    private static final int COUNT_ATTEMPTS = 3;
    private IDejaPayLogger logger;

    private static final byte VISAI_ENQ = 0x05;
    private static final byte VISAI_EOT = 0x04;
    private static final byte VISAI_ACK = 0x06;
    private static final byte VISAI_BEL = 0x07;
    private static final byte VISAI_UNKNOWN = 0x00;
    private static final byte VISAI_DC2 = 0x12;

    private Base64Coder base64Coder;

    private boolean isStopped = false;
//    private boolean isResponseAfterAck = false;

    public TetheringThread(IDejaPayLogger logger) {
        this.logger = logger;
        this.queueItemQueue = new LinkedBlockingDeque<>();
        this.base64Coder = new Base64Coder();
    }

    public void setLogger(IDejaPayLogger logger) {
        this.logger = logger;
    }

    @Override
    public void run() {

        while (!isStopped) {
            try {

                TetheringQueueItem item;
                while ((item  = queueItemQueue.pollFirst()) != null) {
                    logger.d("Found queue item");
                    switch (item.command) {
                        case OpenSocket: {
                            logger.d("Open Socket command");
                            openSocket(item.getHost(), item.getPort(), item.getSecurity());
                            break;
                        }
                        case CloseSocket: {
                            logger.d("Close Socket command");
                            closeSocket();
                            break;
                        }

                        case SendData: {
                            logger.d("Send Data command");
                            sendData(item);
                            break;
                        }
                    }
                }

                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
                logger.e(e, "Tethering Failed read data from socket");
                break;
            }
        }

        logger.d("Tethering thread stopped");
    }


    private void sendData(TetheringQueueItem item) throws InterruptedException {
        if (isOpenedSocket()) {
            byte[] bytes = null;
            for (int attempt = 1; attempt <= COUNT_ATTEMPTS; attempt++) {
                logger.d("SPin tethering attempt: %d. Message: %s", attempt, dbgMessage(item.getData(), item.getData().length));

                if (item.getProtocol() == DejavooTransactionResponse.TetheringProtocol.VisaI) {
                    bytes = getBytesVisaI(item.getPaymentType(), item.getData());
                } else if (item.getProtocol() == DejavooTransactionResponse.TetheringProtocol.DataWire) {
                    bytes = getBytesDefaultDataWire(item.getData());
                    closeSocket();
                } else  {
                    bytes = getBytesDefault(item.getData());
                }

                if (bytes != null) {
//                    logger.d("SPin response bytes: %s", dbgBytes(bytes, bytes.length));
                    logger.d("SPin response message: %s", dbgMessage(bytes, bytes.length));

                    break;
                } else {
                    logger.d("SPin response failed. Next attempt");
                }
                Thread.sleep(ATTEMPT_TIMEOUT_MS);
            }

            if (bytes != null) {
                item.getListener().onResponse(bytes);
            } else {
                item.getListener().onError(new DejavooTetheringFailedException());
            }
        } else {
            item.getListener().onError(new DejavooTetheringFailedException());
        }
    }

    public boolean isOpenedSocket() {
        return (socket != null);
    }

    boolean openSocket(String host, int port, String security) {
        closeSocket();

        logger.d("Trying open socket %s:%d (Security:%s)", host, port, security);
        try {

            SSLSocketFactory socketFactory = (SSLSocketFactory) SSLSocketFactory.getDefault();

            if (Build.VERSION.SDK_INT >= 16 && Build.VERSION.SDK_INT < 22) {
                try {
                    SSLContext sc = SSLContext.getInstance("TLSv1.2");
                    sc.init(null, null, null);
                    socketFactory = new Tls12SocketFactory(sc.getSocketFactory());
                } catch (Exception ex) {
                    logger.e(ex, "Error while setting TLS 1.2");
                    return false;
                }
            }

            socket = (SSLSocket) socketFactory.createSocket(host, port);

            logger.d("Supported protocols: %s", StringUtils.fromArray(socket.getSupportedProtocols(), ","));
            logger.d("Enabled protocols: %s", StringUtils.fromArray(socket.getEnabledProtocols(), ","));
            logger.d("Supported cipher suites: %s", StringUtils.fromArray(socket.getSupportedCipherSuites(), ","));
            logger.d("Enabled cipher suites: %s", StringUtils.fromArray(socket.getEnabledCipherSuites(), ","));

//            socket.connect(new InetSocketAddress(host, port), CONNECT_TIMEOUT_MS);
            socket.setKeepAlive(true);
//            socket.setSoTimeout(0);
            socket.startHandshake();

            outputStream = socket.getOutputStream();
            inputStream = socket.getInputStream();

            logger.d("Socket opened successful");

            return true;
        } catch (IOException ex) {
            logger.e(ex, "Open socket failed");
        }

        return false;
    }

    void closeSocket() {
        if (socket != null) {
            try {
                logger.d("Trying close socket");
                socket.close();
            } catch (IOException e) {
                logger.e(e, "Failed close socket with tethering host");
            } finally {
                outputStream = null;
                inputStream = null;
                socket = null;
            }
        }
//        isResponseAfterAck = false;
    }

    public void processConnect(String host, int port, String security) {
        queueItemQueue.add(new TetheringQueueItem(host, port, security));
    }

    public void processDisconnect() {
        queueItemQueue.add(new TetheringQueueItem(QueueCommand.CloseSocket));
    }

    public void process(
            DejavooTransactionResponse.TetheringProtocol protocol,
            byte[] tetheringMessage,
            DejavooPaymentType paymentType,
            ISpinTetheringListener iSpinTetheringListener
    ) {
        queueItemQueue.add(new TetheringQueueItem(protocol, tetheringMessage, paymentType, iSpinTetheringListener));
    }


    String dbgBytes(byte[] bytes, int length) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i=0; i < length; i++) {
            sb.append(String.format("0x%02x,", bytes[i]));
        }
        sb.append("]");

        return sb.toString();
    }

    boolean isCommandMessage(byte buffer[]) {
        return buffer.length == 1 && (
                buffer[0] == VISAI_EOT || buffer[0] == VISAI_ENQ || buffer[0] == VISAI_ACK || buffer[0] == VISAI_BEL || buffer[0] == VISAI_DC2
        );
    }

    private byte[] getBytesVisaI(DejavooPaymentType paymentType, byte[] bytes) {
        if (paymentType == DejavooPaymentType.Batch) {
            return handleMessageBatch(deserializeBulkToMessages(bytes));
        } else {
            return handleMessageTransaction(bytes);
        }
    }

    private byte[] readData() throws IOException, InterruptedException {
        int countAttempts = 3;

        byte[] buffer = new byte[1024];
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream(1024);
        int bytesRead = 0;
        while ((bytesRead = inputStream.read(buffer)) != -1 || countAttempts > 0) {
            if (bytesRead > 0) {
                byteBuffer.write(buffer, 0, bytesRead);
                logger.d("Read Data: %s", dbgMessage(byteBuffer.toByteArray(), bytesRead));
                break;
            }
            Thread.sleep(100);
            countAttempts++;
        }

        if (byteBuffer.size() > 0) {
            return byteBuffer.toByteArray();
        }

        return null;
    }

    private void writeData(byte data[]) throws IOException {
        logger.d("Write Data: %s", dbgMessage(data, data.length));
        outputStream.write(data);
    }

    private byte[] handleMessageTransaction(byte[] message) {
        try {
            byte[] resultMessage = null;

            while (!isStopped) {
                byte[] data = readData();

                if (data != null && isCommandMessage(data)) {
                    byte cmdByte = data[0];

                    switch (cmdByte) {
                        case VISAI_ENQ: {
                            writeData(message);
                            break;
                        }
                        case VISAI_ACK: {
                            resultMessage = readData();
                            if (resultMessage != null) {
                                writeData(new byte[]{VISAI_ACK});
                            } else {
                                return null;
                            }
                            break;
                        }
                        case VISAI_EOT: {
                            closeSocket();
                            return resultMessage;
                        }
                    }
                } else {
                    return data;
                }
            }

        } catch(IOException e) {
            logger.e(e, "IO failed with tethering host");
        } catch(InterruptedException e) {
            logger.e(e, "await failed interrupted tethering command");
        }
        return null;
    }

    private byte[] handleMessageBatch(List<byte[]> messages) {
        int indexMessage = 0;
        try {
            byte[] resultMessage = null;

            while (!isStopped) {
                byte[] data = readData();

                if (data != null && isCommandMessage(data)) {
                    byte cmdByte = data[0];

                    switch (cmdByte) {
                        case VISAI_ENQ: {
                            if (indexMessage >= messages.size()) throw new AssertionError("Messages boundaries");
                            writeData(messages.get(indexMessage));
                            indexMessage++;
                            break;
                        }
                        case VISAI_ACK: {
                            if (indexMessage >= messages.size()) throw new AssertionError("Messages boundaries");
                            writeData(messages.get(indexMessage));
                            indexMessage++;
                            break;
                        }
                        case VISAI_BEL: {
                            if (indexMessage >= messages.size()) throw new AssertionError("Messages boundaries");
                            writeData(messages.get(indexMessage));
                            indexMessage++;
                            break;
                        }
                        case VISAI_DC2: {
                            resultMessage = readData();
                            if (resultMessage != null) {
                                writeData(new byte[]{VISAI_ACK});
                            } else {
                                return null;
                            }
                            break;
                        }
                        case VISAI_EOT: {
                            return resultMessage;
                        }
                    }
                } else {
                    return data;
                }
            }
        } catch(IOException e){
            logger.e(e, "IO failed with tethering host");
        } catch(InterruptedException e){
            logger.e(e, "await failed interrupted tethering command");
        } // finally { }
        return null;
    }


    private byte[] removeParity(byte[] bytes) {
//        for (int i=0; i < bytes.length;i++) {
//            bytes[i] &= 0x7F;
//        }
//        return bytes;
        return bytes;
    }


    private byte[] getBytesDefault(byte[] message) {
        try {
            ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream(DATA_LENGTH);
            writeData(message);

            byte[] buffer = new byte[DATA_LENGTH];
            int bytesRead;

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                logger.d("Read bytes: %d", bytesRead);
                byteBuffer.write(buffer, 0, bytesRead);

                if (bytesRead < DATA_LENGTH) {
                    break;
                }
            }
            logger.d("Finish reading bytes");
            return byteBuffer.toByteArray();
        }  catch (IOException e) {
            logger.e(e, "IO failed with tethering host");
        }

        return null;
    }

    private byte[] getBytesDefaultDataWire(byte[] message) {
        try {
            ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream(DATA_LENGTH);
            writeData(message);

            byte[] buffer = new byte[DATA_LENGTH];
            int bytesRead;

            while ((bytesRead = readInputStreamWithTimeout(inputStream, buffer, 5000)) != -1) {
                logger.d("Read bytes: %d", bytesRead);
                byteBuffer.write(buffer, 0, bytesRead);
            }
            logger.d("Finish reading bytes");
            return byteBuffer.toByteArray();
        }  catch (IOException e) {
            logger.e(e, "IO failed with tethering host");
        }

        return null;
    }

    public int readInputStreamWithTimeout(InputStream is, final byte[] b, int timeoutMillis) {

        int readByte = -1;

        ExecutorService executor = Executors.newFixedThreadPool(2);

        try {

            // Read data with timeout
            Callable<Integer> readTask = new Callable<Integer>() {
                @Override
                public Integer call() throws Exception {
                    return inputStream.read(b);
                }
            };

            Future<Integer> future = executor.submit(readTask);
            readByte = future.get(timeoutMillis, TimeUnit.MILLISECONDS);
            if (readByte >= 0)
                logger.d("Read: %d", readByte);

        } catch (TimeoutException ex) {
            logger.e(ex, "Timeout exception");
        } catch (ExecutionException ex) {
            logger.e(ex, "Execution exception");
        } catch (InterruptedException ex) {
            logger.e(ex, "Interrupted exception");
        }
        return readByte;
    }

    public void finish() {
        isStopped = true;
    }

    public List<byte[]> deserializeBulkToMessages(byte[] bytes) {
        logger.d("Deserialize bulk raw: %s", dbgMessage(bytes, bytes.length));

        List<byte[]> messages = new ArrayList<>();

        bytes = removeBytesByPattern(bytes, "<bulk>".getBytes());
        bytes = removeBytesByPattern(bytes, "</bulk>".getBytes());
        bytes = removeBytesByPattern(bytes, "<msg>".getBytes());

        messages = split(bytes, "</msg>".getBytes());

        return messages;
    }

    private String dbgMessage(byte[] bytes, int bytesLength) {
        // Remove parity
        for (int i=0; i < bytes.length;i++) {
            bytes[i] &= 0x7F;
        }

        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream(DATA_LENGTH);

        try {

        for (int i=0; i < bytesLength; i++) {
                if (bytes[i] == 0x00) {
                    byteBuffer.write("<NULL>".getBytes());
                } else if (bytes[i] == 0x02) {
                    byteBuffer.write("<STX>".getBytes());
                } else if (bytes[i] == 0x03) {
                    byteBuffer.write("<ETX>".getBytes());
                } else if (bytes[i] == 0x04) {
                    byteBuffer.write("<EOT>".getBytes());
                } else if (bytes[i] == 0x05) {
                    byteBuffer.write("<ENQ>".getBytes());
                } else if (bytes[i] == 0x06) {
                    byteBuffer.write("<ACK>".getBytes());
                } else if (bytes[i] == 0x07) {
                    byteBuffer.write("<BEL>".getBytes());
                } else if (bytes[i] == 0x0A) {
                    byteBuffer.write("<LF>".getBytes());
                } else if (bytes[i] == 0x0D) {
                    byteBuffer.write("<CR>".getBytes());
                } else if (bytes[i] == 0x12){
                    byteBuffer.write("<DC2>".getBytes());
                } else if (bytes[i] == 0x15) {
                    byteBuffer.write("<NAK>".getBytes());
                } else if (bytes[i] == 0x17) {
                    byteBuffer.write("<ETB>".getBytes());
                } else if (bytes[i] == 0x1C) {
                    byteBuffer.write("<FS>".getBytes());
                } else if (bytes[i] == 0x1D) {
                    byteBuffer.write("<GS>".getBytes());
                } else if (bytes[i] == 0x1E) {
                    byteBuffer.write("<GS>".getBytes());
                } else {
                    byteBuffer.write(bytes[i]);
                }
            }
        } catch (IOException ex) {
            logger.e(ex, "dbgMessage failed");
            return "";
        }

        return byteBuffer.toString();
    }

    private byte[] removeBytesByPattern(byte[] input, byte[] pattern) {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream(DATA_LENGTH);
        int blockStart = 0;
        for(int i=0; i<input.length; i++) {
            if(isMatch(input, pattern,i)) {
                blockStart = i+pattern.length;
                i = blockStart;
            }

            if (i < input.length) {
                byteBuffer.write(input[i]);
            }
        }
        return byteBuffer.toByteArray();
    }

    private boolean isMatch(byte[] input, byte[] pattern, int pos) {
        for(int i=0; i< pattern.length; i++) {
            if(pattern[i] != input[pos+i]) {
                return false;
            }
        }
        return true;
    }

    private List<byte[]> split(byte[] input, byte[] pattern) {
        List<byte[]> l = new ArrayList<>();
        int blockStart = 0;
        for(int i=0; i<input.length; i++) {
            if(isMatch(input,pattern,i)) {
                l.add(Arrays.copyOfRange(input, blockStart, i));
                blockStart = i+pattern.length;
                i = blockStart;
            }
        }

        if (blockStart != input.length) {
            l.add(Arrays.copyOfRange(input, blockStart, input.length));
        }
        return l;
    }

    interface ISpinTetheringListener {
        void onResponse(byte[] bytes);
        void onError(DejavooThrowable throwable);
    }

}
