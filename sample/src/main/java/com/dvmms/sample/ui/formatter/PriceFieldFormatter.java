package com.dvmms.sample.ui.formatter;

import java.text.DecimalFormat;
/**
 * Created by Platon on 13.01.2016.
 */
public class PriceFieldFormatter implements IFieldFormatter<Float> {
    public static final String PRICE_PREFIX = "$";

    @Override
    public String format(Float value) {
        if (value == null) {
            return "";
        }

        if (value == 0) {
            return "$0.00";
        }

        DecimalFormat df = new DecimalFormat("#.##");
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        return PRICE_PREFIX + df.format(value).replace(",", ".");
    }

    @Override
    public Float parse(String value) {
        value = value.replace(PRICE_PREFIX, "");

        if (!value.isEmpty()) {
            try {
                return Float.parseFloat(value);
            } catch (NumberFormatException e) {
                // TODO: Add log
                return null;
            }
        }

        return null;
    }

    @Override
    public String transformText(String oldValue, String value) {
        if (value.isEmpty()) {
            return "";
        }

        Float data = parse(value);

        if (data == null) {
            return format(0.0f);
        }

        if (value.contains(".")) {
            int numberCharsAfPoint = value.substring(value.indexOf('.') + 1, value.length()).length();

            if (numberCharsAfPoint == 0) {
                data /= 100.f;
            } else if (numberCharsAfPoint == 1) {
                data /= 10.0f;
            } else if (numberCharsAfPoint == 3) {
                data *= 10.0f;
            }
        } else {
            data /= 100.f;
        }

        return format(data);

    }
}
