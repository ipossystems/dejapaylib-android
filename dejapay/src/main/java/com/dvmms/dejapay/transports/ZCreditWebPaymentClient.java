package com.dvmms.dejapay.transports;

import android.net.Uri;
import android.os.AsyncTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class ZCreditWebPaymentClient implements IPaymentApi {

    private final String path;
    private final int connectionTimeout;
    private final int readTimeout;
    private final boolean production;

    public ZCreditWebPaymentClient(String path, int connectionTimeout, int readTimeout, boolean production) {
        this.path = path;
        this.connectionTimeout = connectionTimeout;
        this.readTimeout = readTimeout;
        this.production = production;
    }

    public static String readFullyAsString(InputStream inputStream, String encoding)
            throws IOException {
        return readFully(inputStream).toString(encoding);
    }

    private static ByteArrayOutputStream readFully(InputStream inputStream)
            throws IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length = 0;
        while ((length = inputStream.read(buffer)) != -1) {
            stream.write(buffer, 0, length);
        }
        return stream;
    }

    @Override
    public WebRequest<String> getIndex() {
        return null;
    }

    @Override
    public WebRequest<String> sendTransaction(String request) {
        String uri = Uri.parse(path + "/cgi.html?TerminalTransaction=" +  encodeQuery(request))
                .buildUpon()
                .build()
                .toString();

        return new WebRequest<>(new GetHttpRequestTask(uri, connectionTimeout, readTimeout));
    }

    @Override
    public WebRequest<String> printout(String request) {
        String uri = Uri.parse(path + "/cgi.html?Printer=" + encodeQuery(request))
                .buildUpon()
                .build()
                .toString();

        return new WebRequest<>(new GetHttpRequestTask(uri, connectionTimeout, readTimeout));
    }

    @Override
    public WebRequest<String> status(String tpn) {
        String host = "spinpos.net";

        if (!production) {
            host = "test.spinpos.net";
        }

        String uri = Uri.parse("http://"+host+"/spin/GetTerminalStatus")
                .buildUpon()
                .appendQueryParameter("tpn", tpn)
                .build().toString();

        return new WebRequest<>(new GetHttpRequestTask(uri, connectionTimeout, readTimeout));
    }

    private String encodeQuery(String query) {
        try {
            return URLEncoder.encode(query, "utf-8");
        } catch (UnsupportedEncodingException ignore) {
            return query;
        }
    }

    private static class Response {
        private final int status;
        private final String data;
        private final Exception exception;

        Response(int status, String data, Exception e) {
            this.status = status;
            this.data = data;
            this.exception = e;
        }

        Response(Exception e) {
            this(0, "", e);
        }

        Response(int status, String data) {
            this(status, data, null);
        }


        boolean isFail() {
            return exception != null;
        }
    }

    private class GetHttpRequestTask extends AsyncTask<Void,Integer, Response> implements IWebClientRequest<String> {

        private final String queryUrl;
        private final int connectTimeout;
        private final int readTimeout;
        private IWebCallback<WebResponse<String>> onCompletion;
        private HttpURLConnection mConnection;

        public GetHttpRequestTask(String queryUrl, int connectTimeout, int readTimeout) {
            this.queryUrl = queryUrl;
            this.connectTimeout = connectTimeout;
            this.readTimeout = readTimeout;
        }

        @Override
        protected Response doInBackground(Void... params) {
            try {
                URL url = new URL(queryUrl);
                mConnection = (HttpURLConnection) url.openConnection();
                mConnection.setRequestMethod("GET");
                mConnection.setConnectTimeout(connectTimeout*1000);
                mConnection.setReadTimeout(readTimeout*1000);

                try {
                    mConnection.connect();

                    String data = readFullyAsString(mConnection.getInputStream(), "UTF-8");
                    int status = mConnection.getResponseCode();

                    return new Response(status, data);

                } catch (Exception e) {
                    return new Response(e);
                } finally {
                    // this is done so that there are no open connections left when this task is going to complete
                    mConnection.disconnect();
                }


            } catch (Exception e){
                return new Response(e);
            }
        }

        @Override
        protected void onPostExecute(Response response) {
            if (onCompletion != null) {
                if (response.isFail()) {
                    onCompletion.failure(response.exception);
                } else {
                    onCompletion.success(new WebResponse<>(response.status, response.data));
                }
            }
        }

        @Override
        public void cancel() {
            onCompletion = null;
            cancel(true);
            if (mConnection != null) {
                mConnection.disconnect();
            }
        }

        @Override
        public void execute(IWebCallback<WebResponse<String>> completion) {
            this.onCompletion = completion;
            super.execute();
        }
    }
}
